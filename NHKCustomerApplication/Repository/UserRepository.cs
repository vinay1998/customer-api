﻿using Dapper;
using Microsoft.Extensions.Configuration;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace NHKCustomerApplication.Repository
{
    /// <summary>
    /// UserRepository.
    /// </summary>
    public class UserRepository : IUserRepository
    {
        #region "Fields"
        private const string RESTPRODUCTATTRIBUTESSPNAME = "sp_Rest_Product_Attributes";
        private const string RESTPRODUCTSPNAME = "sp_Rest_Product";
        private const string USERREGISTRATIONSPNAME = "sp_User_SignUp";
        private const string USERREGISTRATIONVERIFICATIONSPNAME = "sp_User_Verification";
        private const string USERLOGINSPNAME = "sp_User_Login";
        private const string USERLOGININFOSPNAME = "sp_User_Login_Info";
        private const string USERLOGININFOSPNAMEV2_1 = "sp_User_Login_Info_V2_1";
        private const string LASTIPADDRESS = "127.0.0.1";
        private const string PROFILEPIC = "default-image_450.png";
        private string connectionString = string.Empty;
        private const string PASSWORDFORMAT = "SHA512";

        #endregion

        #region "constructor"

        public UserRepository()
        {
            connectionString = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().
                GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value;
        }

        #endregion

        /// <summary>
        /// RegisterCustomer.
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="saltKey"></param>
        /// <param name="password"></param>
        /// <param name="otp"></param>
        /// <returns></returns>
        public CustomerResponseModel RegisterCustomer(CustomerModel customer, string saltKey, string password, string otp, int languageId=0)
        {
            using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
            {
                try
                {
                    string result = db.Query<string>(USERREGISTRATIONSPNAME, new
                    {
                        customer.ApiKey,
                        CustomerGuid = Guid.NewGuid(),
                        Username = customer.Email,
                        customer.Email,
                        LastIpAddress = LASTIPADDRESS,
                        RegisteredInStoreId = customer.StoreId,
                        Phone = customer.MobileNo,
                        ProfilePic = PROFILEPIC,
                        customer.LastName,
                        customer.FirstName,
                        Password = password,
                        PasswordSalt = saltKey,
                        customer.DeviceNo,
                        customer.DeviceToken,
                        customer.Imei1,
                        customer.Imei2,
                        customer.LatPos,
                        customer.LongPos,
                        customer.DeviceType,
                        Otp = otp,
                        LanguageId = languageId
                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return new CustomerResponseModel { ValidData = true, ResultData = result };
                }
                catch (SqlException e)
                {
                    return new CustomerResponseModel { ValidData = false, ResultData = e.Message };
                }
            }
        }

        /// <summary>
        /// RegistaionVerificationCustomer.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public CustomerResponseModel RegistaionVerificationCustomer(RegistationverificationCustomer customer)
        {
            using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
            {
                try
                {
                    string result = db.Query<string>(USERREGISTRATIONVERIFICATIONSPNAME, new
                    {
                        customer.ApiKey,
                        customer.CustomerId,
                        customer.OTP
                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return new CustomerResponseModel { ValidData = true, ResultData = result };
                }
                catch (SqlException e)
                {
                    return new CustomerResponseModel { ValidData = false, ResultData = e.Message };
                }
            }
        }

        /// <summary>
        /// CustomerLogin.
        /// </summary>
        /// <param name="customer">CustomerLogin object.</param>
        /// <returns>CustomerLoginResponseModel object</returns>
        public CustomerLoginResponseModel CustomerLogin(CustomerLogin customer)
        {
            using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
            {
                try
                {
                    PasswordModel result = db.Query<PasswordModel>(USERLOGINSPNAME, new
                    {
                        customer.ApiKey,
                        customer.Email,
                        customer.Phone,
                        customer.StoreId
                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result != null)
                    {
                        var UserPassword = Helper.CreatePasswordHash(customer.Pswd, result.PasswordSalt, PASSWORDFORMAT);
                        if (UserPassword == result.Password)
                        {
                            CustomerLoginResponseModel result2 = db.Query<CustomerLoginResponseModel>(USERLOGININFOSPNAME, new
                            {
                                result.CustomerId,
                                result.Active,
                                result.Email,
                                customer.StoreId
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                            result2.UserID = result.CustomerId;
                            result2.UserStatus = result.Active == true ? "Active" : "InActive";
                            result2.ValidData = true;
                            result2.Email = result.Email;
                            return result2;
                        }
                        else
                        {
                            return new CustomerLoginResponseModel { ValidData = false, ResultData = "Password is  incorrect" };
                        }
                    }
                    return new CustomerLoginResponseModel { ValidData = false, ResultData = "Internal server error" }; ;
                }
                catch (SqlException e)
                {
                    return new CustomerLoginResponseModel { ValidData = false, ResultData = e.Message };
                }
            }
        }

        /// <summary>
        /// CustomerLogin.
        /// </summary>
        /// <param name="customer">CustomerLogin object.</param>
        /// <returns>CustomerLoginResponseModel object</returns>
        public CustomerLoginResponseModel CustomerLoginV2_1(CustomerLoginV2_1 customer)
        {
            using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
            {
                try
                {
                    PasswordModel result = db.Query<PasswordModel>(USERLOGINSPNAME, new
                    {
                        customer.ApiKey,
                        customer.Email,
                        customer.Phone,
                        customer.StoreId
                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result != null)
                    {
                        var UserPassword = Helper.CreatePasswordHash(customer.Pswd, result.PasswordSalt, PASSWORDFORMAT);
                        if (UserPassword == result.Password)
                        {
                            CustomerLoginResponseModel result2 = db.Query<CustomerLoginResponseModel>(USERLOGININFOSPNAMEV2_1, new
                            {
                                result.CustomerId,
                                result.Active,
                                result.Email,
                                customer.StoreId,
                                customer.TimeZoneId
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                            result2.UserID = result.CustomerId;
                            result2.UserStatus = result.Active == true ? "Active" : "InActive";
                            result2.ValidData = true;
                            result2.Email = result.Email;
                            return result2;
                        }
                        else
                        {
                            return new CustomerLoginResponseModel { ValidData = false, ResultData = "Password is  incorrect" };
                        }
                    }
                    return new CustomerLoginResponseModel { ValidData = false, ResultData = "Internal server error" }; ;
                }
                catch (SqlException e)
                {
                    return new CustomerLoginResponseModel { ValidData = false, ResultData = e.Message };
                }
            }
        }

        public CustomerRestProductResponseModel GetCooknRestItemsByRestnCookId(CooknRestProductByID customer)
        {
            using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
            {
                try
                {
                    var result = db.Query<RestProductModel>(RESTPRODUCTSPNAME, new
                    {
                        customer.APIKey,
                        customer.CustId,
                        customer.RestnCookId,
                        customer.StoreId
                    }, commandType: CommandType.StoredProcedure);
                    return new CustomerRestProductResponseModel { ValidData = true, ResultData = "", Products = result.ToList() };
                }
                catch (SqlException e)
                {
                    return new CustomerRestProductResponseModel { ValidData = false, ResultData = e.Message };
                }
            }
        }

        public List<ProductAttributesV2> GetProductProductAttributes(List<int> productIds)
        {
            using (IDbConnection db = new SqlConnection(Convert.ToString(connectionString)))
            {
                try
                {
                    string ProductIds = string.Join(",", productIds);
                    var result = db.Query<ProductAttributesV2>(RESTPRODUCTATTRIBUTESSPNAME, new
                    {
                        ProductIds
                    }, commandType: CommandType.StoredProcedure);
                    return result.ToList();
                }
                catch (SqlException e)
                {
                    return null;
                }
            }
        }
    }
}
