﻿using NHKCustomerApplication.ViewModels;
using System.Collections.Generic;

namespace NHKCustomerApplication.Repository
{
    /// <summary>
    /// UserRepository.
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// RegisterCustomer.
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="saltKey"></param>
        /// <param name="password"></param>
        /// <param name="otp"></param>
        /// <returns></returns>
        CustomerResponseModel RegisterCustomer(CustomerModel customer, string saltKey, string password, string otp, int languageId = 0);

        /// <summary>
        /// RegistaionVerificationCustomer.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerResponseModel RegistaionVerificationCustomer(RegistationverificationCustomer customer);

        /// <summary>
        /// CustomerLogin
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerLoginResponseModel CustomerLogin(CustomerLogin customer);

        /// <summary>
        /// CustomerLogin
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerLoginResponseModel CustomerLoginV2_1(CustomerLoginV2_1 customer);

        /// <summary>
        /// GetCooknRestItemsByRestnCookId.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerRestProductResponseModel GetCooknRestItemsByRestnCookId(CooknRestProductByID customer);

        List<ProductAttributesV2> GetProductProductAttributes(List<int> productIds);
    }
}
