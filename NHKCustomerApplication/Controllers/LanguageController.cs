﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;

namespace NHKCustomerApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LanguageController : ControllerBase
    {
        private ecuadordevContext dbcontext;
        public LanguageController(ecuadordevContext context)
        {
            dbcontext = context;
        }

        /// <summary>
        /// get all store languages
        /// </summary>
        /// <param name="requestObj">AllStoreLanguagesReqModel</param>
        /// <returns>AllStoreLanguagesResponseModel</returns>
        [Route("~/api/v1/AllStoreLanguages")]
        [HttpPost]
        public AllStoreLanguagesResponseModel GetAllStoreLanguages(AllStoreLanguagesReqModel requestObj)
        {
            int languageId = LanguageHelper.GetIdByLangCode(requestObj.StoreId, requestObj.UniqueSeoCode, dbcontext);
            AllStoreLanguagesResponseModel respObj = new AllStoreLanguagesResponseModel();
            if (requestObj.CustomerId == 0)
            {
                respObj.ErrorMessageTitle = "Error!!";
                respObj.ErrorMessage = LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
                respObj.Status = false;
                respObj.StatusCode = 204;
                respObj.ResponseObj = null;
                return respObj;
            }
            if (string.IsNullOrEmpty(requestObj.ApiKey))
            {
                respObj.ErrorMessageTitle = "Error!!";
                respObj.ErrorMessage = LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
                respObj.Status = false;
                respObj.StatusCode = (int)HttpStatusCode.NoContent;
                respObj.ResponseObj = null;
                return respObj;
            }
            if (!string.IsNullOrEmpty(requestObj.ApiKey))
            {
                var keyExist = dbcontext.VersionInfo.Any(x => x.Apikey == requestObj.ApiKey);
                if (!keyExist)
                {
                    respObj.ErrorMessageTitle = "Error!!";
                    respObj.ErrorMessage = LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                    respObj.Status = false;
                    respObj.StatusCode = (int)HttpStatusCode.Unauthorized;
                    respObj.ResponseObj = null;
                    return respObj;
                }
            }
            try
            {
                var languageResponse = new AllStoreLanguagesModel {
                    DefaultLanguageCode = LanguageHelper.GetDefaultLangCode(requestObj.StoreId, requestObj.CustomerId, dbcontext),
                    AllLanguages =LanguageHelper.GetAllLanguagesForList(requestObj.StoreId, dbcontext)
                };
                respObj.ErrorMessageTitle = "Success!!";
                respObj.ErrorMessage = LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.SuccessMessage", languageId, dbcontext);
                respObj.Status = true;
                respObj.StatusCode = 200;
                respObj.ResponseObj = languageResponse;
                return respObj;
                    
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                respObj.ErrorMessageTitle = "Error!!";
                respObj.ErrorMessage = ex.Message;
                respObj.Status = false;
                respObj.StatusCode = 400;
                respObj.ResponseObj = null;
                return respObj;
            }
        }

        /// <summary>
        /// get all local resource strings and update customer default language if UniqueSeoCode has value
        /// </summary>
        /// <param name="requestObj">LocaleStringResourceReqModel</param>
        /// <returns>LocaleStringResourceResponseModel</returns>
        [Route("~/api/v1/LocaleStringResources")]
        [HttpPost]
        public LocaleStringResourceResponseModel GetLocaleStringResources(LocaleStringResourceReqModel requestObj)
        {
            int languageId = LanguageHelper.GetIdByLangCode(requestObj.StoreId, requestObj.UniqueSeoCode, dbcontext);

            LocaleStringResourceResponseModel respObj = new LocaleStringResourceResponseModel();

            if (string.IsNullOrEmpty(requestObj.ApiKey))
            {
                respObj.ErrorMessageTitle = "Error!!";
                respObj.ErrorMessage = LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
                respObj.Status = false;
                respObj.StatusCode = (int)HttpStatusCode.NoContent;
                respObj.ResponseObj = null;
                return respObj;
            }
            if (!string.IsNullOrEmpty(requestObj.ApiKey))
            {
                var keyExist = dbcontext.VersionInfo.Any(x => x.Apikey == requestObj.ApiKey);
                if (!keyExist)
                {
                    respObj.ErrorMessageTitle = "Error!!";
                    respObj.ErrorMessage = LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                    respObj.Status = false;
                    respObj.StatusCode = (int)HttpStatusCode.Unauthorized;
                    respObj.ResponseObj = null;
                    return respObj;
                }
            }
            try
            {
                string languageCode = LanguageHelper.GetDefaultLangCode(requestObj.StoreId, requestObj.CustomerId, dbcontext);
                if (!string.IsNullOrEmpty(requestObj.UniqueSeoCode))
                {
                    var currentLang = LanguageHelper.GetAllLanguages(requestObj.StoreId, requestObj.UniqueSeoCode, dbcontext);
                    if (!currentLang.Any())
                    {
                        respObj.ErrorMessageTitle = "Error!!";
                        respObj.ErrorMessage = LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.ErrorMesaage.SystemNotSupportLanguage", languageId, dbcontext);
                        respObj.Status = false;
                        respObj.StatusCode = (int)HttpStatusCode.NoContent;
                        respObj.ResponseObj = null;
                        return respObj;
                    }
                    languageCode = requestObj.UniqueSeoCode;
                }
                using (IDbConnection db = new SqlConnection(Helper.GetConnectionString()))
                {
                    var data = db.Query<LocaleStringResourceModel>("GetLocaleStringResources", new
                    {
                        requestObj.StoreId,
                        requestObj.CustomerId,
                        requestObj.PrefixValue,
                        UniqueSeoCode=languageCode
                    }, commandType: CommandType.StoredProcedure).ToList();

                    respObj.ErrorMessageTitle = "Success!!";
                    respObj.ErrorMessage = LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.ErrorMesaage.StringFetchedSuccessfully", languageId, dbcontext);
                    respObj.Status = true;
                    respObj.StatusCode = 200;
                    respObj.ResponseObj = data;
                    return respObj;
                }
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                respObj.ErrorMessageTitle = "Error!!";
                respObj.ErrorMessage = ex.Message;
                respObj.Status = false;
                respObj.StatusCode = 400;
                respObj.ResponseObj = null;
                return respObj;
            }
        }
        [Route("~/api/v1/GetTopicContents")]
        [HttpPost]
        public AllStoreLanguagesResponseModel GetTopicContents(TopicReqModel requestObj)
        {
            int languageId = LanguageHelper.GetIdByLangCode(requestObj.StoreId, requestObj.UniqueSeoCode, dbcontext);
            AllStoreLanguagesResponseModel respObj = new AllStoreLanguagesResponseModel();
            if (string.IsNullOrEmpty(requestObj.ApiKey))
            {
                respObj.ErrorMessageTitle = "Error!!";
                respObj.ErrorMessage = LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
                respObj.Status = false;
                respObj.StatusCode = (int)HttpStatusCode.NoContent;
                respObj.ResponseObj = null;
                return respObj;
            }
            var versionInfo = dbcontext.VersionInfo.FirstOrDefault(x => x.StoreId == requestObj.StoreId && x.Apikey == requestObj.ApiKey);
            if (versionInfo == null)
            {
                respObj.ErrorMessageTitle = "Error!!";
                respObj.ErrorMessage = LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                respObj.Status = false;
                respObj.StatusCode = (int)HttpStatusCode.Unauthorized;
                respObj.ResponseObj = null;
                return respObj;
            }
            try
            {
                requestObj.TopicType = requestObj.TopicType?.ToUpper();
                var dataTopicDetails = LanguageHelper.GetTopicContent(versionInfo.GetHelpUrl, versionInfo.Privacy, versionInfo.SupportChatUrl, versionInfo.TermsCondition, requestObj.TopicType, languageId, requestObj.StoreId, dbcontext);
                respObj.ErrorMessageTitle = "Success!!";
                respObj.ErrorMessage = LanguageHelper.GetResourseValueByName(requestObj.StoreId, "API.GetAllStoreLanguages.SuccessMessage", languageId, dbcontext);
                respObj.Status = true;
                respObj.StatusCode = 200;
                respObj.ResponseObj = dataTopicDetails;
                return respObj;

            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                respObj.ErrorMessageTitle = "Error!!";
                respObj.ErrorMessage = ex.Message;
                respObj.Status = false;
                respObj.StatusCode = 400;
                respObj.ResponseObj = null;
                return respObj;
            }
        }
    }
}
