﻿using NHKCustomerApplication.Models;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Xml;

namespace NHKCustomerApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderCheckoutController : ControllerBase
    {
        /// <summary>
        /// The key of the settings to save fixed tax rate of the tax category
        /// </summary>
        public const string FixedRateSettingsKey = "Tax.TaxProvider.FixedOrByCountryStateZip.TaxCategoryId{0}";

        //private readonly string ChefBaseUrl = "http://nhkdev.nestorhawk.com/images/thumbs/";
        private ecuadordevContext dbcontext;
        

        public OrderCheckoutController(ecuadordevContext context)
        {
            dbcontext = context;
        }
        [Route("~/api/v1/AddProductToCart")]
        [HttpPost]
        public CustomerAPIResponses AddProductToCart_Catalogv3(AddProductToCartv2 addProductToCart)
        {
            int languageId = LanguageHelper.GetIdByLangCode(addProductToCart.StoreId, addProductToCart.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            try
            {
                if (addProductToCart == null)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                if (string.IsNullOrEmpty(addProductToCart.ApiKey))
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                if (addProductToCart.CustomerId == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                if (addProductToCart.StoreId == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                int CurrStoreId = addProductToCart.StoreId;
                int vendorId = 0;
                if (addProductToCart.ProductId != 0)
                {
                    var isAttributeRequired = dbcontext.ProductProductAttributeMapping.Where(x => x.ProductId == addProductToCart.ProductId).Any() ?
                         dbcontext.ProductProductAttributeMapping.Where(x => x.ProductId == addProductToCart.ProductId).FirstOrDefault().IsRequired : false;
                    if (isAttributeRequired)
                    {
                        if (!addProductToCart.ProductAttributes.Any())
                        {
                            customerAPIResponses.ErrorMessageTitle = "Error!!";
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.SelectAtleastOneAttribute", languageId, dbcontext);
                            customerAPIResponses.Status = false;
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                            customerAPIResponses.ResponseObj = null;
                            return customerAPIResponses;
                        }
                    }
                }

                if (!addProductToCart.IsRestnCookChanged && addProductToCart.ProductId != 0)
                {
                    var isProductBelongTosameChef = dbcontext.ShoppingCartItem.Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId).FirstOrDefault();
                    if (isProductBelongTosameChef != null)
                    {
                        var VendorBelongtoProduct = (from pro in dbcontext.Product
                                                     where pro.Id == isProductBelongTosameChef.ProductId
                                                     select new
                                                     {
                                                         pro.VendorId
                                                     }).FirstOrDefault();
                        var NewVendorBelongtoProduct = (from pro in dbcontext.Product
                                                        where pro.Id == addProductToCart.ProductId
                                                        select new
                                                        {
                                                            pro.VendorId
                                                        }).FirstOrDefault();
                        vendorId = NewVendorBelongtoProduct.VendorId;
                        if (VendorBelongtoProduct != null && NewVendorBelongtoProduct.VendorId != VendorBelongtoProduct.VendorId)
                        {
                            var vendor = dbcontext.Vendor.Where(x => x.Id == VendorBelongtoProduct.VendorId).FirstOrDefault();
                            var Newvendor = dbcontext.Vendor.Where(x => x.Id == NewVendorBelongtoProduct.VendorId).FirstOrDefault();
                            customerAPIResponses.ErrorMessageTitle = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.Replacecart", languageId, dbcontext);
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.Yourcartcontainsitemsfrom", languageId, dbcontext) +" "+ vendor.Name +"."+" "+ LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.Doyouwanttodiscardtheselectionandadditemsfrom", languageId, dbcontext)+" " + Newvendor.Name + "?";
                            customerAPIResponses.Status = false;
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.Conflict;
                            customerAPIResponses.ResponseObj = null;
                            return customerAPIResponses;
                        }
                    }
                }
                if (addProductToCart.IsRestnCookChanged)
                {

                    var cartcollectiontoDelete = dbcontext.ShoppingCartItem.Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId).ToList();
                    if (cartcollectiontoDelete.Any())
                    {
                        foreach (var item in cartcollectiontoDelete)
                        {
                            dbcontext.Entry(item).State = EntityState.Deleted;
                            dbcontext.SaveChanges();
                        }
                    }
                }
                var result = string.Empty;
                if (addProductToCart.ProductAttributes != null)
                    foreach (var item in addProductToCart.ProductAttributes)
                    {

                        var xmlDoc = new XmlDocument();
                        if (string.IsNullOrEmpty(result))
                        {
                            var element1 = xmlDoc.CreateElement("Attributes");
                            xmlDoc.AppendChild(element1);
                        }
                        else
                        {
                            xmlDoc.LoadXml(result);
                        }
                        var rootElement = (XmlElement)xmlDoc.SelectSingleNode(@"//Attributes");

                        XmlElement attributeElement = null;
                        //find existing
                        var nodeList1 = xmlDoc.SelectNodes(@"//Attributes/ProductAttribute");
                        foreach (XmlNode node1 in nodeList1)
                        {
                            if (node1.Attributes != null && node1.Attributes["ID"] != null)
                            {
                                var str1 = node1.Attributes["ID"].InnerText.Trim();
                                if (int.TryParse(str1, out int id))
                                {
                                    if (id == item.ProductAttributeMappingId)
                                    {
                                        attributeElement = (XmlElement)node1;
                                        break;
                                    }
                                }
                            }
                        }

                        //create new one if not found
                        if (attributeElement == null)
                        {
                            attributeElement = xmlDoc.CreateElement("ProductAttribute");
                            attributeElement.SetAttribute("ID", item.ProductAttributeMappingId.ToString());
                            rootElement.AppendChild(attributeElement);
                        }
                        var attributeValueElement = xmlDoc.CreateElement("ProductAttributeValue");
                        attributeElement.AppendChild(attributeValueElement);

                        var attributeValueValueElement = xmlDoc.CreateElement("Value");
                        attributeValueValueElement.InnerText = item.ProductAttributeValueId.ToString();
                        attributeValueElement.AppendChild(attributeValueValueElement);

                    
                        result = xmlDoc.OuterXml;
                    }
                var cartcollection = dbcontext.ShoppingCartItem.Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId && x.ProductId == addProductToCart.ProductId).ToList();
                if (cartcollection.Any())
                {
                    bool flag = false;
                    foreach (var item in cartcollection)
                    {

                        if (result.Equals(item.AttributesXml))
                        {
                            flag = true;
                            item.Quantity = item.Quantity != 0 ? item.Quantity + addProductToCart.quantity : addProductToCart.quantity;
                            item.UpdatedOnUtc = DateTime.UtcNow;
                            dbcontext.Entry(item).State = EntityState.Modified;
                            dbcontext.SaveChanges();
                        }
                    }
                    if (!flag)
                    {
                        ShoppingCartItem shoppingCartItemModel = new ShoppingCartItem();
                        shoppingCartItemModel.ProductId = addProductToCart.ProductId;
                        shoppingCartItemModel.StoreId = addProductToCart.StoreId;
                        shoppingCartItemModel.ShoppingCartTypeId = (int)ShoppingCartType.ShoppingCart;
                        shoppingCartItemModel.Quantity = addProductToCart.quantity;
                        shoppingCartItemModel.CustomerId = addProductToCart.CustomerId;

                        shoppingCartItemModel.AttributesXml = result;
                        shoppingCartItemModel.CreatedOnUtc = DateTime.UtcNow;
                        shoppingCartItemModel.UpdatedOnUtc = DateTime.UtcNow;
                        shoppingCartItemModel.Customer = dbcontext.Customer.Where(x => x.Id == addProductToCart.CustomerId).FirstOrDefault();
                       
                        dbcontext.ShoppingCartItem.Add(shoppingCartItemModel);
                        dbcontext.SaveChanges();
                    }
                }
                else
                {
                    if (addProductToCart.ProductId != 0)
                    {
                        ShoppingCartItem shoppingCartItemModel = new ShoppingCartItem();
                        shoppingCartItemModel.ProductId = addProductToCart.ProductId;
                        shoppingCartItemModel.StoreId = addProductToCart.StoreId;
                        shoppingCartItemModel.ShoppingCartTypeId = (int)ShoppingCartType.ShoppingCart;
                        shoppingCartItemModel.Quantity = addProductToCart.quantity;
                        shoppingCartItemModel.CustomerId = addProductToCart.CustomerId;
                        shoppingCartItemModel.AttributesXml = result;
                        shoppingCartItemModel.CreatedOnUtc = DateTime.UtcNow;
                        shoppingCartItemModel.UpdatedOnUtc = DateTime.UtcNow;
                        shoppingCartItemModel.Customer = dbcontext.Customer.Where(x => x.Id == addProductToCart.CustomerId).FirstOrDefault();
                        dbcontext.ShoppingCartItem.Add(shoppingCartItemModel);
                        dbcontext.SaveChanges();
                    }


                }
                                
                string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == addProductToCart.StoreId).FirstOrDefault().Value;
                string CurrencySymbol = Helper.GetStoreCurrency(addProductToCart.StoreId, context: dbcontext);
                var CartItems = (from p in dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId)
                                 join pro in dbcontext.Product.AsEnumerable()
                                 on p.ProductId equals pro.Id
                                 select new ProductDetailModel
                                 {
                                     ProductAttribute = p.AttributesXml,
                                     CartItemId = p.Id,
                                     Quantity = p.Quantity,
                                     ProductId = p.ProductId,
                                     ProductName = pro.Name,
                                    
                                     ProductPrice = pro.TaxCategoryId == 0
                                                                        ? pro.Price.ToString() : (from priceCategoryTax in dbcontext.Product.AsEnumerable()
                                                                                                  join categoryTax in dbcontext.TaxRate.AsEnumerable()
                                                                                                  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
                                                                                                  where categoryTax.StoreId == addProductToCart.StoreId &&
                                                                                                  priceCategoryTax.Id == pro.Id

                                                                                                  select new
                                                                                                  {
                                                                                                      PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
                                                                                                  }).FirstOrDefault().PriceCalculated.ToString(),
                                     ProductImageURL = dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == pro.Id).FirstOrDefault() != null ? dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == pro.Id).FirstOrDefault().PictureId.ToString() : "0",
                                      
            }).ToList();
                if (CartItems.Any())
                {
                    ProductDetailRootModel productDetailRoot = new ProductDetailRootModel();
                    productDetailRoot.CurrencySymbol = CurrencySymbol;
                    productDetailRoot.Tip = Helper.ConvertdecimaltoUptotwoPlaces(0);

                    productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(addProductToCart.TipAmount);
                    var ProductId = CartItems.FirstOrDefault().ProductId;
                    var VendorId = dbcontext.Product.Where(x => x.Id == ProductId).FirstOrDefault().VendorId;

                    
                    var deliveryCharges = dbcontext.Vendor.Where(x => x.Id == VendorId).FirstOrDefault();
                    if (deliveryCharges != null && addProductToCart.IsDelivery && !addProductToCart.IsTakeaway)
                    {
                        productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(deliveryCharges.DeliveryCharge));

                    }
                    else
                    {
                        productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(0);
                    }
                                        
                    productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(0);
                    productDetailRoot.Coupon = "";
                    productDetailRoot.CartProducts = new List<ProductDetailModel>();
                    var customer = dbcontext.Customer.Where(i => i.Id == addProductToCart.CustomerId).FirstOrDefault();
                    foreach (var item in CartItems)
                    {
                        var attributesTotalPrice = decimal.Zero;
                        var attributeValues = Helper.ParseProductAttributeValues(item.ProductAttribute, 0, dbcontext);
                        if (attributeValues != null)
                        {
                            foreach (var attributeValue in attributeValues)
                            {
                                attributesTotalPrice += Helper.GetProductAttributeValuePriceAdjustment(attributeValue, dbcontext);
                            }
                        }
                        item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice) + attributesTotalPrice);
                        int pictureSize = 75;
                        var setting = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
                        if (setting != null)
                        {
                            pictureSize = Convert.ToInt32(setting.Value);
                        }
                        if (!string.IsNullOrEmpty(item.ProductImageURL))
                        {
                            if (item.ProductImageURL != "0")
                            {
                                int pictureId = Convert.ToInt32(item.ProductImageURL);
                                var pictureImage = dbcontext.Picture.AsEnumerable().Where(x => x.Id == pictureId).FirstOrDefault();

                                string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
                                string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
                                ? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
                                : $"{pictureId:0000000}_{pictureSize}.{lastPart}";
                                item.ProductImageURL = ChefBaseUrl + thumbFileName;
                            }
                            else
                            {
                                item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                            }
                        }
                        else
                        {
                            item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                        }

                        decimal Tax = 0, ProductTaxPrice=0;
                        var product = dbcontext.Product.Where(x => x.Id == item.ProductId).FirstOrDefault();
                        GetProductPrice(product, 0, product.Price + attributesTotalPrice, true, customer, true, CurrStoreId, out Tax, out ProductTaxPrice, item.Quantity);
                        item.ProductAttribute = Helper.FormatAttributes( attributesXml: item.ProductAttribute, context: dbcontext);
                        item.ProductAttributeAvailable = !string.IsNullOrEmpty(item.ProductAttribute);
                        item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice));
                        item.ProductAttributePrice = Helper.ConvertdecimaltoUptotwoPlaces(attributesTotalPrice);
                        item.ProductAttributeValue = item.ProductAttribute + " (+" + CurrencySymbol + "" + item.ProductAttributePrice + ")";
                        productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Tax) + ProductTaxPrice);
                        productDetailRoot.Subtotal = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal) + (Convert.ToDecimal(item.ProductPrice) * item.Quantity));                                                              
                        productDetailRoot.CartProducts.Add(item);
                    }
                    if (!string.IsNullOrEmpty(addProductToCart.CouponCode))
                    {
                        var response = Helper.ApplyCoupon(addProductToCart.CustomerId, addProductToCart.CouponCode, Convert.ToDecimal(productDetailRoot.Subtotal), dbcontext, addProductToCart.StoreId, addProductToCart.languageId);
                        if (response.Status)
                        {
                            var responseDiscount = (DiscountModel)response.ResponseObj;
                            productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(responseDiscount.DiscountAmount);
                            productDetailRoot.DiscountId = responseDiscount.DiscountId;
                            productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                        }
                        else
                        {
                            productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                            productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                            response.ResponseObj = productDetailRoot;
                            return response;
                        }

                    }
                    else
                    {
                        productDetailRoot.DiscountCode = "";
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                    }
                    productDetailRoot.EstTime = "20-25 Minutes";
                    productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal)+ Convert.ToDecimal(productDetailRoot.Tax) + Convert.ToDecimal(productDetailRoot.DeliveryCharges) + Convert.ToDecimal(productDetailRoot.Tip) + (Convert.ToDecimal(productDetailRoot.Total) - Convert.ToDecimal(productDetailRoot.DiscountAmount)));
                    customerAPIResponses.ErrorMessageTitle = "Success!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ProductList", languageId, dbcontext);
                    customerAPIResponses.Status = true;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                    customerAPIResponses.ResponseObj = productDetailRoot;
                    return customerAPIResponses;
                }
                else
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.NoProductsInCart", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
        }
        [Route("~/api/v2/AddProductonCart")]
        [HttpPost]
        public CustomerAPIResponses AddProductonCart(AddRemoveProductCart addProductToCart)
        {
            int languageId = LanguageHelper.GetIdByLangCode(addProductToCart.StoreId, addProductToCart.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (addProductToCart == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(addProductToCart.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.StoreId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }


            int CurrStoreId = addProductToCart.StoreId;
            var cartcollection = dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId && x.Id == addProductToCart.CartItemId).ToList();
            if (cartcollection.Any())
            {
                foreach (var item in cartcollection)
                {

                    item.Quantity = item.Quantity + 1;
                    item.UpdatedOnUtc = DateTime.UtcNow;
                    if (item.Quantity <= 0)
                    {
                        dbcontext.Entry(item).State = EntityState.Deleted;
                    }
                    else
                    {
                        dbcontext.Entry(item).State = EntityState.Modified;
                    }

                    dbcontext.SaveChanges();
                }
            }
            string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == addProductToCart.StoreId).FirstOrDefault().Value;
            string CurrencySymbol = Helper.GetStoreCurrency(addProductToCart.StoreId, dbcontext);
            var CartItems = (from p in dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId)
                             join pro in dbcontext.Product.AsEnumerable()
                             on p.ProductId equals pro.Id
                             select new ProductDetailModel
                             {
                                 CartItemId = p.Id,
                                 ProductAttribute = p.AttributesXml,
                                 Quantity = p.Quantity,
                                 ProductId = p.ProductId,
                                 ProductName = pro.Name,
                                 ProductPrice = pro.TaxCategoryId == 0
                                                                    ? pro.Price.ToString() : (from priceCategoryTax in dbcontext.Product.AsEnumerable()
                                                                                              join categoryTax in dbcontext.TaxRate.AsEnumerable()
                                                                                                  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
                                                                                              where categoryTax.StoreId == addProductToCart.StoreId &&
                                                                                                  priceCategoryTax.Id == pro.Id

                                                                                              select new
                                                                                              {
                                                                                                  PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
                                                                                              }).FirstOrDefault().PriceCalculated.ToString(),

                                 ProductImageURL = dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == pro.Id).FirstOrDefault() != null ? dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == pro.Id).FirstOrDefault().PictureId.ToString() : "0"
                             }).ToList();
            if (CartItems.Any())
            {
                ProductDetailRootModel productDetailRoot = new ProductDetailRootModel();
                productDetailRoot.CurrencySymbol = CurrencySymbol;
                productDetailRoot.Tip = Helper.ConvertdecimaltoUptotwoPlaces(addProductToCart.TipAmount);
                productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(0);

                var MinTimeForCheckout = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext);
                var Id = CartItems.FirstOrDefault().ProductId;
                var VendorId = dbcontext.Product.Where(x => x.Id == Id).FirstOrDefault().VendorId;
                                            
                var deliveryCharges = dbcontext.Vendor.Where(x => x.Id == VendorId).FirstOrDefault();
                if (deliveryCharges != null && addProductToCart.IsDelivery == true && addProductToCart.IsTakeAway == false)
                {
                    productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(deliveryCharges.DeliveryCharge));

                }
                else
                {
                    productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
               

                var ProductId = CartItems.FirstOrDefault().ProductId;
                              
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(0);
                productDetailRoot.Coupon = "";
                productDetailRoot.CartProducts = new List<ProductDetailModel>();
                string cutoffTime = string.Empty;
                string HolidayStart = string.Empty;
                string HolidayEnd = string.Empty;
                string StartTime = string.Empty;
                string EndTime = string.Empty;

                var customer = dbcontext.Customer.Where(i => i.Id == addProductToCart.CustomerId).FirstOrDefault();
                foreach (var item in CartItems)
                {
                    var attributesTotalPrice = decimal.Zero;
                    var attributeValues = Helper.ParseProductAttributeValues(item.ProductAttribute, context: dbcontext);
                    if (attributeValues != null)
                    {
                        foreach (var attributeValue in attributeValues)
                        {
                            attributesTotalPrice += Helper.GetProductAttributeValuePriceAdjustment(attributeValue, dbcontext);
                        }
                    }

                    decimal Tax = 0, ProductTaxPrice=0;
                    var product = dbcontext.Product.Where(x => x.Id == item.ProductId).FirstOrDefault();
                    GetProductPrice(product, 0, product.Price + attributesTotalPrice, true, customer, true, CurrStoreId, out Tax,  out ProductTaxPrice, item.Quantity);

                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice) + attributesTotalPrice);

                    item.ProductTaxPrice = Tax;

                    int pictureSize = 75;
                    var setting = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
                    if (setting != null)
                    {
                        pictureSize = Convert.ToInt32(setting.Value);
                    }
                    if (!string.IsNullOrEmpty(item.ProductImageURL))
                    {
                        if (item.ProductImageURL != "0")
                        {
                            int pictureId = Convert.ToInt32(item.ProductImageURL);
                            var pictureImage = dbcontext.Picture.AsEnumerable().Where(x => x.Id == pictureId).FirstOrDefault();

                            string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
                            string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
                            ? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
                            : $"{pictureId:0000000}_{pictureSize}.{lastPart}";
                            item.ProductImageURL = ChefBaseUrl + thumbFileName;
                        }
                        else
                        {
                            item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                        }
                    }
                    else
                    {
                        item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                    }
                    item.ProductAttribute = Helper.FormatAttributes(/*dbcontext.Products.Where(x => x.Id == item.ProductId).FirstOrDefault()*/ item.ProductAttribute, context: dbcontext);
                    item.ProductAttributeAvailable = !string.IsNullOrEmpty(item.ProductAttribute);
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice));
                    item.ProductAttributePrice = Helper.ConvertdecimaltoUptotwoPlaces(attributesTotalPrice);
                    item.ProductAttributeValue = item.ProductAttribute + " (+" + CurrencySymbol + "" + item.ProductAttributePrice + ")";             

                    productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Tax) + ProductTaxPrice);
                    productDetailRoot.Subtotal = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal) + (Convert.ToDecimal(item.ProductPrice) * item.Quantity));
                    productDetailRoot.CartProducts.Add(item);
                }


                List<Product> lstproducts = new List<Product>();
                foreach (var prod in CartItems.Select(x => x.ProductId).ToList())
                {
                    lstproducts.Add(dbcontext.Product.Where(x => x.Id == Convert.ToInt32(prod)).FirstOrDefault());
                }


                productDetailRoot.AppliedTaxRates = GetAppliedTaxRates(customer, lstproducts, CurrStoreId);

                if (!string.IsNullOrEmpty(addProductToCart.CouponCode))
                {
                    var response = Helper.ApplyCoupon(addProductToCart.CustomerId, addProductToCart.CouponCode, Convert.ToDecimal(productDetailRoot.Subtotal), dbcontext,addProductToCart.StoreId,languageId);
                    if (response.Status)
                    {
                        var responseDiscount = (DiscountModel)response.ResponseObj;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(responseDiscount.DiscountAmount);
                        productDetailRoot.DiscountId = responseDiscount.DiscountId;
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                    }
                    else
                    {
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                        response.ResponseObj = productDetailRoot;
                        return response;
                    }

                }
                else
                {
                    productDetailRoot.DiscountCode = "";
                    productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
                productDetailRoot.EstTime = "20-25 Minutes";
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal) + Convert.ToDecimal(productDetailRoot.Tax) + Convert.ToDecimal(productDetailRoot.DeliveryCharges) + Convert.ToDecimal(productDetailRoot.Tip) + (Convert.ToDecimal(productDetailRoot.Total) - Convert.ToDecimal(productDetailRoot.DiscountAmount)));
                customerAPIResponses.ErrorMessageTitle = "Success!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ProductList", languageId, dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                customerAPIResponses.ResponseObj = productDetailRoot;
                return customerAPIResponses;
            }
            else
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.NoProductsInCart", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                customerAPIResponses.ResponseObj = null;
            }

            return customerAPIResponses;


        }

        [Route("~/api/v2.1/AddProductonCart")]
        [HttpPost]
        public CustomerAPIResponses AddProductonCartV2_1(AddRemoveProductCart addProductToCart)
        {
            int languageId = LanguageHelper.GetIdByLangCode(addProductToCart.StoreId, addProductToCart.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (addProductToCart == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(addProductToCart.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.StoreId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }


            int CurrStoreId = addProductToCart.StoreId;
            var cartcollection = dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId && x.Id == addProductToCart.CartItemId).ToList();
            if (cartcollection.Any())
            {
                foreach (var item in cartcollection)
                {

                    item.Quantity = item.Quantity + 1;
                    item.UpdatedOnUtc = DateTime.UtcNow;
                    if (item.Quantity <= 0)
                    {
                        dbcontext.Entry(item).State = EntityState.Deleted;
                    }
                    else
                    {
                        dbcontext.Entry(item).State = EntityState.Modified;
                    }

                    dbcontext.SaveChanges();
                }
            }
            string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == addProductToCart.StoreId).FirstOrDefault().Value;
            string CurrencySymbol = Helper.GetStoreCurrency(addProductToCart.StoreId, dbcontext);
            var CartItems = (from p in dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId)
                             join pro in dbcontext.Product.AsEnumerable()
                             on p.ProductId equals pro.Id
                             select new ProductDetailModel
                             {
                                 CartItemId = p.Id,
                                 ProductAttribute = p.AttributesXml,
                                 Quantity = p.Quantity,
                                 ProductId = p.ProductId,
                                 ProductName = pro.Name,
                                 ProductPrice = pro.TaxCategoryId == 0
                                                                    ? pro.Price.ToString() : (from priceCategoryTax in dbcontext.Product.AsEnumerable()
                                                                                              join categoryTax in dbcontext.TaxRate.AsEnumerable()
                                                                                                  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
                                                                                              where categoryTax.StoreId == addProductToCart.StoreId &&
                                                                                                  priceCategoryTax.Id == pro.Id

                                                                                              select new
                                                                                              {
                                                                                                  PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
                                                                                              }).FirstOrDefault().PriceCalculated.ToString(),

                                 ProductImageURL = dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == pro.Id).FirstOrDefault() != null ? dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == pro.Id).FirstOrDefault().PictureId.ToString() : "0"
                             }).ToList();
            if (CartItems.Any())
            {
                ProductDetailRootModel productDetailRoot = new ProductDetailRootModel();
                productDetailRoot.CurrencySymbol = CurrencySymbol;
                productDetailRoot.Tip = Helper.ConvertdecimaltoUptotwoPlaces(addProductToCart.TipAmount);
                productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(0);

                var MinTimeForCheckout = Helper.ConvertToUserTimeV2_1(DateTime.UtcNow, DateTimeKind.Utc, dbcontext, addProductToCart.CustomerId);
                var Id = CartItems.FirstOrDefault().ProductId;
                var VendorId = dbcontext.Product.Where(x => x.Id == Id).FirstOrDefault().VendorId;

                var deliveryCharges = dbcontext.Vendor.Where(x => x.Id == VendorId).FirstOrDefault();
                if (deliveryCharges != null && addProductToCart.IsDelivery == true && addProductToCart.IsTakeAway == false)
                {
                    productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(deliveryCharges.DeliveryCharge));

                }
                else
                {
                    productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }


                var ProductId = CartItems.FirstOrDefault().ProductId;
                                
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(0);
                productDetailRoot.Coupon = "";
                productDetailRoot.CartProducts = new List<ProductDetailModel>();
                string cutoffTime = string.Empty;
                string HolidayStart = string.Empty;
                string HolidayEnd = string.Empty;
                string StartTime = string.Empty;
                string EndTime = string.Empty;

                var customer = dbcontext.Customer.Where(i => i.Id == addProductToCart.CustomerId).FirstOrDefault();
                foreach (var item in CartItems)
                {
                    var attributesTotalPrice = decimal.Zero;
                    var attributeValues = Helper.ParseProductAttributeValues(item.ProductAttribute, context: dbcontext);
                    if (attributeValues != null)
                    {
                        foreach (var attributeValue in attributeValues)
                        {
                            attributesTotalPrice += Helper.GetProductAttributeValuePriceAdjustment(attributeValue, dbcontext);
                        }
                    }

                    decimal Tax = 0, ProductTaxPrice=0;
                    var product = dbcontext.Product.Where(x => x.Id == item.ProductId).FirstOrDefault();
                    GetProductPrice(product, 0, product.Price + attributesTotalPrice, true, customer, true, CurrStoreId, out Tax, out ProductTaxPrice, item.Quantity);
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice) + attributesTotalPrice);
                    item.ProductTaxPrice = Tax;
                    int pictureSize = 75;
                    var setting = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
                    if (setting != null)
                    {
                        pictureSize = Convert.ToInt32(setting.Value);
                    }
                    if (!string.IsNullOrEmpty(item.ProductImageURL))
                    {
                        if (item.ProductImageURL != "0")
                        {
                            int pictureId = Convert.ToInt32(item.ProductImageURL);
                            var pictureImage = dbcontext.Picture.AsEnumerable().Where(x => x.Id == pictureId).FirstOrDefault();

                            string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
                            string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
                            ? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
                            : $"{pictureId:0000000}_{pictureSize}.{lastPart}";
                            item.ProductImageURL = ChefBaseUrl + thumbFileName;
                        }
                        else
                        {
                            item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                        }
                    }
                    else
                    {
                        item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                    }
                    item.ProductAttribute = Helper.FormatAttributes(item.ProductAttribute, context: dbcontext);
                    item.ProductAttributeAvailable = !string.IsNullOrEmpty(item.ProductAttribute);
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice));
                    item.ProductAttributePrice = Helper.ConvertdecimaltoUptotwoPlaces(attributesTotalPrice);
                    item.ProductAttributeValue = item.ProductAttribute + " (+" + CurrencySymbol + "" + item.ProductAttributePrice + ")";                    
                    productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Tax) + ProductTaxPrice);
                    productDetailRoot.Subtotal = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal) + (Convert.ToDecimal(item.ProductPrice) * item.Quantity));
                    productDetailRoot.CartProducts.Add(item);
                }


                List<Product> lstproducts = new List<Product>();
                foreach (var prod in CartItems.Select(x => x.ProductId).ToList())
                {
                    lstproducts.Add(dbcontext.Product.Where(x => x.Id == Convert.ToInt32(prod)).FirstOrDefault());
                }


                productDetailRoot.AppliedTaxRates = GetAppliedTaxRates(customer, lstproducts, CurrStoreId);

                if (!string.IsNullOrEmpty(addProductToCart.CouponCode))
                {
                    var response = Helper.ApplyCoupon(addProductToCart.CustomerId, addProductToCart.CouponCode, Convert.ToDecimal(productDetailRoot.Subtotal), dbcontext, addProductToCart.StoreId, addProductToCart.languageId);
                    if (response.Status)
                    {
                        var responseDiscount = (DiscountModel)response.ResponseObj;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(responseDiscount.DiscountAmount);
                        productDetailRoot.DiscountId = responseDiscount.DiscountId;
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                    }
                    else
                    {
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                        response.ResponseObj = productDetailRoot;
                        return response;
                    }

                }
                else
                {
                    productDetailRoot.DiscountCode = "";
                    productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
                productDetailRoot.EstTime = "20-25 Minutes";
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal) + Convert.ToDecimal(productDetailRoot.Tax) + Convert.ToDecimal(productDetailRoot.DeliveryCharges) + Convert.ToDecimal(productDetailRoot.Tip) + (Convert.ToDecimal(productDetailRoot.Total) - Convert.ToDecimal(productDetailRoot.DiscountAmount)));
                customerAPIResponses.ErrorMessageTitle = "Success!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ProductList", languageId, dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                customerAPIResponses.ResponseObj = productDetailRoot;
                return customerAPIResponses;
            }
            else
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.NoProductsInCart", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                customerAPIResponses.ResponseObj = null;
            }

            return customerAPIResponses;
        }
        [Route("~/api/v2.2/AddProductonCart")]
        [HttpPost]
        public CustomerAPIResponses AddProductonCartV2_2(AddRemoveProductCart addProductToCart)
        {
            int languageId = LanguageHelper.GetIdByLangCode(addProductToCart.StoreId, addProductToCart.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (addProductToCart == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(addProductToCart.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.StoreId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }


            int CurrStoreId = addProductToCart.StoreId;
            var cartcollection = dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId && x.Id == addProductToCart.CartItemId).ToList();
            if (cartcollection.Any())
            {
                foreach (var item in cartcollection)
                {

                    item.Quantity = item.Quantity + 1;
                    item.UpdatedOnUtc = DateTime.UtcNow;
                    if (item.Quantity <= 0)
                    {
                        dbcontext.Entry(item).State = EntityState.Deleted;
                    }
                    else
                    {
                        dbcontext.Entry(item).State = EntityState.Modified;
                    }

                    dbcontext.SaveChanges();
                }
            }
            string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == addProductToCart.StoreId).FirstOrDefault().Value;
            string CurrencySymbol = Helper.GetStoreCurrency(addProductToCart.StoreId, dbcontext);
            var CartItems = (from p in dbcontext.ShoppingCartItem.AsEnumerable().Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId)
                             join pro in dbcontext.Product.AsEnumerable()
                             on p.ProductId equals pro.Id
                             select new ProductDetailModel
                             {
                                 CartItemId = p.Id,
                                 ProductAttribute = p.AttributesXml,
                                 Quantity = p.Quantity,
                                 ProductId = p.ProductId,
                                 ProductName = pro.Name,
                                 ProductPrice = pro.TaxCategoryId == 0
                                                                    ? pro.Price.ToString() : (from priceCategoryTax in dbcontext.Product.AsEnumerable()
                                                                                              join categoryTax in dbcontext.TaxRate.AsEnumerable()
                                                                                                  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
                                                                                              where categoryTax.StoreId == addProductToCart.StoreId &&
                                                                                                  priceCategoryTax.Id == pro.Id

                                                                                              select new
                                                                                              {
                                                                                                  PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
                                                                                              }).FirstOrDefault().PriceCalculated.ToString(),

                                 ProductImageURL = dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == pro.Id).FirstOrDefault() != null ? dbcontext.ProductPictureMapping.AsEnumerable().Where(x => x.ProductId == pro.Id).FirstOrDefault().PictureId.ToString() : "0"
                             }).ToList();
            if (CartItems.Any())
            {
                ProductDetailRootModel productDetailRoot = new ProductDetailRootModel();
                productDetailRoot.CurrencySymbol = CurrencySymbol;
                productDetailRoot.Tip = Helper.ConvertdecimaltoUptotwoPlaces(addProductToCart.TipAmount);
                productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(0);

                var MinTimeForCheckout = Helper.ConvertToUserTime(DateTime.UtcNow, DateTimeKind.Utc, dbcontext);
                var Id = CartItems.FirstOrDefault().ProductId;
                var VendorId = dbcontext.Product.Where(x => x.Id == Id).FirstOrDefault().VendorId;

                var deliveryCharges = dbcontext.Vendor.Where(x => x.Id == VendorId).FirstOrDefault();
                int deliveryAddressId = addProductToCart.deliveryAddressId != 0  ? addProductToCart.deliveryAddressId : deliveryCharges.AddressId;
                if (deliveryCharges != null && addProductToCart.IsDelivery == true && addProductToCart.IsTakeAway == false)
                {
                    productDetailRoot.DeliveryCharges = Convert.ToString(Helper.CalculateDeliveryFee(VendorId, deliveryAddressId, addProductToCart.StoreId, dbcontext));

                }
                else
                {
                    productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
                

                var ProductId = CartItems.FirstOrDefault().ProductId;

                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(0);
                productDetailRoot.Coupon = "";
                productDetailRoot.CartProducts = new List<ProductDetailModel>();
                string cutoffTime = string.Empty;
                string HolidayStart = string.Empty;
                string HolidayEnd = string.Empty;
                string StartTime = string.Empty;
                string EndTime = string.Empty;

                var customer = dbcontext.Customer.Where(i => i.Id == addProductToCart.CustomerId).FirstOrDefault();
                foreach (var item in CartItems)
                {
                    var attributesTotalPrice = decimal.Zero;
                    var attributeValues = Helper.ParseProductAttributeValues(item.ProductAttribute, context: dbcontext);
                    if (attributeValues != null)
                    {
                        foreach (var attributeValue in attributeValues)
                        {
                            attributesTotalPrice += Helper.GetProductAttributeValuePriceAdjustment(attributeValue, dbcontext);
                        }
                    }

                    decimal Tax = 0, ProductTaxPrice = 0;
                    var product = dbcontext.Product.Where(x => x.Id == item.ProductId).FirstOrDefault();
                    GetProductPrice(product, 0, product.Price + attributesTotalPrice, true, customer, true, CurrStoreId, out Tax, out ProductTaxPrice, item.Quantity);
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice) + attributesTotalPrice);
                    item.ProductTaxPrice = Tax;
                    int pictureSize = 75;
                    var setting = dbcontext.Setting.AsEnumerable().Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
                    if (setting != null)
                    {
                        pictureSize = Convert.ToInt32(setting.Value);
                    }
                    if (!string.IsNullOrEmpty(item.ProductImageURL))
                    {
                        if (item.ProductImageURL != "0")
                        {
                            int pictureId = Convert.ToInt32(item.ProductImageURL);
                            var pictureImage = dbcontext.Picture.AsEnumerable().Where(x => x.Id == pictureId).FirstOrDefault();

                            string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
                            string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
                            ? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
                            : $"{pictureId:0000000}_{pictureSize}.{lastPart}";
                            item.ProductImageURL = ChefBaseUrl + thumbFileName;
                        }
                        else
                        {
                            item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                        }
                    }
                    else
                    {
                        item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                    }
                    item.ProductAttribute = Helper.FormatAttributes(item.ProductAttribute, context: dbcontext);
                    item.ProductAttributeAvailable = !string.IsNullOrEmpty(item.ProductAttribute);
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice));
                    item.ProductAttributePrice = Helper.ConvertdecimaltoUptotwoPlaces(attributesTotalPrice);
                    item.ProductAttributeValue = item.ProductAttribute + " (+" + CurrencySymbol + "" + item.ProductAttributePrice + ")";
                    productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Tax) + ProductTaxPrice);
                    productDetailRoot.Subtotal = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal) + (Convert.ToDecimal(item.ProductPrice) * item.Quantity));
                    productDetailRoot.CartProducts.Add(item);
                }


                List<Product> lstproducts = new List<Product>();
                foreach (var prod in CartItems.Select(x => x.ProductId).ToList())
                {
                    lstproducts.Add(dbcontext.Product.Where(x => x.Id == Convert.ToInt32(prod)).FirstOrDefault());
                }

               

                productDetailRoot.AppliedTaxRates = GetAppliedTaxRates(customer, lstproducts, CurrStoreId);

                if (!string.IsNullOrEmpty(addProductToCart.CouponCode))
                {
                    var response = Helper.ApplyCoupon(addProductToCart.CustomerId, addProductToCart.CouponCode, Convert.ToDecimal(productDetailRoot.Subtotal), dbcontext, addProductToCart.StoreId,languageId);
                    if (response.Status)
                    {
                        var responseDiscount = (DiscountModel)response.ResponseObj;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(responseDiscount.DiscountAmount);
                        productDetailRoot.DiscountId = responseDiscount.DiscountId;
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                    }
                    else
                    {
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                        response.ResponseObj = productDetailRoot;
                        return response;
                    }

                }
                else
                {
                    productDetailRoot.DiscountCode = "";
                    productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
                productDetailRoot.ServiceChargeAmount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(Helper.CalculateServiceChargeAmount(Convert.ToDecimal(productDetailRoot.Subtotal), addProductToCart.StoreId, dbcontext)));
                productDetailRoot.EstTime = "20-25 Minutes";
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.DeliveryCharges) + Convert.ToDecimal(productDetailRoot.Tip) + Convert.ToDecimal(productDetailRoot.ServiceChargeAmount) + Convert.ToDecimal(productDetailRoot.Tax) + (Convert.ToDecimal(productDetailRoot.Subtotal) - Convert.ToDecimal(productDetailRoot.DiscountAmount)));
                customerAPIResponses.ErrorMessageTitle = "Success!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ProductList", languageId, dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                customerAPIResponses.ResponseObj = productDetailRoot;
                return customerAPIResponses;
            }
            else
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.NoProductsInCart", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                customerAPIResponses.ResponseObj = null;
            }

            return customerAPIResponses;


        }

        /// <summary>
        /// get applied taxes
        /// </summary>
        /// <param name="products"></param>
        /// <param name="address"></param>
        /// <param name="storeId"></param>
        /// <returns></returns>
        [NonAction]
        public string GetAppliedTaxRate(List<Product> products, Address address, int storeId)
        {
            string result = string.Empty;

            //the tax rate calculation by fixed rate
            var countryStateZipEnabled = Convert.ToBoolean(dbcontext.Setting.Where(s => s.StoreId == storeId && s.Name == "fixedorbycountrystateziptaxsettings.countrystatezipenabled").Select(s => s.Value).FirstOrDefault());

            if (!countryStateZipEnabled || products == null || address == null)
            {
                return result;
            }


            var alltaxRates = dbcontext.TaxRate.Where(s => s.StoreId == 0 || s.StoreId == storeId);


            var allTaxRates = alltaxRates.Select(taxRate => new TaxRateForCaching
            {
                Id = taxRate.Id,
                StoreId = taxRate.StoreId,
                TaxCategoryId = taxRate.TaxCategoryId,
                CountryId = taxRate.CountryId,
                StateProvinceId = taxRate.StateProvinceId,
                Zip = taxRate.Zip,
                Percentage = taxRate.Percentage
            }).ToList();

            var countryId = address.CountryId;
            var stateProvinceId = address.StateProvinceId;
            var zip = address.ZipPostalCode?.Trim() ?? string.Empty;

            //get tax by tax categories
            var taxCategoryIds = new List<int>();
            foreach (var product in products)
            {
                if (!string.IsNullOrEmpty(product?.TaxCategoryIds))
                {
                    var ids = product.TaxCategoryIds.Split(',').Select(x => int.Parse(x)).ToList();
                    taxCategoryIds.AddRange(ids.Where(x => !taxCategoryIds.Contains(x)));
                }
            }
            var existingRates = allTaxRates.Where(taxRate => taxRate.CountryId == countryId && taxCategoryIds.Contains(taxRate.TaxCategoryId));

            //filter by store
            var matchedByStore = existingRates.Where(taxRate => storeId == taxRate.StoreId || taxRate.StoreId == 0);

            //filter by state/province
            var matchedByStateProvince = matchedByStore.Where(taxRate => stateProvinceId == taxRate.StateProvinceId || taxRate.StateProvinceId == 0);

            //filter by zip
            var matchedByZip = matchedByStateProvince.Where(taxRate => string.IsNullOrWhiteSpace(taxRate.Zip) || taxRate.Zip.Equals(zip, StringComparison.InvariantCultureIgnoreCase));

            //sort from particular to general, more particular cases will be the first
            var foundRecords = matchedByZip.OrderBy(r => r.StoreId == 0).ThenBy(r => r.StateProvinceId == 0).ThenBy(r => string.IsNullOrEmpty(r.Zip));

            var foundRecord = foundRecords.ToList();

            if (foundRecord.Count > 0)
            {
                var allTaxCats = dbcontext.TaxCategory.ToList();
                var qry = from t in foundRecord
                          join tc in allTaxCats
                          on t.TaxCategoryId equals tc.Id
                          select ("(" + tc.Name + " " + t.Percentage.ToString("##.##") + "%)");
                result = "(" + string.Join("+", qry) + ")";
            }

            return result;
        }



        /// <summary>
        /// get applied tax rates
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="products"></param>
        /// <param name="address"></param>
        /// <param name="storeId"></param>
        /// <returns></returns>
        [NonAction]
        public virtual string GetAppliedTaxRates(Customer customer, List<Product> products, int storeId)
        {
            var vendorAddressId = (dbcontext.Vendor.Where(x => x.Id == products.FirstOrDefault().VendorId).FirstOrDefault())?.AddressId ?? 0;
            var vendorAddress = dbcontext.Address.Where(x => x.Id == vendorAddressId).FirstOrDefault();
            return GetAppliedTaxRate(products, vendorAddress, storeId);
        }

        /// <summary>
        /// Gets price
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="taxCategoryId">Tax category identifier</param>
        /// <param name="price">Price</param>
        /// <param name="includingTax">A value indicating whether calculated price should include tax</param>
        /// <param name="customer">Customer</param>
        /// <param name="priceIncludesTax">A value indicating whether price already includes tax</param>
        /// <param name="taxRate">Tax rate</param>
        /// <returns>Price</returns>
        /// 



        [NonAction]
        public decimal GetProductTaxPrice(Product product, int taxCategoryId,
            decimal price, bool includingTax, Customer customer,
            bool priceIncludesTax, int storeId, out decimal ProductTaxPrice,int quantity)
        {
            if (price == decimal.Zero)
            {
                ProductTaxPrice = decimal.Zero;
                return ProductTaxPrice;
            }

            string catIds = dbcontext.Product.Where(x => x.Id == product.Id).FirstOrDefault().TaxCategoryIds.ToString();
            string[] catLists = catIds.Split(',');
             ProductTaxPrice = 0.00M;
            Decimal productPrice = dbcontext.Product.Where(x => x.Id == product.Id).FirstOrDefault().Price;
            foreach (var items in catLists)
            {
                int catId = Int32.Parse(items);
                Decimal percentage = (dbcontext.TaxRate.Where(x => x.TaxCategoryId == catId && (x.StoreId==storeId || x.StoreId==0)).FirstOrDefault().Percentage ) / 100;
                ProductTaxPrice = ProductTaxPrice + (productPrice *quantity* percentage);
                

            }

            return ProductTaxPrice;

           

        }
        [NonAction]
        public decimal GetProductPrice(Product product, int taxCategoryId,
            decimal price, bool includingTax, Customer customer,
            bool priceIncludesTax, int storeId, out decimal taxRate, out decimal ProductTaxPrice, decimal Quantity)
        {
            //no need to calculate tax rate if passed "price" is 0
            ProductTaxPrice = decimal.Zero;
            if (price == decimal.Zero)
            {
                taxRate = decimal.Zero;
                ProductTaxPrice = decimal.Zero;
                return taxRate;
            }

            GetTaxRate(product, taxCategoryId, customer, price, storeId, out taxRate, out var isTaxable, out  ProductTaxPrice, Quantity);
           
            if (priceIncludesTax)
            {
                //"price" already includes tax
                if (includingTax)
                {
                    //we should calculate price WITH tax
                    if (!isTaxable)
                    {
                        //but our request is not taxable
                        //hence we should calculate price WITHOUT tax
                        price = CalculatePrice(price, taxRate, false);
                    }
                }
                else
                {
                    //we should calculate price WITHOUT tax
                    price = CalculatePrice(price, taxRate, false);
                }
            }
            else
            {
                //"price" doesn't include tax
                if (includingTax)
                {
                    //we should calculate price WITH tax
                    //do it only when price is taxable
                    if (isTaxable)
                    {
                        price = CalculatePrice(price, taxRate, true);
                    }
                }
            }

            if (!isTaxable)
            {
                //we return 0% tax rate in case a request is not taxable
                taxRate = decimal.Zero;
            }

            //allowed to support negative price adjustments
            //if (price < decimal.Zero)
            //    price = decimal.Zero;

            return price;
        }

        /// <summary>
        /// Calculated price
        /// </summary>
        /// <param name="price">Price</param>
        /// <param name="percent">Percent</param>
        /// <param name="increase">Increase</param>
        /// <returns>New price</returns>
        [NonAction]
        public decimal CalculatePrice(decimal price, decimal percent, bool increase)
        {
            if (percent == decimal.Zero)
                return price;

            decimal result;
            if (increase)
            {
                result = price * (1 + percent / 100);
            }
            else
            {
                result = price - price / (100 + percent) * percent;
            }

            return result;
        }

        /// <summary>
        /// Gets tax rate
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="taxCategoryId">Tax category identifier</param>
        /// <param name="customer">Customer</param>
        /// <param name="price">Price (taxable value)</param>
        /// <param name="taxRate">Calculated tax rate</param>
        /// <param name="isTaxable">A value indicating whether a request is taxable</param>
        [NonAction]
        public void GetTaxRate(Product product, int taxCategoryId,
            Customer customer, decimal price, int storeId, out decimal taxRate, out bool isTaxable, out decimal ProductTaxPrice, decimal Quantity)
        {
            taxRate = decimal.Zero;
            ProductTaxPrice = decimal.Zero;
            isTaxable = true;

            //tax request
            var calculateTaxRequest = CreateCalculateTaxRequest(product, taxCategoryId, customer, price, storeId);

            //tax exempt
            if (IsTaxExempt(product, calculateTaxRequest.Customer))
            {
                isTaxable = false;
            }

            var euvatenabled = Convert.ToBoolean(dbcontext.Setting.Where(s => s.StoreId == storeId && s.Name == "taxsettings.euvatenabled").Select(s => s.Value).FirstOrDefault());

            //make EU VAT exempt validation (the European Union Value Added Tax)
            if (isTaxable &&
                euvatenabled) //&& IsVatExempt(calculateTaxRequest.Address, calculateTaxRequest.Customer))
            {
                //VAT is not chargeable
                isTaxable = false;
            }

            //get tax rate
            var calculateTaxResult = GetTaxRate(calculateTaxRequest, storeId, out  ProductTaxPrice, Quantity);
            if (calculateTaxResult.Success)
            {
                //ensure that tax is equal or greater than zero
                if (calculateTaxResult.TaxRate < decimal.Zero)
                    calculateTaxResult.TaxRate = decimal.Zero;

                taxRate = calculateTaxResult.TaxRate;
            }
        }

        /// <summary>
        /// Gets a value indicating whether a product is tax exempt
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="customer">Customer</param>
        /// <returns>A value indicating whether a product is tax exempt</returns>
        [NonAction]
        public bool IsTaxExempt(Product product, Customer customer)
        {
            if (customer != null)
            {
                if (customer.IsTaxExempt)
                    return true;

                //#TODO RAM
                //if (customer.CustomerRoles.Where(cr => cr.Active).Any(cr => cr.TaxExempt))
                //    return true;
            }

            if (product == null)
            {
                return false;
            }

            if (product.IsTaxExempt)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Create request for tax calculation
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="taxCategoryId">Tax category identifier</param>
        /// <param name="customer">Customer</param>
        /// <param name="price">Price</param>
        /// <returns>Package for tax calculation</returns>
        [NonAction]
        public CalculateTaxRequest CreateCalculateTaxRequest(Product product,
            int taxCategoryId, Customer customer, decimal price, int store_id)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            var calculateTaxRequest = new CalculateTaxRequest
            {
                Customer = customer,
                Product = product,
                Price = price,
                TaxCategoryId = taxCategoryId > 0 ? taxCategoryId : product?.TaxCategoryId ?? 0,
                CurrentStoreId = store_id
            };

            var basedOn = dbcontext.Setting.Where(s => s.StoreId == store_id && s.Name == "taxsettings.taxbasedon").Select(s => s.Value).FirstOrDefault();

            var euvatenabled = Convert.ToBoolean(dbcontext.Setting.Where(s => s.StoreId == store_id && s.Name == "taxsettings.euvatenabled").Select(s => s.Value).FirstOrDefault());
            //new EU VAT rules starting January 1st 2015
            //find more info at http://ec.europa.eu/taxation_customs/taxation/vat/how_vat_works/telecom/index_en.htm#new_rules
            var overriddenBasedOn =
                ////EU VAT enabled?
                euvatenabled &&
                ////telecommunications, broadcasting and electronic services?
                product != null && product.IsTelecommunicationsOrBroadcastingOrElectronicServices;//&&
                                                                                                  ////January 1st 2015 passed? Yes, not required anymore
                                                                                                  // //DateTime.UtcNow > new DateTime(2015, 1, 1, 0, 0, 0, DateTimeKind.Utc) &&
                                                                                                  // //Europe Union consumer?
                                                                                                  // IsEuConsumer(customer); //commented by Arun
            if (overriddenBasedOn)
            {
                //We must charge VAT in the EU country where the customer belongs (not where the business is based)
                basedOn = "BillingAddress";
            }

            var TaxBasedOnPickupPointAddress = Convert.ToBoolean(dbcontext.Setting.Where(s => s.StoreId == store_id && s.Name == "taxsettings.taxbasedonpickuppointaddress").Select(s => s.Value).FirstOrDefault());
            var AllowPickupInStore = Convert.ToBoolean(dbcontext.Setting.Where(s => s.StoreId == store_id && s.Name == "shippingsettings.allowpickupinstore").Select(s => s.Value).FirstOrDefault());

            //tax is based on pickup point address
            if (!overriddenBasedOn && TaxBasedOnPickupPointAddress && AllowPickupInStore)
            {
                var pickupPoint = dbcontext.GenericAttribute.Where(g => g.StoreId == store_id && g.Key == "SelectedPickupPoint" && g.EntityId == customer.Id).ToList();

                if (pickupPoint != null && pickupPoint.Count > 0)
                {
                    calculateTaxRequest.Address = new CalculateTaxRequest.TaxAddress
                    {
                        //#TODO Ram
                        //CountryId = pickupPointcountry?.Id ?? 0,
                        //StateProvinceId = state?.Id ?? 0,
                        //County = pickupPoint.County,
                        //City = pickupPoint.City,
                        //Address1 = pickupPoint.Address,
                        //ZipPostalCode = pickupPoint.ZipPostalCode
                    };
                    return calculateTaxRequest;
                }
            }

            //tax by merchant address

            var vendorAddressId = Convert.ToInt32(dbcontext.Vendor.Where(v => v.Id == product.VendorId).Select(v => v.AddressId).FirstOrDefault());
            var vendorAddress = dbcontext.Address.Where(a => a.Id == vendorAddressId).FirstOrDefault();
            if (vendorAddress != null)
            {
                calculateTaxRequest.Address = PrepareTaxAddress(vendorAddress);
            }
            else
            {
                if (basedOn == "BillingAddress" && customer.BillingAddress == null ||
                    basedOn == "ShippingAddress" && customer.ShippingAddress == null)
                {
                    basedOn = "DefaultAddress";
                }

                switch (basedOn)
                {
                    case "BillingAddress":
                        calculateTaxRequest.Address = PrepareTaxAddress(customer.BillingAddress);
                        break;
                    case "ShippingAddress":
                        calculateTaxRequest.Address = PrepareTaxAddress(customer.ShippingAddress);
                        break;
                    case "DefaultAddress":
                    default:
                        calculateTaxRequest.Address = LoadDefaultTaxAddress(store_id);
                        break;
                }
            }

            return calculateTaxRequest;
        }

        /// <summary>
        /// Gets a default tax address
        /// </summary>
        /// <returns>Address</returns>
        [NonAction]
        public CalculateTaxRequest.TaxAddress LoadDefaultTaxAddress(int store_id)
        {
            var addressId = Convert.ToInt32(dbcontext.Setting.Where(s => s.StoreId == store_id && s.Name == " taxsettings.defaulttaxaddressid").Select(s => s.Value).FirstOrDefault());
            var address = dbcontext.Address.Where(s => s.Id == addressId).FirstOrDefault();
            return PrepareTaxAddress(address);
        }

        /// <summary>
        /// Prepare TaxAddress object based on a specific address
        /// </summary>
        /// <param name="address">Address</param>
        /// <returns>Address</returns>
        [NonAction]
        public CalculateTaxRequest.TaxAddress PrepareTaxAddress(Address address)
        {
            if (address == null)
                return null;

            return new CalculateTaxRequest.TaxAddress
            {
                AddressId = address.Id,
                CountryId = address.CountryId,
                StateProvinceId = address.StateProvinceId,
                County = address.County,
                City = address.City,
                Address1 = address.Address1,
                Address2 = address.Address2,
                ZipPostalCode = address.ZipPostalCode
            };
        }

        /// <summary>
        /// Gets tax rate
        /// </summary>
        /// <param name="calculateTaxRequest">Tax calculation request</param>
        /// <returns>Tax</returns>
        [NonAction]
        public CalculateTaxResult GetTaxRate(CalculateTaxRequest calculateTaxRequest, int store_Id, out decimal ProductTaxPrice, decimal Quantity)
        {
            var result = new CalculateTaxResult();
            int storeId_Id = store_Id;
            //the tax rate calculation by fixed rate
            ProductTaxPrice = 0;
            var countryStateZipEnabled = Convert.ToBoolean(dbcontext.Setting.Where(s => (s.StoreId == storeId_Id || s.StoreId == 0) && s.Name == "fixedorbycountrystateziptaxsettings.countrystatezipenabled").Select(s => s.Value).FirstOrDefault());

            if (!countryStateZipEnabled)
            {
                result.TaxRate = Convert.ToDecimal(dbcontext.Setting.Where(s => s.StoreId == storeId_Id && s.Name == (string.Format(FixedRateSettingsKey, calculateTaxRequest.TaxCategoryId))).Select(x => x.Value).FirstOrDefault());
                return result;
            }

            //the tax rate calculation by country & state & zip 
            if (calculateTaxRequest.Address == null)
            {
                result.Errors.Add("Address is not set");
                return result;
            }

            var alltaxRates = dbcontext.TaxRate.Where(s => s.StoreId == 0 || s.StoreId == store_Id);


            var allTaxRates = alltaxRates.Select(taxRate => new TaxRateForCaching
            {
                Id = taxRate.Id,
                StoreId = taxRate.StoreId,
                TaxCategoryId = taxRate.TaxCategoryId,
                CountryId = taxRate.CountryId,
                StateProvinceId = taxRate.StateProvinceId,
                Zip = taxRate.Zip,
                Percentage = taxRate.Percentage
            }).ToList();

            var storeId = calculateTaxRequest.CurrentStoreId;
            var taxCategoryId = calculateTaxRequest.TaxCategoryId;
            var countryId = calculateTaxRequest.Address.CountryId;
            var stateProvinceId = calculateTaxRequest.Address.StateProvinceId;
            var zip = calculateTaxRequest.Address.ZipPostalCode?.Trim() ?? string.Empty;

            //get tax by tax categories
            var taxCategoryIds = new List<int>();
            if (!string.IsNullOrEmpty(calculateTaxRequest.Product?.TaxCategoryIds))
                taxCategoryIds = calculateTaxRequest.Product.TaxCategoryIds.Split(',').Select(x => int.Parse(x)).ToList();
            var existingRates = allTaxRates.Where(taxRate => taxRate.CountryId == countryId && taxCategoryIds.Contains(taxRate.TaxCategoryId));

            //filter by store
            var matchedByStore = existingRates.Where(taxRate => storeId == taxRate.StoreId || taxRate.StoreId == 0);

            //filter by state/province
            var matchedByStateProvince = matchedByStore.Where(taxRate => stateProvinceId == taxRate.StateProvinceId || taxRate.StateProvinceId == 0);

            //filter by zip
            var matchedByZip = matchedByStateProvince.Where(taxRate => string.IsNullOrWhiteSpace(taxRate.Zip) || taxRate.Zip.Equals(zip, StringComparison.InvariantCultureIgnoreCase));

            //sort from particular to general, more particular cases will be the first
            var foundRecords = matchedByZip.OrderBy(r => r.StoreId == 0).ThenBy(r => r.StateProvinceId == 0).ThenBy(r => string.IsNullOrEmpty(r.Zip));

            var foundRecord = foundRecords.ToList();

            if (foundRecord.Count > 0)
            {
                result.TaxRate = foundRecord.Sum(x => x.Percentage);
                ProductTaxPrice = ProductTaxPrice + (calculateTaxRequest.Price * Quantity * (result.TaxRate/100));

            }

            return result;
        }

        [Route("~/api/v1/RemoveProductToCart")]
        [HttpPost]
        public CustomerAPIResponses RemoveProductToCart_Catalog(AddProductToCart addProductToCart)
        {
            int languageId = LanguageHelper.GetIdByLangCode(addProductToCart.StoreId, addProductToCart.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (addProductToCart == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(addProductToCart.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.StoreId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }

            int CurrStoreId = addProductToCart.StoreId;
            if (dbcontext.ShoppingCartItem.Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId && x.ProductId == addProductToCart.ProductId).Count() > 1)
            {
                customerAPIResponses.ErrorMessageTitle = "View Cart!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.DiffrentVariationItemsVisitCartToRemove", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.Conflict;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }


            var cartcollection = dbcontext.ShoppingCartItem.Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId && x.ProductId == addProductToCart.ProductId).ToList();
            if (cartcollection.Any())
            {
                foreach (var item in cartcollection)
                {
                    item.Quantity = item.Quantity - addProductToCart.quantity;
                    item.UpdatedOnUtc = DateTime.UtcNow;
                    if (item.Quantity <= 0)
                    {
                        dbcontext.Entry(item).State = EntityState.Deleted;
                    }
                    else
                    {
                        dbcontext.Entry(item).State = EntityState.Modified;
                    }

                    dbcontext.SaveChanges();
                }
            }
            else
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.NoProductsInCart", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == addProductToCart.StoreId).FirstOrDefault().Value;
            string CurrencySymbol = Helper.GetStoreCurrency(addProductToCart.StoreId, dbcontext);
            var CartItems = (from p in dbcontext.ShoppingCartItem.Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId)
                             join pro in dbcontext.Product
                             on p.ProductId equals pro.Id
                             select new ProductDetailModel
                             {
                                 Quantity = p.Quantity,
                                 ProductId = p.ProductId,
                                 ProductName = pro.Name,
                                 ProductPrice = pro.TaxCategoryId == 0
                                                                    ? pro.Price.ToString() : (from priceCategoryTax in dbcontext.Product
                                                                                              join categoryTax in dbcontext.TaxRate
                                                                                                  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
                                                                                              where categoryTax.StoreId == addProductToCart.StoreId &&
                                                                                                  priceCategoryTax.Id == pro.Id

                                                                                              select new
                                                                                              {
                                                                                                  PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
                                                                                              }).FirstOrDefault().PriceCalculated.ToString(),
                                 ProductImageURL = dbcontext.ProductPictureMapping.Where(x => x.ProductId == pro.Id).FirstOrDefault() != null ? dbcontext.ProductPictureMapping.Where(x => x.ProductId == pro.Id).FirstOrDefault().PictureId.ToString() : "0"
                             }).ToList();
            if (CartItems.Any())
            {
                ProductDetailRootModel productDetailRoot = new ProductDetailRootModel();
                productDetailRoot.CurrencySymbol = CurrencySymbol;
                productDetailRoot.Tip = Helper.ConvertdecimaltoUptotwoPlaces(addProductToCart.TipAmount);
                productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(0);
                var ProductId = CartItems.FirstOrDefault().ProductId;
                var VendorId = dbcontext.Product.Where(x => x.Id == ProductId).FirstOrDefault().VendorId;

                // productDetailRoot.MinDateOrder = Helper.ConvertToUnixTimestamp(MinTimeForCheckout).ToString();
                var deliveryCharges = dbcontext.Vendor.Where(x => x.Id == VendorId).FirstOrDefault();
                if (deliveryCharges != null && addProductToCart.IsDelivery && !addProductToCart.IsTakeaway)
                {
                    productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(deliveryCharges.DeliveryCharge));

                }
                else
                {
                    productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
                                 
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(0);
                productDetailRoot.Coupon = "";
                productDetailRoot.CartProducts = new List<ProductDetailModel>();
                var customer = dbcontext.Customer.Where(i => i.Id == addProductToCart.CustomerId).FirstOrDefault();
                foreach (var item in CartItems)
                {
                    var attributesTotalPrice = decimal.Zero;
                    var attributeValues = Helper.ParseProductAttributeValues(item.ProductAttribute, context: dbcontext);
                    if (attributeValues != null)
                    {
                        foreach (var attributeValue in attributeValues)
                        {
                            attributesTotalPrice += Helper.GetProductAttributeValuePriceAdjustment(attributeValue, dbcontext);
                        }
                    }
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice) + attributesTotalPrice);
                    int pictureSize = 75;
                    var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
                    if (setting != null)
                    {
                        pictureSize = Convert.ToInt32(setting.Value);
                    }
                    if (!string.IsNullOrEmpty(item.ProductImageURL))
                    {
                        if (item.ProductImageURL != "0")
                        {
                            int pictureId = Convert.ToInt32(item.ProductImageURL);
                            var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();

                            string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
                            string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
                            ? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
                            : $"{pictureId:0000000}_{pictureSize}.{lastPart}";
                            item.ProductImageURL = ChefBaseUrl + thumbFileName;
                        }
                        else
                        {
                            item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                        }
                    }
                    else
                    {
                        item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                    }
                    decimal Tax = 0, ProductTaxPrice = 0;
                    var product = dbcontext.Product.Where(x => x.Id == item.ProductId).FirstOrDefault();
                    GetProductPrice(product, 0, product.Price + attributesTotalPrice, true, customer, true, CurrStoreId, out Tax, out ProductTaxPrice, item.Quantity);
                    item.ProductAttribute = Helper.FormatAttributes(/*dbcontext.Products.Where(x => x.Id == item.ProductId).FirstOrDefault()*/ item.ProductAttribute, context: dbcontext);
                    item.ProductAttributeAvailable = !string.IsNullOrEmpty(item.ProductAttribute);
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice));
                    item.ProductTaxPrice = Tax;
                    item.ProductAttributePrice = Helper.ConvertdecimaltoUptotwoPlaces(attributesTotalPrice);
                    item.ProductAttributeValue = item.ProductAttribute + " (+" + CurrencySymbol + "" + item.ProductAttributePrice + ")";
                    productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Tax) + ProductTaxPrice);
                    productDetailRoot.Subtotal = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal) + (Convert.ToDecimal(item.ProductPrice) * item.Quantity));
                    productDetailRoot.CartProducts.Add(item);
                }
                if (!string.IsNullOrEmpty(addProductToCart.CouponCode))
                {
                    var response = Helper.ApplyCoupon(addProductToCart.CustomerId, addProductToCart.CouponCode, Convert.ToDecimal(productDetailRoot.Subtotal), dbcontext,addProductToCart.StoreId,addProductToCart.languageId);
                    if (response.Status)
                    {
                        var responseDiscount = (DiscountModel)response.ResponseObj;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(responseDiscount.DiscountAmount);
                        productDetailRoot.DiscountId = responseDiscount.DiscountId;
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                    }
                    else
                    {
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                        response.ResponseObj = productDetailRoot;
                        return response;
                    }

                }
                else
                {
                    productDetailRoot.DiscountCode = "";
                    productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
                productDetailRoot.EstTime = "20-25 Minutes";
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal) + Convert.ToDecimal(productDetailRoot.Tax) + Convert.ToDecimal(productDetailRoot.DeliveryCharges) + Convert.ToDecimal(productDetailRoot.Tip) + (Convert.ToDecimal(productDetailRoot.Total) - Convert.ToDecimal(productDetailRoot.DiscountAmount)));
                customerAPIResponses.ErrorMessageTitle = "Success!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ProductList", languageId, dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                customerAPIResponses.ResponseObj = productDetailRoot;
                return customerAPIResponses;
            }
            else
            {
                customerAPIResponses.ErrorMessageTitle = "Success!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ProductList", languageId, dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                customerAPIResponses.ResponseObj = null;
            }

            return customerAPIResponses;


        }
        [Route("~/api/v2/RemoveProductToCart")]
        [HttpPost]
        public CustomerAPIResponses RemoveProductToCart_CatalogV2(AddRemoveProductCart addProductToCart)
        {
            int languageId = LanguageHelper.GetIdByLangCode(addProductToCart.StoreId, addProductToCart.UniqueSeoCode, dbcontext);

            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (addProductToCart == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(addProductToCart.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.StoreId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }

            int CurrStoreId = addProductToCart.StoreId;
            var cartcollection = dbcontext.ShoppingCartItem.Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId && x.Id == addProductToCart.CartItemId).ToList();
            if (cartcollection.Any())
            {
                foreach (var item in cartcollection)
                {
                    item.Quantity = item.Quantity - 1;
                    item.UpdatedOnUtc = DateTime.UtcNow;
                    if (item.Quantity <= 0)
                    {
                        dbcontext.Entry(item).State = EntityState.Deleted;
                    }
                    else
                    {
                        dbcontext.Entry(item).State = EntityState.Modified;
                    }

                    dbcontext.SaveChanges();
                }
            }
            string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == addProductToCart.StoreId).FirstOrDefault().Value;
            string CurrencySymbol = Helper.GetStoreCurrency(addProductToCart.StoreId, dbcontext);
            var CartItems = (from p in dbcontext.ShoppingCartItem.Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId)
                             join pro in dbcontext.Product
                             on p.ProductId equals pro.Id
                             select new ProductDetailModel
                             {
                                 CartItemId = p.Id,
                                 ProductAttribute = p.AttributesXml,
                                 Quantity = p.Quantity,
                                 ProductId = p.ProductId,
                                 ProductName = pro.Name,
                                 ProductPrice = pro.TaxCategoryId == 0
                                                                    ? pro.Price.ToString() : (from priceCategoryTax in dbcontext.Product
                                                                                              join categoryTax in dbcontext.TaxRate
                                                                                                  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
                                                                                              where categoryTax.StoreId == addProductToCart.StoreId &&
                                                                                                  priceCategoryTax.Id == pro.Id

                                                                                              select new
                                                                                              {
                                                                                                  PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
                                                                                              }).FirstOrDefault().PriceCalculated.ToString(),
                                 ProductImageURL = dbcontext.ProductPictureMapping.Where(x => x.ProductId == pro.Id).FirstOrDefault() != null ? dbcontext.ProductPictureMapping.Where(x => x.ProductId == pro.Id).FirstOrDefault().PictureId.ToString() : "0"
                             }).ToList();
            if (CartItems.Any())
            {
                ProductDetailRootModel productDetailRoot = new ProductDetailRootModel();
                productDetailRoot.CurrencySymbol = CurrencySymbol;
                productDetailRoot.Tip = Helper.ConvertdecimaltoUptotwoPlaces(addProductToCart.TipAmount);
                productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(0);
                var ProductId = CartItems.FirstOrDefault().ProductId;
                var VendorId = dbcontext.Product.Where(x => x.Id == ProductId).FirstOrDefault().VendorId;

                // productDetailRoot.MinDateOrder = Helper.ConvertToUnixTimestamp(MinTimeForCheckout).ToString();
                var deliveryCharges = dbcontext.Vendor.Where(x => x.Id == VendorId).FirstOrDefault();
                if (deliveryCharges != null && addProductToCart.IsDelivery && !addProductToCart.IsTakeAway)
                {
                    productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(deliveryCharges.DeliveryCharge));

                }
                else
                {
                    productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
                
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(0);
                productDetailRoot.Coupon = "";
                productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(0);
                productDetailRoot.CartProducts = new List<ProductDetailModel>();
                var customer = dbcontext.Customer.Where(i => i.Id == addProductToCart.CustomerId).FirstOrDefault();
                foreach (var item in CartItems)
                {
                    var attributesTotalPrice = decimal.Zero;
                    var attributeValues = Helper.ParseProductAttributeValues(item.ProductAttribute, context: dbcontext);
                    if (attributeValues != null)
                    {
                        foreach (var attributeValue in attributeValues)
                        {
                            attributesTotalPrice += Helper.GetProductAttributeValuePriceAdjustment(attributeValue, dbcontext);
                        }
                    }
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice) + attributesTotalPrice);
                    int pictureSize = 75;
                    var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
                    if (setting != null)
                    {
                        pictureSize = Convert.ToInt32(setting.Value);
                    }
                    if (!string.IsNullOrEmpty(item.ProductImageURL))
                    {
                        if (item.ProductImageURL != "0")
                        {
                            int pictureId = Convert.ToInt32(item.ProductImageURL);
                            var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();

                            string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
                            string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
                            ? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
                            : $"{pictureId:0000000}_{pictureSize}.{lastPart}";
                            item.ProductImageURL = ChefBaseUrl + thumbFileName;
                        }
                        else
                        {
                            item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                        }
                    }
                    else
                    {
                        item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                    }
                    var product = dbcontext.Product.Where(x => x.Id == item.ProductId).FirstOrDefault();
                    decimal Tax = 0, ProductTaxPrice = 0;
                    GetProductPrice(product, 0, product.Price + attributesTotalPrice, true, customer, true, CurrStoreId, out Tax, out ProductTaxPrice, item.Quantity);

                    item.ProductAttribute = Helper.FormatAttributes(item.ProductAttribute, context: dbcontext);
                    item.ProductAttributeAvailable = !string.IsNullOrEmpty(item.ProductAttribute);
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice));
                    item.ProductTaxPrice = Tax;
                    item.ProductAttributePrice = Helper.ConvertdecimaltoUptotwoPlaces(attributesTotalPrice);
                    item.ProductAttributeValue = item.ProductAttribute + " (+" + CurrencySymbol + "" + item.ProductAttributePrice + ")";
                    productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Tax) + ProductTaxPrice);
                    productDetailRoot.Subtotal = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal) + (Convert.ToDecimal(item.ProductPrice) * item.Quantity));
                    productDetailRoot.CartProducts.Add(item);
                }
                if (!string.IsNullOrEmpty(addProductToCart.CouponCode))
                {
                    var response = Helper.ApplyCoupon(addProductToCart.CustomerId, addProductToCart.CouponCode, Convert.ToDecimal(productDetailRoot.Subtotal), dbcontext,addProductToCart.StoreId,addProductToCart.languageId);
                    if (response.Status)
                    {
                        var responseDiscount = (DiscountModel)response.ResponseObj;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(responseDiscount.DiscountAmount);
                        productDetailRoot.DiscountId = responseDiscount.DiscountId;
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                    }
                    else
                    {
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                        response.ResponseObj = productDetailRoot;
                        return response;
                    }

                }
                else
                {
                    productDetailRoot.DiscountCode = "";
                    productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
                productDetailRoot.EstTime = "20-25 Minutes";
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal) + Convert.ToDecimal(productDetailRoot.Tax) + Convert.ToDecimal(productDetailRoot.DeliveryCharges) + Convert.ToDecimal(productDetailRoot.Tip) + (Convert.ToDecimal(productDetailRoot.Total) - Convert.ToDecimal(productDetailRoot.DiscountAmount)));
                customerAPIResponses.ErrorMessageTitle = "Success!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ProductList", languageId, dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                customerAPIResponses.ResponseObj = productDetailRoot;
                return customerAPIResponses;
            }
            else
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.NoProductsInCart", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                customerAPIResponses.ResponseObj = null;
            }

            return customerAPIResponses;


        }

        [Route("~/api/v2.2/RemoveProductToCart")]
        [HttpPost]
        public CustomerAPIResponses RemoveProductToCart_CatalogV2_2(AddRemoveProductCart addProductToCart)
        {
            int languageId = LanguageHelper.GetIdByLangCode(addProductToCart.StoreId, addProductToCart.UniqueSeoCode, dbcontext);

            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (addProductToCart == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(addProductToCart.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.StoreId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }

            int CurrStoreId = addProductToCart.StoreId;
            var cartcollection = dbcontext.ShoppingCartItem.Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId && x.Id == addProductToCart.CartItemId).ToList();
            if (cartcollection.Any())
            {
                foreach (var item in cartcollection)
                {
                    item.Quantity = item.Quantity - 1;
                    item.UpdatedOnUtc = DateTime.UtcNow;
                    if (item.Quantity <= 0)
                    {
                        dbcontext.Entry(item).State = EntityState.Deleted;
                    }
                    else
                    {
                        dbcontext.Entry(item).State = EntityState.Modified;
                    }

                    dbcontext.SaveChanges();
                }
            }
            string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == addProductToCart.StoreId).FirstOrDefault().Value;
            string CurrencySymbol = Helper.GetStoreCurrency(addProductToCart.StoreId, dbcontext);
            var CartItems = (from p in dbcontext.ShoppingCartItem.Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId)
                             join pro in dbcontext.Product
                             on p.ProductId equals pro.Id
                             select new ProductDetailModel
                             {
                                 CartItemId = p.Id,
                                 ProductAttribute = p.AttributesXml,
                                 Quantity = p.Quantity,
                                 ProductId = p.ProductId,
                                 ProductName = pro.Name,
                                 ProductPrice = pro.TaxCategoryId == 0
                                                                    ? pro.Price.ToString() : (from priceCategoryTax in dbcontext.Product
                                                                                              join categoryTax in dbcontext.TaxRate
                                                                                                  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
                                                                                              where categoryTax.StoreId == addProductToCart.StoreId &&
                                                                                                  priceCategoryTax.Id == pro.Id

                                                                                              select new
                                                                                              {
                                                                                                  PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
                                                                                              }).FirstOrDefault().PriceCalculated.ToString(),
                                 ProductImageURL = dbcontext.ProductPictureMapping.Where(x => x.ProductId == pro.Id).FirstOrDefault() != null ? dbcontext.ProductPictureMapping.Where(x => x.ProductId == pro.Id).FirstOrDefault().PictureId.ToString() : "0"
                             }).ToList();
            if (CartItems.Any())
            {
                ProductDetailRootModel productDetailRoot = new ProductDetailRootModel();
                productDetailRoot.CurrencySymbol = CurrencySymbol;
                productDetailRoot.Tip = Helper.ConvertdecimaltoUptotwoPlaces(addProductToCart.TipAmount);
                productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(0);
                var ProductId = CartItems.FirstOrDefault().ProductId;
                var VendorId = dbcontext.Product.Where(x => x.Id == ProductId).FirstOrDefault().VendorId;

                // productDetailRoot.MinDateOrder = Helper.ConvertToUnixTimestamp(MinTimeForCheckout).ToString();
                var deliveryCharges = dbcontext.Vendor.Where(x => x.Id == VendorId).FirstOrDefault();
                int deliveryAddressId = addProductToCart.deliveryAddressId != 0 ? addProductToCart.deliveryAddressId : deliveryCharges.AddressId;
                if (deliveryCharges != null && addProductToCart.IsDelivery && !addProductToCart.IsTakeAway)
                {
                    productDetailRoot.DeliveryCharges = Convert.ToString(Helper.CalculateDeliveryFee(VendorId, deliveryAddressId, addProductToCart.StoreId, dbcontext));
                }
                else
                {
                    productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
                
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(0);
                //productDetailRoot.Total = 0;
                productDetailRoot.Coupon = "";
                productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(0);
                productDetailRoot.CartProducts = new List<ProductDetailModel>();

                var customer = dbcontext.Customer.Where(i => i.Id == addProductToCart.CustomerId).FirstOrDefault();
                foreach (var item in CartItems)
                {
                    var attributesTotalPrice = decimal.Zero;
                    var attributeValues = Helper.ParseProductAttributeValues(item.ProductAttribute, context: dbcontext);
                    if (attributeValues != null)
                    {
                        foreach (var attributeValue in attributeValues)
                        {
                            attributesTotalPrice += Helper.GetProductAttributeValuePriceAdjustment(attributeValue, dbcontext);
                        }
                    }
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice) + attributesTotalPrice);
                    int pictureSize = 75;
                    var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
                    if (setting != null)
                    {
                        pictureSize = Convert.ToInt32(setting.Value);
                    }
                    if (!string.IsNullOrEmpty(item.ProductImageURL))
                    {
                        if (item.ProductImageURL != "0")
                        {
                            int pictureId = Convert.ToInt32(item.ProductImageURL);
                            var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();

                            string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
                            string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
                            ? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
                            : $"{pictureId:0000000}_{pictureSize}.{lastPart}";
                            item.ProductImageURL = ChefBaseUrl + thumbFileName;
                        }
                        else
                        {
                            item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                        }
                    }
                    else
                    {
                        item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                    }
                    
                    var product = dbcontext.Product.Where(x => x.Id == item.ProductId).FirstOrDefault();
                    decimal Tax = 0, ProductTaxPrice = 0;
                    GetProductPrice(product, 0, product.Price + attributesTotalPrice, true, customer, true, CurrStoreId, out Tax, out ProductTaxPrice, item.Quantity);

                    item.ProductAttribute = Helper.FormatAttributes(/*dbcontext.Products.Where(x => x.Id == item.ProductId).FirstOrDefault()*/ item.ProductAttribute, context: dbcontext);
                    item.ProductAttributeAvailable = !string.IsNullOrEmpty(item.ProductAttribute);
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice));
                    item.ProductTaxPrice = Tax;
                    item.ProductAttributePrice = Helper.ConvertdecimaltoUptotwoPlaces(attributesTotalPrice);
                    item.ProductAttributeValue = item.ProductAttribute + " (+" + CurrencySymbol + "" + item.ProductAttributePrice + ")";
                    productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Tax) + ProductTaxPrice);
                    //productDetailRoot.CartItemId = item.CartItemId;
                    productDetailRoot.Subtotal = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal) + (Convert.ToDecimal(item.ProductPrice) * item.Quantity));
                    productDetailRoot.CartProducts.Add(item);
                }
                if (!string.IsNullOrEmpty(addProductToCart.CouponCode))
                {
                    var response = Helper.ApplyCoupon(addProductToCart.CustomerId, addProductToCart.CouponCode, Convert.ToDecimal(productDetailRoot.Subtotal), dbcontext, addProductToCart.StoreId, addProductToCart.languageId);
                    if (response.Status)
                    {
                        var responseDiscount = (DiscountModel)response.ResponseObj;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(responseDiscount.DiscountAmount);
                        productDetailRoot.DiscountId = responseDiscount.DiscountId;
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                    }
                    else
                    {
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                        response.ResponseObj = productDetailRoot;
                        return response;
                    }

                }
                else
                {
                    productDetailRoot.DiscountCode = "";
                    productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
                productDetailRoot.ServiceChargeAmount = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(Helper.CalculateServiceChargeAmount(Convert.ToDecimal(productDetailRoot.Subtotal), addProductToCart.StoreId, dbcontext)));
                productDetailRoot.EstTime = "20-25 Minutes";
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.DeliveryCharges) + Convert.ToDecimal(productDetailRoot.Tip) +Convert.ToDecimal(productDetailRoot.ServiceChargeAmount) + Convert.ToDecimal(productDetailRoot.Tax) + (Convert.ToDecimal(productDetailRoot.Subtotal) - Convert.ToDecimal(productDetailRoot.DiscountAmount)));
                customerAPIResponses.ErrorMessageTitle = "Success!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ProductList", languageId, dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                customerAPIResponses.ResponseObj = productDetailRoot;
                return customerAPIResponses;
            }
            else
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.NoProductsInCart", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                customerAPIResponses.ResponseObj = null;
            }

            return customerAPIResponses;


        }

        [Route("~/api/v2/RemoveAllProductToCart")]
        [HttpPost]
        public CustomerAPIResponses RemoveAllProductToCart_CatalogV2(AddRemoveProductCart addProductToCart)
        {
            int languageId = LanguageHelper.GetIdByLangCode(addProductToCart.StoreId, addProductToCart.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (addProductToCart == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(addProductToCart.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (addProductToCart.StoreId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            int CurrStoreId = addProductToCart.StoreId;

            var cartcollection = dbcontext.ShoppingCartItem.Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId && x.Id == addProductToCart.CartItemId).ToList();
            if (cartcollection.Any())
            {
                foreach (var item in cartcollection)
                {
                    dbcontext.Entry(item).State = EntityState.Deleted;
                    dbcontext.SaveChanges();
                }
            }
            string ChefBaseUrl = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.weburl") && x.StoreId == addProductToCart.StoreId).FirstOrDefault().Value;
            string CurrencySymbol = Helper.GetStoreCurrency(addProductToCart.StoreId, dbcontext);
            var CartItems = (from p in dbcontext.ShoppingCartItem.Where(x => x.StoreId == addProductToCart.StoreId && x.CustomerId == addProductToCart.CustomerId)
                             join pro in dbcontext.Product
                             on p.ProductId equals pro.Id
                             select new ProductDetailModel
                             {
                                 ProductAttribute = p.AttributesXml,
                                 CartItemId = p.Id,
                                 Quantity = p.Quantity,
                                 ProductId = p.ProductId,
                                 ProductName = pro.Name,
                                 ProductPrice = pro.TaxCategoryId == 0
                                                                    ? pro.Price.ToString() : (from priceCategoryTax in dbcontext.Product
                                                                                              join categoryTax in dbcontext.TaxRate
                                                                                                  on priceCategoryTax.TaxCategoryId equals categoryTax.TaxCategoryId
                                                                                              where categoryTax.StoreId == addProductToCart.StoreId &&
                                                                                                  priceCategoryTax.Id == pro.Id

                                                                                              select new
                                                                                              {
                                                                                                  PriceCalculated = ((categoryTax.Percentage * priceCategoryTax.Price) / 100) + priceCategoryTax.Price
                                                                                              }).FirstOrDefault().PriceCalculated.ToString(),
                                 ProductImageURL = dbcontext.ProductPictureMapping.Where(x => x.ProductId == pro.Id).FirstOrDefault() != null ? dbcontext.ProductPictureMapping.Where(x => x.ProductId == pro.Id).FirstOrDefault().PictureId.ToString() : "0"
                             }).ToList();
            if (CartItems.Any())
            {
                ProductDetailRootModel productDetailRoot = new ProductDetailRootModel();
                productDetailRoot.CurrencySymbol = CurrencySymbol;
                productDetailRoot.Tip = Helper.ConvertdecimaltoUptotwoPlaces(addProductToCart.TipAmount);
                productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(0);
                var ProductId = CartItems.FirstOrDefault().ProductId;
                var VendorId = dbcontext.Product.Where(x => x.Id == ProductId).FirstOrDefault().VendorId;

                // productDetailRoot.MinDateOrder = Helper.ConvertToUnixTimestamp(MinTimeForCheckout).ToString();
                var deliveryCharges = dbcontext.Vendor.Where(x => x.Id == VendorId).FirstOrDefault();
                if (deliveryCharges != null && addProductToCart.IsDelivery && !addProductToCart.IsTakeAway)
                {
                    productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(deliveryCharges.DeliveryCharge));

                }
                else
                {
                    productDetailRoot.DeliveryCharges = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
                
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(0);
                
                productDetailRoot.Coupon = "";
                productDetailRoot.CartProducts = new List<ProductDetailModel>();
                var customer = dbcontext.Customer.Where(i => i.Id == addProductToCart.CustomerId).FirstOrDefault();
                foreach (var item in CartItems)
                {
                    var attributesTotalPrice = decimal.Zero;
                    var attributeValues = Helper.ParseProductAttributeValues(item.ProductAttribute, context: dbcontext);
                    if (attributeValues != null)
                    {
                        foreach (var attributeValue in attributeValues)
                        {
                            attributesTotalPrice += Helper.GetProductAttributeValuePriceAdjustment(attributeValue, dbcontext);
                        }
                    }
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice) + attributesTotalPrice);
                    int pictureSize = 75;
                    var setting = dbcontext.Setting.Where(x => x.Name.Contains("mediasettings.productthumbpicturesize")).FirstOrDefault();
                    if (setting != null)
                    {
                        pictureSize = Convert.ToInt32(setting.Value);
                    }
                    if (!string.IsNullOrEmpty(item.ProductImageURL))
                    {
                        if (item.ProductImageURL != "0")
                        {
                            int pictureId = Convert.ToInt32(item.ProductImageURL);
                            var pictureImage = dbcontext.Picture.Where(x => x.Id == pictureId).FirstOrDefault();

                            string lastPart = Helper.GetFileExtensionFromMimeType(pictureImage.MimeType);
                            string thumbFileName = !string.IsNullOrEmpty(pictureImage.SeoFilename)
                            ? $"{pictureId:0000000}_{pictureImage.SeoFilename}.{lastPart}"
                            : $"{pictureId:0000000}_{pictureSize}.{lastPart}";
                            item.ProductImageURL = ChefBaseUrl + thumbFileName;
                        }
                        else
                        {
                            item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                        }
                    }
                    else
                    {
                        item.ProductImageURL = ChefBaseUrl + "default-prod-image_" + pictureSize + ".png?" + DateTime.Now;
                    }
                    var product = dbcontext.Product.Where(x => x.Id == item.ProductId).FirstOrDefault();
                    decimal Tax = 0, ProductTaxPrice = 0;
                    GetProductPrice(product, 0, product.Price + attributesTotalPrice, true, customer, true, CurrStoreId, out Tax, out ProductTaxPrice, item.Quantity);

                    item.ProductAttribute = Helper.FormatAttributes(item.ProductAttribute, context: dbcontext);
                    item.ProductAttributeAvailable = !string.IsNullOrEmpty(item.ProductAttribute);
                    item.ProductPrice = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(item.ProductPrice));
                    item.ProductTaxPrice = Tax;
                    item.ProductAttributePrice = Helper.ConvertdecimaltoUptotwoPlaces(attributesTotalPrice);
                    item.ProductAttributeValue = item.ProductAttribute + " (+" + CurrencySymbol + "" + item.ProductAttributePrice + ")";
                    productDetailRoot.Tax = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Tax) + ProductTaxPrice);
                    productDetailRoot.Subtotal = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.Subtotal) + (Convert.ToDecimal(item.ProductPrice) * item.Quantity));
                    productDetailRoot.CartProducts.Add(item);
                }
                if (!string.IsNullOrEmpty(addProductToCart.CouponCode))
                {
                    var response = Helper.ApplyCoupon(addProductToCart.CustomerId, addProductToCart.CouponCode, Convert.ToDecimal(productDetailRoot.Subtotal), dbcontext,addProductToCart.StoreId,addProductToCart.languageId);
                    if (response.Status)
                    {
                        var responseDiscount = (DiscountModel)response.ResponseObj;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(responseDiscount.DiscountAmount);
                        productDetailRoot.DiscountId = responseDiscount.DiscountId;
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                    }
                    else
                    {
                        productDetailRoot.DiscountCode = addProductToCart.CouponCode;
                        productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                        response.ResponseObj = productDetailRoot;
                        return response;
                    }

                }
                else
                {
                    productDetailRoot.DiscountCode = "";
                    productDetailRoot.DiscountAmount = Helper.ConvertdecimaltoUptotwoPlaces(0);
                }
                productDetailRoot.EstTime = "20-25 Minutes";
                productDetailRoot.Total = Helper.ConvertdecimaltoUptotwoPlaces(Convert.ToDecimal(productDetailRoot.DeliveryCharges) + Convert.ToDecimal(productDetailRoot.Tip) + (Convert.ToDecimal(productDetailRoot.Subtotal) - Convert.ToDecimal(productDetailRoot.DiscountAmount)));
                customerAPIResponses.ErrorMessageTitle = "Success!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.ProductList", languageId, dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                customerAPIResponses.ResponseObj = productDetailRoot;
                return customerAPIResponses;
            }
            else
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(addProductToCart.StoreId, "API.ErrorMesaage.NoProductsInCart", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                customerAPIResponses.ResponseObj = null;
            }

            return customerAPIResponses;


        }
        [Route("~/api/v1/GetCartCount")]
        [HttpPost]
        public CustomerAPIResponses GetCartCount(ProductCartCount productCartCount)
        {
            int languageId = LanguageHelper.GetIdByLangCode(productCartCount.StoreId, productCartCount.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (productCartCount == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productCartCount.StoreId, "API.ErrorMesaage.ModelNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(productCartCount.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productCartCount.StoreId, "API.ErrorMessage.AuthenticationkeyMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (productCartCount.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productCartCount.StoreId, "API.GetAllStoreLanguages.CustomerId", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (productCartCount.StoreId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productCartCount.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }


            int CurrStoreId = productCartCount.StoreId;
            var CartItemsCount = (from p in dbcontext.ShoppingCartItem.Where(x => x.StoreId == productCartCount.StoreId && x.CustomerId == productCartCount.CustomerId)
                                  select new
                                  {
                                      p.Quantity,
                                      p.ProductId,
                                  }).ToList().Count();

            customerAPIResponses.ErrorMessageTitle = "Success!!";
            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(productCartCount.StoreId, "API.ErrorMesaage.ProductListCount", languageId, dbcontext);
            customerAPIResponses.Status = true;
            customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
            customerAPIResponses.ResponseObj = CartItemsCount;


            return customerAPIResponses;


        }
        //[Route("~/api/v2/GetCartCount")]
        //[HttpPost]
        //public CustomerAPIResponses GetCartCountv2(ProductCartCount productCartCount)
        //{
        //    CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
        //    if (productCartCount == null)
        //    {
        //        customerAPIResponses.ErrorMessageTitle = "Error!!";
        //        customerAPIResponses.ErrorMessage = "Model Not Defined";
        //        customerAPIResponses.Status = false;
        //        customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
        //        customerAPIResponses.ResponseObj = null;
        //        return customerAPIResponses;
        //    }
        //    if (string.IsNullOrEmpty(productCartCount.ApiKey))
        //    {
        //        customerAPIResponses.ErrorMessageTitle = "Error!!";
        //        customerAPIResponses.ErrorMessage = "Authentication Key is missing";
        //        customerAPIResponses.Status = false;
        //        customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
        //        customerAPIResponses.ResponseObj = null;
        //        return customerAPIResponses;
        //    }
        //    if (productCartCount.CustomerId == 0)
        //    {
        //        customerAPIResponses.ErrorMessageTitle = "Error!!";
        //        customerAPIResponses.ErrorMessage = "Customer Id is missing";
        //        customerAPIResponses.Status = false;
        //        customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
        //        customerAPIResponses.ResponseObj = null;
        //        return customerAPIResponses;
        //    }
        //    if (productCartCount.StoreId == 0)
        //    {
        //        customerAPIResponses.ErrorMessageTitle = "Error!!";
        //        customerAPIResponses.ErrorMessage = "Store Id is missing";
        //        customerAPIResponses.Status = false;
        //        customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
        //        customerAPIResponses.ResponseObj = null;
        //        return customerAPIResponses;
        //    }


        //    using (var dbcontext = new CustomerNestorhawkContext())
        //    {
        //        int CurrStoreId = productCartCount.StoreId;
        //        var CartItemsCount = (from p in dbcontext.ShoppingCartItems.Where(x => x.StoreId == productCartCount.StoreId && x.CustomerId == productCartCount.CustomerId)
        //                              select new
        //                              {
        //                                  p.Quantity,
        //                                  p.ProductId,
        //                              }).ToList();
        //        ProductCartCountModel cartCountModel = new ProductCartCountModel();
        //        cartCountModel.Count = CartItemsCount.Count();
        //        if (cartCountModel.Count == 0)
        //        {
        //            cartCountModel.IsPre = false;
        //        }
        //        else
        //        {
        //            var ProductId = CartItemsCount.FirstOrDefault().ProductId;
        //            var vendorName = (from a in dbcontext.Products.Where(x => x.Id == ProductId)
        //                              join b in dbcontext.Vendors
        //                              on a.VendorId equals b.Id
        //                              select new
        //                              {
        //                                  b
        //                              }).FirstOrDefault().b.SystemName;
        //            if (vendorName.ToLower().Contains("pre"))
        //            {
        //                cartCountModel.IsPre = true;
        //            }
        //            else
        //            {
        //                cartCountModel.IsPre = false;
        //            }
        //        }
        //        customerAPIResponses.ErrorMessageTitle = "Success!!";
        //        customerAPIResponses.ErrorMessage = "Product List Count";
        //        customerAPIResponses.Status = true;
        //        customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
        //        customerAPIResponses.ResponseObj = cartCountModel;


        //        return customerAPIResponses;


        //    }
        //}

    }
    public class ProductCartCountModel
    {
        public int Count { get; set; }
        public bool IsPre { get; set; }
    }



    /// <summary>
    /// Represents a result of tax calculation
    /// </summary>
    public partial class CalculateTaxResult
    {
        public CalculateTaxResult()
        {
            Errors = new List<string>();
        }

        /// <summary>
        /// Gets or sets a tax rate
        /// </summary>
        public decimal TaxRate { get; set; }

        /// <summary>
        /// Gets or sets errors
        /// </summary>
        public IList<string> Errors { get; set; }

        /// <summary>
        /// Gets a value indicating whether request has been completed successfully
        /// </summary>
        public bool Success => !Errors.Any();

        /// <summary>
        /// Add error
        /// </summary>
        /// <param name="error">Error</param>
        public void AddError(string error)
        {
            Errors.Add(error);
        }
    }
    /// <summary>
    /// Represents a request for tax calculation
    /// </summary>
    public partial class CalculateTaxRequest
    {
        /// <summary>
        /// Gets or sets a customer
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets a product
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// Gets or sets an address
        /// </summary>
        public TaxAddress Address { get; set; }

        /// <summary>
        /// Gets or sets a tax category identifier
        /// </summary>
        public int TaxCategoryId { get; set; }

        /// <summary>
        /// Gets or sets a price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets a current store identifier
        /// </summary>
        public int CurrentStoreId { get; set; }

        /// <summary>
        /// Represents information about address for tax calculation
        /// </summary>
        [Serializable]
        public class TaxAddress
        {
            /// <summary>
            /// Gets or sets an indentifier of appropriate "Address" entity (if available). Otherwise, 0
            /// </summary>
            public int AddressId { get; set; }

            /// <summary>
            /// Gets or sets the country identifier
            /// </summary>
            public int? CountryId { get; set; }

            /// <summary>
            /// Gets or sets the state/province identifier
            /// </summary>
            public int? StateProvinceId { get; set; }

            /// <summary>
            /// Gets or sets the county
            /// </summary>
            public string County { get; set; }

            /// <summary>
            /// Gets or sets the city
            /// </summary>
            public string City { get; set; }

            /// <summary>
            /// Gets or sets the address 1
            /// </summary>
            public string Address1 { get; set; }

            /// <summary>
            /// Gets or sets the address 2
            /// </summary>
            public string Address2 { get; set; }

            /// <summary>
            /// Gets or sets the zip/postal code
            /// </summary>
            public string ZipPostalCode { get; set; }
        }
    }

    /// <summary>
    /// Represents a tax rate for caching
    /// </summary>
    public partial class TaxRateForCaching
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public int TaxCategoryId { get; set; }
        public int CountryId { get; set; }
        public int StateProvinceId { get; set; }
        public string Zip { get; set; }
        public decimal Percentage { get; set; }
    }
    public partial class PickupPoint
    {
        /// <summary>
        /// Gets or sets an identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets a name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a system name of the pickup point provider
        /// </summary>
        public string ProviderSystemName { get; set; }

        /// <summary>
        /// Gets or sets an address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets a city
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets a county
        /// </summary>
        public string County { get; set; }

        /// <summary>
        /// Gets or sets a state abbreviation
        /// </summary>
        public string StateAbbreviation { get; set; }

        /// <summary>
        /// Gets or sets a two-letter ISO country code
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets or sets a zip postal code
        /// </summary>
        public string ZipPostalCode { get; set; }

        /// <summary>
        /// Gets or sets a latitude
        /// </summary>
        public decimal? Latitude { get; set; }

        /// <summary>
        /// Gets or sets a longitude
        /// </summary>
        public decimal? Longitude { get; set; }

        /// <summary>
        /// Gets or sets a fee for the pickup
        /// </summary>
        public decimal PickupFee { get; set; }

        /// <summary>
        /// Gets or sets an opening hours
        /// </summary>
        public string OpeningHours { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }
    }
}
