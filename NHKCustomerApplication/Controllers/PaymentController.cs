﻿using NHKCustomerApplication.Models;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Stripe;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Braintree;
using System.Text;
using System.Globalization;

namespace NHKCustomerApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly string googleMapKey = "key=AIzaSyClQteKHAPw20u9I8FQZaHvnH_4yg0Lph8";
        private readonly string stripeKey = "sk_test_N6uF5avkJV9DTmdfBOnYYOcM00Jqer0RzN";
        private ecuadordevContext dbcontext;
        public PaymentController(ecuadordevContext context)
        {
            dbcontext = context;
        }
        [Route("~/api/v1/CheckoutPayment")]
        [HttpPost]
        public CustomerAPIResponses CheckoutPaymentv2([FromBody] CheckoutPaymentModelV2 checkoutPaymentModel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(checkoutPaymentModel.StoreId, checkoutPaymentModel.UniqueSeoCode, dbcontext);

            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (checkoutPaymentModel == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(checkoutPaymentModel.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (checkoutPaymentModel.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Customer Id Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (checkoutPaymentModel.OrderId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.OrderIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            try
            {
                if (!string.IsNullOrEmpty(checkoutPaymentModel.ApiKey))
                {
                    var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == checkoutPaymentModel.ApiKey).Any();
                    if (!keyExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }
                if (checkoutPaymentModel.OrderId != 0)
                {
                    var keyExist = dbcontext.Order.Where(x => x.Id == checkoutPaymentModel.OrderId).Any();
                    if (!keyExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.OrderNotFound", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }

                var order = dbcontext.Order.Where(x => x.Id == checkoutPaymentModel.OrderId).FirstOrDefault();
                long total = 0;

                total = Convert.ToInt64(order.OrderTotal);
                Charge charge = null;
                if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.COD)
                {
                    checkoutPaymentModel.IsSuccess = true;
                }
                else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal && checkoutPaymentModel.IsSuccess)
                {
                    checkoutPaymentModel.IsSuccess = true;
                }
                else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Square && checkoutPaymentModel.IsSuccess)
                {
                    checkoutPaymentModel.IsSuccess = true;
                }
                else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Card)
                {
                    StripeConfiguration.ApiKey = Helper.GetStripeKey(order.StoreId, dbcontext);
                    string customerId = string.Empty;
                    string cardid = string.Empty;
                    var isCustomerExistinStrip = dbcontext.GenericAttribute.Where(x => x.EntityId == checkoutPaymentModel.CustomerId && x.Key == "CustomerStripeId").FirstOrDefault();
                    if (!checkoutPaymentModel.IsNew)
                    {
                        customerId = isCustomerExistinStrip.Value;
                        cardid = checkoutPaymentModel.CardId;
                    }
                    // Token is created using Checkout or Elements!
                    // Get the payment token submitted by the form:
                    else
                    {
                        bool PaymentFlag = false;
                        string newToken = string.Empty;
                        if (isCustomerExistinStrip != null)
                        {
                            var service1 = new TokenService();
                            var token = service1.Get(checkoutPaymentModel.PaymentToken);

                            if (token != null && token.Id != null)
                            {
                                var CardService = new CardService();
                                var CardListOptions = new CardListOptions
                                {
                                    Limit = 15,
                                };
                                List<Card> cards = CardService.List(isCustomerExistinStrip.Value, CardListOptions).ToList();
                                if (cards.Any())
                                {
                                    foreach (var item in cards)
                                    {
                                        if (item.Fingerprint.Equals(token.Card.Fingerprint))
                                        {
                                            customerId = item.CustomerId;
                                            cardid = item.Id;
                                            PaymentFlag = true;
                                            var servicetoGetCustomer = new CustomerService();
                                            var customer = servicetoGetCustomer.Get(isCustomerExistinStrip.Value);
                                            var optionsUpdate = new CustomerUpdateOptions
                                            {
                                                DefaultSource = item.Id
                                            };
                                            servicetoGetCustomer.Update(isCustomerExistinStrip.Value, optionsUpdate);
                                        }
                                    }

                                }
                            }
                        }
                        else
                        {
                            var customerExist = dbcontext.Customer.Where(x => x.Id == checkoutPaymentModel.CustomerId).FirstOrDefault();
                            var optionsCustomer = new CustomerCreateOptions
                            {
                                Description = "Customer for " + customerExist.Email,
                                Email = customerExist.Email,
                            };
                            var serviceCustomer = new CustomerService();
                            var customer = serviceCustomer.Create(optionsCustomer);

                            GenericAttribute genericAttribute = new GenericAttribute();
                            genericAttribute.KeyGroup = "Customer";
                            genericAttribute.StoreId = order.StoreId;
                            genericAttribute.Key = "CustomerStripeId";
                            genericAttribute.Value = customer.Id;
                            genericAttribute.EntityId = checkoutPaymentModel.CustomerId;
                            dbcontext.GenericAttribute.Add(genericAttribute);
                            dbcontext.SaveChanges();
                            PaymentFlag = false;
                        }

                        if (!PaymentFlag)
                        {
                            var cardCreateOptions = new CardCreateOptions
                            {
                                Source = checkoutPaymentModel.PaymentToken,
                                //C
                            };
                            var cardService = new CardService();
                            if (isCustomerExistinStrip == null)
                            {
                                isCustomerExistinStrip = dbcontext.GenericAttribute.Where(x => x.EntityId == checkoutPaymentModel.CustomerId && x.Key == "CustomerStripeId").FirstOrDefault();
                            }
                            Card card = cardService.Create(isCustomerExistinStrip.Value, cardCreateOptions);
                            //card.tok                   
                            //  newToken = card.;
                            customerId = card.CustomerId;
                            cardid = card.Id;
                            var servicetoGetCustomer = new CustomerService();
                            var customer = servicetoGetCustomer.Get(isCustomerExistinStrip.Value);
                            var optionsUpdate = new CustomerUpdateOptions
                            {
                                DefaultSource = card.Id
                            };
                            servicetoGetCustomer.Update(isCustomerExistinStrip.Value, optionsUpdate);
                        }

                    }

                    var options = new ChargeCreateOptions
                    {
                        Amount = total * 100,
                        Currency = Helper.GetCurrentCurrencyCode(order.StoreId, dbcontext),
                        Description = "Platform Charges",
                        Customer = customerId,
                        StatementDescriptor = "Custom description",
                        Source = cardid,
                        Metadata = new Dictionary<String, String>() { { "OrderId", "" + checkoutPaymentModel.OrderId } },
                    };
                    var service = new ChargeService();
                    charge = service.Create(options);
                }
                var orderNoteToDelete = dbcontext.OrderNote.Where(x => x.OrderId == checkoutPaymentModel.OrderId).ToList();
                foreach (var item in orderNoteToDelete)
                {
                    dbcontext.Entry(item).State = EntityState.Deleted;
                    dbcontext.SaveChanges();
                }
                PaymentCheckout paymentCheckout = new PaymentCheckout();
                var orderToUpdate = dbcontext.Order.Where(x => x.Id == checkoutPaymentModel.OrderId).FirstOrDefault();
                paymentCheckout.OrderNumber = orderToUpdate.CustomOrderNumber;
                paymentCheckout.OrderGuid = orderToUpdate.OrderGuid.ToString();
                var ProductsIds = dbcontext.OrderItem.Where(x => x.OrderId == checkoutPaymentModel.OrderId).ToList().Select(x => x.ProductId).ToList();
                int PrepareTime = 0;
                int ProductPrepareTime = 0;
                string VendorLatLong = string.Empty;

                foreach (var productId in ProductsIds)
                {
                    var productPrepareTime = (from p in dbcontext.Product
                                              where p.Id == productId
                                              select new
                                              {
                                                  p.PrepareTime
                                              }) != null ? (from p in dbcontext.Product
                                                            where p.Id == productId
                                                            select new
                                                            {
                                                                p.PrepareTime
                                                            }).FirstOrDefault().PrepareTime : 0;
                    if (productPrepareTime.HasValue)
                        ProductPrepareTime = productPrepareTime.Value + ProductPrepareTime;
                    var vendorId = dbcontext.Product.Where(x => x.Id == productId).Select(x => x.VendorId).FirstOrDefault();
                    VendorLatLong = dbcontext.Vendor.Where(x => x.Id == vendorId).FirstOrDefault().Geolocation;
                }
                PrepareTime = ProductPrepareTime / ProductsIds.Count();
                //int PId = ProductsIds.FirstOrDefault();
                //var VendorId = dbcontext.Products.Where(x => x.Id == PId).FirstOrDefault().VendorId;
                //var vendors = dbcontext.Vendors.Where(x => x.Id == VendorId).FirstOrDefault();

                if (!string.IsNullOrEmpty(VendorLatLong) && VendorLatLong.Contains("MARKER"))
                {
                    string[] venLtLngStrArr = VendorLatLong.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
                    if (venLtLngStrArr.Length > 0)
                    {
                        var AddressId = order.PickupInStore ? order.BillingAddressId : order.ShippingAddressId;
                        var customerAddress = dbcontext.Address.Where(x => x.Id == AddressId).FirstOrDefault();
                        double latRest = Convert.ToDouble(venLtLngStrArr[0]);
                        double longRest = Convert.ToDouble(venLtLngStrArr[1]);
                        double latCust = Convert.ToDouble(customerAddress.Latitude);
                        double longCust = Convert.ToDouble(customerAddress.Longitude);
                        string URl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latRest + "," + longRest + "&destination=" + latCust + "," + longCust + "&mode=transit&" + googleMapKey;
                        var request = (HttpWebRequest)WebRequest.Create(URl);
                        WebResponse response = request.GetResponse();
                        using (Stream dataStream = response.GetResponseStream())
                        {
                            // Open the stream using a StreamReader for easy access.  
                            StreamReader reader = new StreamReader(dataStream);
                            // Read the content.  
                            string responseFromServer = reader.ReadToEnd();
                            // Display the content.
                            var result = JsonConvert.DeserializeObject<GoogleTimeAPIModel>(responseFromServer);
                            //new JavaScriptSerializer().Deserialize<Friends>(result);
                            if (result.routes.Any())
                            {
                                var TravelTime = (result.routes.FirstOrDefault().legs.FirstOrDefault().duration.value) / 60;
                                PrepareTime = PrepareTime + TravelTime;
                            }

                            Console.WriteLine(responseFromServer);
                        }

                        //if (ret <= DistanceCovered)
                        //{
                        //    CooknRestlist.Add(item.Id);
                        //}
                    }
                }
                var createdOnUtc = Helper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc, context: dbcontext);
                var OrderFromTime = createdOnUtc.AddMinutes(PrepareTime).ToString("hh:mm tt");
                var OrderToTime = createdOnUtc.AddMinutes(PrepareTime + 10).ToString("hh:mm tt");
                paymentCheckout.OrderArrivalTime = OrderFromTime + "-" + OrderToTime;
                var ProductsToCheckout = dbcontext.ShoppingCartItem.Where(x => x.CustomerId == order.CustomerId && x.StoreId == order.StoreId).ToList();
                foreach (var item in ProductsToCheckout)
                {
                    dbcontext.Entry(item).State = EntityState.Deleted;
                    dbcontext.SaveChanges();
                }

                if ((checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.COD ||
                    (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal && checkoutPaymentModel.IsSuccess) ||
                    (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Square && checkoutPaymentModel.IsSuccess)) ||
                    (charge != null && charge.Status == "succeeded"))
                {
                    if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.COD)
                    {

                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Pending;
                        orderToUpdate.AuthorizationTransactionCode = Guid.NewGuid().ToString();
                        orderToUpdate.PaymentMethodSystemName = "Payments.CashOnDelivery";
                    }
                    else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal)
                    {

                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Paid;
                        orderToUpdate.AuthorizationTransactionCode = checkoutPaymentModel.PaymentId;
                        orderToUpdate.PaymentMethodSystemName = "Payments.Paypal";
                    }
                    else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Square)
                    {
                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Paid;
                        orderToUpdate.AuthorizationTransactionCode = checkoutPaymentModel.PaymentId;
                        orderToUpdate.PaymentMethodSystemName = "Payments.Square";
                    }
                    else
                    {
                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Paid;
                        orderToUpdate.AuthorizationTransactionCode = charge?.Id;
                    }
                    orderToUpdate.OrderStatusId = (int)OrderHistoryEnum.Received;
                    dbcontext.Entry(orderToUpdate).State = EntityState.Modified;
                    dbcontext.SaveChanges();
                    OrderNote orderNote = new OrderNote();
                    orderNote.CreatedOnUtc = DateTime.UtcNow;
                    orderNote.DisplayToCustomer = true;
                    orderNote.DownloadId = 0;
                    orderNote.Note = "Order Received & Payment Made Successfully";
                    orderNote.OrderStatus = (int)OrderHistoryEnum.Received;
                    orderNote.OrderId = checkoutPaymentModel.OrderId;
                    dbcontext.OrderNote.Add(orderNote);
                    dbcontext.SaveChanges();
                    OrderReviewModel orderReviewModel = new OrderReviewModel();
                    orderReviewModel.CustomerId = checkoutPaymentModel.CustomerId;
                    orderReviewModel.OrderId = checkoutPaymentModel.OrderId;
                    orderReviewModel.StoreId = orderToUpdate.StoreId;
                    orderReviewModel.UniqueSeoCode = checkoutPaymentModel.UniqueSeoCode;

                    // email after order place
                    Helper.SendEmailNotification(orderToUpdate, orderReviewModel, languageId, dbcontext);
                    var response = Helper.sendNotificationToCustomer(orderReviewModel, languageId, dbcontext);
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.PaymentSuccessfully", languageId, dbcontext);
                    customerAPIResponses.Status = true;
                    customerAPIResponses.StatusCode = 200;
                    customerAPIResponses.ResponseObj = paymentCheckout;
                    return customerAPIResponses;
                }
                else
                {
                    if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal || checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Square)
                    {
                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Failed;
                        orderToUpdate.OrderStatusId = (int)OrderHistoryEnum.Cancelled;
                        orderToUpdate.AuthorizationTransactionCode = checkoutPaymentModel.PaymentId;
                        dbcontext.Entry(orderToUpdate).State = EntityState.Modified;
                        dbcontext.SaveChanges();
                    }
                    OrderNote orderNote = new OrderNote();
                    orderNote.CreatedOnUtc = DateTime.UtcNow;
                    orderNote.DisplayToCustomer = true;
                    orderNote.DownloadId = 0;
                    orderNote.Note = "Order Cancelled due to payment failed.";
                    orderNote.OrderStatus = (int)OrderHistoryEnum.Cancelled;
                    orderNote.OrderId = checkoutPaymentModel.OrderId;
                    dbcontext.OrderNote.Add(orderNote);
                    dbcontext.SaveChanges();
                    customerAPIResponses.ErrorMessage = charge != null ? charge.FailureMessage : LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.PaymentFailed", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = 400;
                    customerAPIResponses.ResponseObj = paymentCheckout;
                    return customerAPIResponses;
                }


            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = 400;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
        }

        [Route("~/api/v2.1/CheckoutPayment")]
        [HttpPost]
        public CustomerAPIResponses CheckoutPaymentv2_1([FromBody] CheckoutPaymentModelV2 checkoutPaymentModel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(checkoutPaymentModel.StoreId, checkoutPaymentModel.UniqueSeoCode, dbcontext);

            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (checkoutPaymentModel == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(checkoutPaymentModel.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (checkoutPaymentModel.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Customer Id Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (checkoutPaymentModel.OrderId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.OrderIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            try
            {
                if (!string.IsNullOrEmpty(checkoutPaymentModel.ApiKey))
                {
                    var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == checkoutPaymentModel.ApiKey).Any();
                    if (!keyExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }
                if (checkoutPaymentModel.OrderId != 0)
                {
                    var keyExist = dbcontext.Order.Where(x => x.Id == checkoutPaymentModel.OrderId).Any();
                    if (!keyExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.OrderNotFound", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }

                var order = dbcontext.Order.Where(x => x.Id == checkoutPaymentModel.OrderId).FirstOrDefault();
                long total = 0;

                total = Convert.ToInt64(order.OrderTotal);
                Charge charge = null;
                if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.COD)
                {
                    checkoutPaymentModel.IsSuccess = true;
                }
                else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal && checkoutPaymentModel.IsSuccess)
                {
                    checkoutPaymentModel.IsSuccess = true;
                }
                else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Square && checkoutPaymentModel.IsSuccess)
                {
                    checkoutPaymentModel.IsSuccess = true;
                }
                else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Card)
                {
                    StripeConfiguration.ApiKey = Helper.GetStripeKey(order.StoreId, dbcontext);
                    string customerId = string.Empty;
                    string cardid = string.Empty;
                    var isCustomerExistinStrip = dbcontext.GenericAttribute.Where(x => x.EntityId == checkoutPaymentModel.CustomerId && x.Key == "CustomerStripeId").FirstOrDefault();
                    if (!checkoutPaymentModel.IsNew)
                    {
                        customerId = isCustomerExistinStrip.Value;
                        cardid = checkoutPaymentModel.CardId;
                    }
                    // Token is created using Checkout or Elements!
                    // Get the payment token submitted by the form:
                    else
                    {
                        bool PaymentFlag = false;
                        string newToken = string.Empty;
                        if (isCustomerExistinStrip != null)
                        {
                            var service1 = new TokenService();
                            var token = service1.Get(checkoutPaymentModel.PaymentToken);

                            if (token != null && token.Id != null)
                            {
                                var CardService = new CardService();
                                var CardListOptions = new CardListOptions
                                {
                                    Limit = 15,
                                };
                                List<Card> cards = CardService.List(isCustomerExistinStrip.Value, CardListOptions).ToList();
                                if (cards.Any())
                                {
                                    foreach (var item in cards)
                                    {
                                        if (item.Fingerprint.Equals(token.Card.Fingerprint))
                                        {
                                            customerId = item.CustomerId;
                                            cardid = item.Id;
                                            PaymentFlag = true;
                                            var servicetoGetCustomer = new CustomerService();
                                            var customer = servicetoGetCustomer.Get(isCustomerExistinStrip.Value);
                                            var optionsUpdate = new CustomerUpdateOptions
                                            {
                                                DefaultSource = item.Id
                                            };
                                            servicetoGetCustomer.Update(isCustomerExistinStrip.Value, optionsUpdate);
                                        }
                                    }

                                }
                            }
                        }
                        else
                        {
                            var customerExist = dbcontext.Customer.Where(x => x.Id == checkoutPaymentModel.CustomerId).FirstOrDefault();
                            var optionsCustomer = new CustomerCreateOptions
                            {
                                Description = "Customer for " + customerExist.Email,
                                Email = customerExist.Email,
                            };
                            var serviceCustomer = new CustomerService();
                            var customer = serviceCustomer.Create(optionsCustomer);

                            GenericAttribute genericAttribute = new GenericAttribute();
                            genericAttribute.KeyGroup = "Customer";
                            genericAttribute.StoreId = order.StoreId;
                            genericAttribute.Key = "CustomerStripeId";
                            genericAttribute.Value = customer.Id;
                            genericAttribute.EntityId = checkoutPaymentModel.CustomerId;
                            dbcontext.GenericAttribute.Add(genericAttribute);
                            dbcontext.SaveChanges();
                            PaymentFlag = false;
                        }

                        if (!PaymentFlag)
                        {
                            var cardCreateOptions = new CardCreateOptions
                            {
                                Source = checkoutPaymentModel.PaymentToken,
                                //C
                            };
                            var cardService = new CardService();
                            if (isCustomerExistinStrip == null)
                            {
                                isCustomerExistinStrip = dbcontext.GenericAttribute.Where(x => x.EntityId == checkoutPaymentModel.CustomerId && x.Key == "CustomerStripeId").FirstOrDefault();
                            }
                            Card card = cardService.Create(isCustomerExistinStrip.Value, cardCreateOptions);
                            //card.tok                   
                            //  newToken = card.;
                            customerId = card.CustomerId;
                            cardid = card.Id;
                            var servicetoGetCustomer = new CustomerService();
                            var customer = servicetoGetCustomer.Get(isCustomerExistinStrip.Value);
                            var optionsUpdate = new CustomerUpdateOptions
                            {
                                DefaultSource = card.Id
                            };
                            servicetoGetCustomer.Update(isCustomerExistinStrip.Value, optionsUpdate);
                        }

                    }

                    var options = new ChargeCreateOptions
                    {
                        Amount = total * 100,
                        Currency = Helper.GetCurrentCurrencyCode(order.StoreId, dbcontext),
                        Description = "Platform Charges",
                        Customer = customerId,
                        StatementDescriptor = "Custom description",
                        Source = cardid,
                        Metadata = new Dictionary<String, String>() { { "OrderId", "" + checkoutPaymentModel.OrderId } },
                    };
                    var service = new ChargeService();
                    charge = service.Create(options);
                }
                var orderNoteToDelete = dbcontext.OrderNote.Where(x => x.OrderId == checkoutPaymentModel.OrderId).ToList();
                foreach (var item in orderNoteToDelete)
                {
                    dbcontext.Entry(item).State = EntityState.Deleted;
                    dbcontext.SaveChanges();
                }
                PaymentCheckout paymentCheckout = new PaymentCheckout();
                var orderToUpdate = dbcontext.Order.Where(x => x.Id == checkoutPaymentModel.OrderId).FirstOrDefault();
                paymentCheckout.OrderNumber = orderToUpdate.CustomOrderNumber;
                paymentCheckout.OrderGuid = orderToUpdate.OrderGuid.ToString();
                var ProductsIds = dbcontext.OrderItem.Where(x => x.OrderId == checkoutPaymentModel.OrderId).ToList().Select(x => x.ProductId).ToList();
                int PrepareTime = 0;
                int ProductPrepareTime = 0;
                string VendorLatLong = string.Empty;

                foreach (var productId in ProductsIds)
                {
                    var productPrepareTime = (from p in dbcontext.Product
                                              where p.Id == productId
                                              select new
                                              {
                                                  p.PrepareTime
                                              }) != null ? (from p in dbcontext.Product
                                                            where p.Id == productId
                                                            select new
                                                            {
                                                                p.PrepareTime
                                                            }).FirstOrDefault().PrepareTime : 0;
                    if (productPrepareTime.HasValue)
                        ProductPrepareTime = productPrepareTime.Value + ProductPrepareTime;
                    var vendorId = dbcontext.Product.Where(x => x.Id == productId).Select(x => x.VendorId).FirstOrDefault();
                    VendorLatLong = dbcontext.Vendor.Where(x => x.Id == vendorId).FirstOrDefault().Geolocation;
                }
                PrepareTime = ProductPrepareTime / ProductsIds.Count();
                //int PId = ProductsIds.FirstOrDefault();
                //var VendorId = dbcontext.Products.Where(x => x.Id == PId).FirstOrDefault().VendorId;
                //var vendors = dbcontext.Vendors.Where(x => x.Id == VendorId).FirstOrDefault();

                if (!string.IsNullOrEmpty(VendorLatLong) && VendorLatLong.Contains("MARKER"))
                {
                    string[] venLtLngStrArr = VendorLatLong.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
                    if (venLtLngStrArr.Length > 0)
                    {
                        var AddressId = order.PickupInStore ? order.BillingAddressId : order.ShippingAddressId;
                        var customerAddress = dbcontext.Address.Where(x => x.Id == AddressId).FirstOrDefault();
                        double latRest = Convert.ToDouble(venLtLngStrArr[0]);
                        double longRest = Convert.ToDouble(venLtLngStrArr[1]);
                        double latCust = Convert.ToDouble(customerAddress.Latitude);
                        double longCust = Convert.ToDouble(customerAddress.Longitude);
                        string URl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latRest + "," + longRest + "&destination=" + latCust + "," + longCust + "&mode=transit&" + googleMapKey;
                        var request = (HttpWebRequest)WebRequest.Create(URl);
                        WebResponse response = request.GetResponse();
                        using (Stream dataStream = response.GetResponseStream())
                        {
                            // Open the stream using a StreamReader for easy access.  
                            StreamReader reader = new StreamReader(dataStream);
                            // Read the content.  
                            string responseFromServer = reader.ReadToEnd();
                            // Display the content.
                            var result = JsonConvert.DeserializeObject<GoogleTimeAPIModel>(responseFromServer);
                            //new JavaScriptSerializer().Deserialize<Friends>(result);
                            if (result.routes.Any())
                            {
                                var TravelTime = (result.routes.FirstOrDefault().legs.FirstOrDefault().duration.value) / 60;
                                PrepareTime = PrepareTime + TravelTime;
                            }

                            Console.WriteLine(responseFromServer);
                        }

                        //if (ret <= DistanceCovered)
                        //{
                        //    CooknRestlist.Add(item.Id);
                        //}
                    }
                }
                var createdOnUtc = Helper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc, context: dbcontext);
                var OrderFromTime = createdOnUtc.AddMinutes(PrepareTime).ToString("hh:mm tt");
                var OrderToTime = createdOnUtc.AddMinutes(PrepareTime + 10).ToString("hh:mm tt");
                paymentCheckout.OrderArrivalTime = OrderFromTime + "-" + OrderToTime;
                var ProductsToCheckout = dbcontext.ShoppingCartItem.Where(x => x.CustomerId == order.CustomerId && x.StoreId == order.StoreId).ToList();
                foreach (var item in ProductsToCheckout)
                {
                    dbcontext.Entry(item).State = EntityState.Deleted;
                    dbcontext.SaveChanges();
                }

                if ((checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.COD ||
                   (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal && checkoutPaymentModel.IsSuccess) ||
                   (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Square && checkoutPaymentModel.IsSuccess)) ||
                   (charge != null && charge.Status == "succeeded"))
                {
                    if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.COD)
                    {
                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Pending;
                        orderToUpdate.AuthorizationTransactionCode = Guid.NewGuid().ToString();
                        orderToUpdate.PaymentMethodSystemName = "Payments.CashOnDelivery";
                    }
                    else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal)
                    {

                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Paid;
                        orderToUpdate.AuthorizationTransactionCode = checkoutPaymentModel.PaymentId;
                        orderToUpdate.PaymentMethodSystemName = "Payments.Paypal";
                    }
                    else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Square)
                    {
                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Paid;
                        orderToUpdate.AuthorizationTransactionCode = checkoutPaymentModel.PaymentId;
                        orderToUpdate.PaymentMethodSystemName = "Payments.Square";
                    }
                    orderToUpdate.OrderStatusId = (int)OrderHistoryEnum.Received;
                    dbcontext.Entry(orderToUpdate).State = EntityState.Modified;
                    dbcontext.SaveChanges();
                    OrderNote orderNote = new OrderNote();
                    orderNote.CreatedOnUtc = DateTime.UtcNow;
                    orderNote.DisplayToCustomer = true;
                    orderNote.DownloadId = 0;
                    orderNote.Note = "Order Received & Payment Made Successfully";
                    orderNote.OrderStatus = (int)OrderHistoryEnum.Received;
                    orderNote.OrderId = checkoutPaymentModel.OrderId;
                    dbcontext.OrderNote.Add(orderNote);
                    dbcontext.SaveChanges();
                    OrderReviewModel orderReviewModel = new OrderReviewModel();
                    orderReviewModel.CustomerId = checkoutPaymentModel.CustomerId;
                    orderReviewModel.OrderId = checkoutPaymentModel.OrderId;
                    orderReviewModel.StoreId = orderToUpdate.StoreId;
                    orderReviewModel.UniqueSeoCode = checkoutPaymentModel.UniqueSeoCode;

                    // email after order place
                    Helper.SendEmailNotification(orderToUpdate, orderReviewModel, languageId, dbcontext);
                    var response = Helper.sendNotificationToCustomerV2_1(orderReviewModel, languageId, dbcontext, checkoutPaymentModel.CustomerId);
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.PaymentSuccessfully", languageId, dbcontext);
                    customerAPIResponses.Status = true;
                    customerAPIResponses.StatusCode = 200;
                    customerAPIResponses.ResponseObj = paymentCheckout;
                    return customerAPIResponses;
                }
                else
                {
                    if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal || checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Square)
                    {
                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Failed;
                        orderToUpdate.OrderStatusId = (int)OrderHistoryEnum.Cancelled;
                        orderToUpdate.AuthorizationTransactionCode = checkoutPaymentModel.PaymentId;
                        dbcontext.Entry(orderToUpdate).State = EntityState.Modified;
                        dbcontext.SaveChanges();
                    }

                    OrderNote orderNote = new OrderNote();
                    orderNote.CreatedOnUtc = DateTime.UtcNow;
                    orderNote.DisplayToCustomer = true;
                    orderNote.DownloadId = 0;
                    orderNote.Note = "Order Cancelled due to payment failed.";
                    orderNote.OrderStatus = (int)OrderHistoryEnum.Cancelled;
                    orderNote.OrderId = checkoutPaymentModel.OrderId;
                    dbcontext.OrderNote.Add(orderNote);
                    dbcontext.SaveChanges();
                    customerAPIResponses.ErrorMessage = charge != null ? charge.FailureMessage : LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.PaymentFailed", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = 400;
                    customerAPIResponses.ResponseObj = paymentCheckout;
                    return customerAPIResponses;
                }


            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = 400;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
        }

        [Route("~/api/v1/GetAllPaymentMethods")]
        [HttpPost]
        public CustomerAPIResponses GetAllPaymentMethods([FromBody] CustomerDetailsModel cartProductsModel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(cartProductsModel.StoreId, cartProductsModel.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (cartProductsModel == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(cartProductsModel.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (cartProductsModel.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Customer Id Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (cartProductsModel.StoreId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Store Id Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            try
            {
                if (!string.IsNullOrEmpty(cartProductsModel.ApiKey))
                {
                    var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == cartProductsModel.ApiKey).Any();
                    if (!keyExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }
                var isCustomerExist = dbcontext.Customer.Where(x => x.Id == cartProductsModel.CustomerId).Any();
                if (!isCustomerExist)
                {
                    customerAPIResponses.ErrorMessageTitle = "Customer";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                var VendorInfo = (from a in dbcontext.ShoppingCartItem.Where(x => x.CustomerId == cartProductsModel.CustomerId)
                                  join b in dbcontext.Product
                                  on a.ProductId equals b.Id
                                  select new
                                  {
                                      b.VendorId
                                  }).FirstOrDefault();
                //var IsCODSHOW = dbcontext.Vendors.Where(x => x.Id == VendorInfo.VendorId).FirstOrDefault().Name.ToLower().Contains("subscription");
                List<PaymentMethodModel> paymentMethodModels = new List<PaymentMethodModel>();
                PaymentMethodModel paymentMethodModel3 = new PaymentMethodModel();
                paymentMethodModel3.PaymentMethodId = (int)PaymentMethodEnum.PayPal;
                paymentMethodModel3.PaymentMethodName = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllPaymentMethods.Paypal", languageId, dbcontext);
                paymentMethodModel3.isShow = false;
                paymentMethodModels.Add(paymentMethodModel3);

                paymentMethodModel3 = new PaymentMethodModel();
                paymentMethodModel3.PaymentMethodId = (int)PaymentMethodEnum.Card;
                paymentMethodModel3.PaymentMethodName = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllPaymentMethods.CreditDebitCart", languageId, dbcontext);
                paymentMethodModel3.isShow = false;
                paymentMethodModels.Add(paymentMethodModel3);

                paymentMethodModel3 = new PaymentMethodModel();
                paymentMethodModel3.PaymentMethodId = (int)PaymentMethodEnum.COD;
                paymentMethodModel3.PaymentMethodName = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllPaymentMethods.COD", languageId, dbcontext);
                paymentMethodModel3.isShow = true;
                paymentMethodModels.Add(paymentMethodModel3);

                paymentMethodModel3 = new PaymentMethodModel();
                paymentMethodModel3.PaymentMethodId = (int)PaymentMethodEnum.Square;
                paymentMethodModel3.PaymentMethodName = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllPaymentMethods.Square", languageId, dbcontext);
                paymentMethodModel3.isShow = true;
                paymentMethodModels.Add(paymentMethodModel3);

                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.SuccessMessage", languageId, dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                customerAPIResponses.ResponseObj = paymentMethodModels;
                return customerAPIResponses;
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = 400;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }

        }

        [Route("~/api/v1/GetPaypalToken")]
        [HttpPost]
        public async Task<CustomerAPIResponses> GetPaypalToken([FromBody] CustomerDetailsModel cartProductsModel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(cartProductsModel.StoreId, cartProductsModel.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (cartProductsModel == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(cartProductsModel.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Error!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (cartProductsModel.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (cartProductsModel.StoreId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.StoreIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            try
            {
                if (!string.IsNullOrEmpty(cartProductsModel.ApiKey))
                {
                    var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == cartProductsModel.ApiKey).Any();
                    if (!keyExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }
                var isCustomerExist = dbcontext.Customer.Where(x => x.Id == cartProductsModel.CustomerId).Any();
                if (!isCustomerExist)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                string useYourAccessToken = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("paypalaccesstoken") && x.StoreId == cartProductsModel.StoreId).FirstOrDefault()?.Value;
                BraintreeGateway gateway = new BraintreeGateway(useYourAccessToken);
                var clientToken = gateway.ClientToken.Generate();
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.SuccessMessage", languageId, dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                customerAPIResponses.ResponseObj = clientToken;
                return customerAPIResponses;
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = 400;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }

        }

        [Route("~/api/v1/SavePaymentCard")]
        [HttpPost]
        public CustomerAPIResponses SavePaymentCard(CardModel cardModel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(cardModel.StoreId, cardModel.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (cardModel == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cardModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(cardModel.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cardModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(cardModel.CardToken))
            {
                customerAPIResponses.ErrorMessageTitle = "Card Token Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cardModel.StoreId, "API.ErrorMesaage.CardTokenNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (cardModel.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Customer Id Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cardModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }

            try
            {
                if (!string.IsNullOrEmpty(cardModel.ApiKey))
                {
                    var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == cardModel.ApiKey).Any();
                    if (!keyExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = " Invalid Authentication key";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cardModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }
                if (string.IsNullOrEmpty(stripeKey))
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cardModel.StoreId, "API.ErrorMesaage.ProblemOfStripePrivateKey", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else
                {

                    StripeConfiguration.ApiKey = Helper.GetStripeKey(cardModel.StoreId, dbcontext);
                    var customerExist = dbcontext.Customer.Where(x => x.Id == cardModel.CustomerId && x.RegisteredInStoreId == cardModel.StoreId).FirstOrDefault();
                    if (customerExist == null)
                    {
                        customerAPIResponses.ErrorMessage = "Customer Not Found";
                        customerAPIResponses.ErrorMessageTitle = LanguageHelper.GetResourseValueByName(cardModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                    var isCustomerExistinStrip = dbcontext.GenericAttribute.Where(x => x.EntityId == cardModel.CustomerId && x.Key == "CustomerStripeId").FirstOrDefault();
                    if (isCustomerExistinStrip != null)
                    {

                        var service1 = new TokenService();
                        var token = service1.Get(cardModel.CardToken);
                        if (token != null && token.Id != null)
                        {
                            var service = new CardService();
                            var options = new CardListOptions
                            {
                                Limit = 15,
                            };
                            List<Card> cards = service.List(isCustomerExistinStrip.Value, options).ToList();
                            if (cards.Any())
                            {
                                foreach (var item in cards)
                                {
                                    if (item.Fingerprint.Equals(token.Card.Fingerprint))
                                    {
                                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cardModel.StoreId, "API.ErrorMesaage.CardExist", languageId, dbcontext);
                                        customerAPIResponses.Status = false;
                                        customerAPIResponses.StatusCode = 400;
                                        customerAPIResponses.ResponseObj = null;
                                        return customerAPIResponses;
                                    }
                                }
                                var cardCreateOptions = new CardCreateOptions
                                {
                                    Source = cardModel.CardToken,
                                    // SourceCard 
                                };
                                var cardService = new CardService();
                                Card card = cardService.Create(isCustomerExistinStrip.Value, cardCreateOptions);
                            }
                            else
                            {
                                var cardCreateOptions = new CardCreateOptions
                                {
                                    Source = cardModel.CardToken,
                                    // SourceCard 
                                };
                                var cardService = new CardService();

                                Card card = cardService.Create(isCustomerExistinStrip.Value, cardCreateOptions);

                            }

                        }
                        else
                        {
                            var cardCreateOptions = new CardCreateOptions
                            {
                                Source = cardModel.CardToken,
                                // SourceCard 
                            };
                            var cardService = new CardService();

                            Card card = cardService.Create(isCustomerExistinStrip.Value, cardCreateOptions);

                        }

                    }
                    else
                    {
                        var options = new CustomerCreateOptions
                        {
                            Description = "Customer for " + customerExist.Email,
                            Email = customerExist.Email,
                        };

                        var service = new CustomerService();
                        var customer = service.Create(options);
                        var cardCreateOptions = new CardCreateOptions
                        {
                            Source = cardModel.CardToken,
                            // SourceCard 
                        };

                        GenericAttribute genericAttribute = new GenericAttribute();
                        genericAttribute.KeyGroup = "Customer";
                        genericAttribute.StoreId = cardModel.StoreId;
                        genericAttribute.Key = "CustomerStripeId";
                        genericAttribute.Value = customer.Id;
                        genericAttribute.EntityId = customerExist.Id;
                        dbcontext.GenericAttribute.Add(genericAttribute);
                        dbcontext.SaveChanges();
                        var cardService = new CardService();
                        var card = cardService.Create(customer.Id, cardCreateOptions);
                    }

                }

                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cardModel.StoreId, "API.ErrorMesaage.CardAddedSuccessfully", languageId, dbcontext);
                customerAPIResponses.Status = true;
                customerAPIResponses.StatusCode = 200;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = 400;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
        }
        [Route("~/api/v1/DeletePaymentCard")]
        [HttpPost]
        public CustomerAPIResponses DeletePaymentCard([FromBody] DeleteCardModel cartProductsModel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(cartProductsModel.StoreId, cartProductsModel.UniqueSeoCode, dbcontext);
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (cartProductsModel == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(cartProductsModel.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (cartProductsModel.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Customer Id Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(cartProductsModel.CardId))
            {
                customerAPIResponses.ErrorMessageTitle = "Card Token Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CardTokenNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            try
            {
                if (!string.IsNullOrEmpty(cartProductsModel.ApiKey))
                {
                    var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == cartProductsModel.ApiKey).Any();
                    if (!keyExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }
                var isCustomerExist = dbcontext.Customer.Where(x => x.Id == cartProductsModel.CustomerId).Any();
                if (!isCustomerExist)
                {
                    customerAPIResponses.ErrorMessageTitle = "Customer";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else
                {
                    var customerStripeKey = dbcontext.GenericAttribute.Where(x => x.EntityId == cartProductsModel.CustomerId
                     && x.Key == "CustomerStripeId").FirstOrDefault();
                    if (customerStripeKey != null)
                    {
                        StripeConfiguration.ApiKey = Helper.GetStripeKey(customerStripeKey.StoreId, dbcontext);

                        var service = new CardService();
                        var response = service.Delete(customerStripeKey.Value, cartProductsModel.CardId);
                        if (response != null)
                        {
                            if (response.Deleted.Value)
                            {
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CardDeletedSuccessfuly", languageId, dbcontext);
                                customerAPIResponses.Status = true;
                                customerAPIResponses.StatusCode = 200;
                                customerAPIResponses.ResponseObj = null;
                                return customerAPIResponses;
                            }
                            else
                            {
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CardNotDeleted", languageId, dbcontext);
                                customerAPIResponses.Status = false;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                customerAPIResponses.ResponseObj = null;
                                return customerAPIResponses;
                            }
                        }
                        else
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CardNotDeleted", languageId, dbcontext);
                            customerAPIResponses.Status = false;
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                            customerAPIResponses.ResponseObj = null;
                            return customerAPIResponses;

                        }
                    }
                    else
                    {
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = 400;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
        }
        [Route("~/api/v1/GetAllPaymentCard")]
        [HttpPost]
        public CustomerAPIResponses GetAllPaymentCard([FromBody] CustomerDetailsModel cartProductsModel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(cartProductsModel.StoreId, cartProductsModel.UniqueSeoCode, dbcontext);

            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (cartProductsModel == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(cartProductsModel.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (cartProductsModel.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Customer Id Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            try
            {
                if (!string.IsNullOrEmpty(cartProductsModel.ApiKey))
                {
                    var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == cartProductsModel.ApiKey).Any();
                    if (!keyExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }
                var isCustomerExist = dbcontext.Customer.Where(x => x.Id == cartProductsModel.CustomerId).Any();
                if (!isCustomerExist)
                {
                    customerAPIResponses.ErrorMessageTitle = "Customer";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                else
                {
                    var customerStripeKey = dbcontext.GenericAttribute.Where(x => x.EntityId == cartProductsModel.CustomerId
                     && x.Key == "CustomerStripeId").FirstOrDefault();
                    if (customerStripeKey != null)
                    {
                        //StripeConfiguration.SetApiKey(stripeKey);

                        //var CardService = new CardService();
                        //var CardListOptions = new CardListOptions
                        //{
                        //    Limit=10
                        //};
                        //List<Cards> cards = CardService.List(customerStripeKey.Value, CardListOptions);
                        StripeConfiguration.ApiKey = Helper.GetStripeKey(customerStripeKey.StoreId, dbcontext);

                        var service = new CardService();
                        var options = new CardListOptions
                        {
                            Limit = 15,
                        };
                        List<Card> cards = service.List(customerStripeKey.Value, options).ToList();

                        if (cards.Any())
                        {
                            customerAPIResponses.Status = true;
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                            customerAPIResponses.ResponseObj = cards;
                            return customerAPIResponses;
                        }
                        else
                        {
                            customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.NoCardFound", languageId, dbcontext);
                            customerAPIResponses.Status = false;
                            customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                            customerAPIResponses.ResponseObj = null;
                            return customerAPIResponses;
                        }
                    }
                    else
                    {
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(cartProductsModel.StoreId, "API.ErrorMesaage.NoCardFound", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = 400;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }

        }

        [Route("~/api/v1/CheckoutPaymentWithPaypal")]
        [HttpPost]
        public CustomerAPIResponses CheckoutPaymentWithPaypal([FromBody] CheckoutPaymentModelV2 checkoutPaymentModel)
        {
            int languageId = LanguageHelper.GetIdByLangCode(checkoutPaymentModel.StoreId, checkoutPaymentModel.UniqueSeoCode, dbcontext);

            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
            if (checkoutPaymentModel == null)
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(checkoutPaymentModel.ApiKey))
            {
                customerAPIResponses.ErrorMessageTitle = "Key Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.KeyNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (checkoutPaymentModel.CustomerId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Customer Id Not Defined";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.CustomerNotDefined", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            if (checkoutPaymentModel.OrderId == 0)
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.OrderIdMissing", languageId, dbcontext);
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
            try
            {
                if (!string.IsNullOrEmpty(checkoutPaymentModel.ApiKey))
                {
                    var keyExist = dbcontext.VersionInfo.Where(x => x.Apikey == checkoutPaymentModel.ApiKey).Any();
                    if (!keyExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Invalid Authentication key";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }
                if (checkoutPaymentModel.OrderId != 0)
                {
                    var keyExist = dbcontext.Order.Where(x => x.Id == checkoutPaymentModel.OrderId).Any();
                    if (!keyExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.OrderNotFound", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                }

                var order = dbcontext.Order.Where(x => x.Id == checkoutPaymentModel.OrderId).FirstOrDefault();
                long total = 0;

                total = Convert.ToInt64(order.OrderTotal);
                Charge charge = null;

                PaymentCheckout paymentCheckout = new PaymentCheckout();

                if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.COD)
                {
                    checkoutPaymentModel.IsSuccess = true;
                }
                else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal)
                {
                    checkoutPaymentModel.IsSuccess = false;
                    paymentCheckout.PayPalurl = Helper.GetPayPalPaymentUrl(order, dbcontext, this.HttpContext);
                }
                else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Square && checkoutPaymentModel.IsSuccess)
                {
                    checkoutPaymentModel.IsSuccess = true;
                }
                else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Card)
                {
                    StripeConfiguration.ApiKey = Helper.GetStripeKey(order.StoreId, dbcontext);
                    string customerId = string.Empty;
                    string cardid = string.Empty;
                    var isCustomerExistinStrip = dbcontext.GenericAttribute.Where(x => x.EntityId == checkoutPaymentModel.CustomerId && x.Key == "CustomerStripeId").FirstOrDefault();
                    if (!checkoutPaymentModel.IsNew)
                    {
                        customerId = isCustomerExistinStrip.Value;
                        cardid = checkoutPaymentModel.CardId;
                    }
                    // Token is created using Checkout or Elements!
                    // Get the payment token submitted by the form:
                    else
                    {
                        bool PaymentFlag = false;
                        string newToken = string.Empty;
                        if (isCustomerExistinStrip != null)
                        {
                            var service1 = new TokenService();
                            var token = service1.Get(checkoutPaymentModel.PaymentToken);

                            if (token != null && token.Id != null)
                            {
                                var CardService = new CardService();
                                var CardListOptions = new CardListOptions
                                {
                                    Limit = 15,
                                };
                                List<Card> cards = CardService.List(isCustomerExistinStrip.Value, CardListOptions).ToList();
                                if (cards.Any())
                                {
                                    foreach (var item in cards)
                                    {
                                        if (item.Fingerprint.Equals(token.Card.Fingerprint))
                                        {
                                            customerId = item.CustomerId;
                                            cardid = item.Id;
                                            PaymentFlag = true;
                                            var servicetoGetCustomer = new CustomerService();
                                            var customer = servicetoGetCustomer.Get(isCustomerExistinStrip.Value);
                                            var optionsUpdate = new CustomerUpdateOptions
                                            {
                                                DefaultSource = item.Id
                                            };
                                            servicetoGetCustomer.Update(isCustomerExistinStrip.Value, optionsUpdate);
                                        }
                                    }

                                }
                            }
                        }
                        else
                        {
                            var customerExist = dbcontext.Customer.Where(x => x.Id == checkoutPaymentModel.CustomerId).FirstOrDefault();
                            var optionsCustomer = new CustomerCreateOptions
                            {
                                Description = "Customer for " + customerExist.Email,
                                Email = customerExist.Email,
                            };
                            var serviceCustomer = new CustomerService();
                            var customer = serviceCustomer.Create(optionsCustomer);

                            GenericAttribute genericAttribute = new GenericAttribute();
                            genericAttribute.KeyGroup = "Customer";
                            genericAttribute.StoreId = order.StoreId;
                            genericAttribute.Key = "CustomerStripeId";
                            genericAttribute.Value = customer.Id;
                            genericAttribute.EntityId = checkoutPaymentModel.CustomerId;
                            dbcontext.GenericAttribute.Add(genericAttribute);
                            dbcontext.SaveChanges();
                            PaymentFlag = false;
                        }

                        if (!PaymentFlag)
                        {
                            var cardCreateOptions = new CardCreateOptions
                            {
                                Source = checkoutPaymentModel.PaymentToken,
                                //C
                            };
                            var cardService = new CardService();
                            if (isCustomerExistinStrip == null)
                            {
                                isCustomerExistinStrip = dbcontext.GenericAttribute.Where(x => x.EntityId == checkoutPaymentModel.CustomerId && x.Key == "CustomerStripeId").FirstOrDefault();
                            }
                            Card card = cardService.Create(isCustomerExistinStrip.Value, cardCreateOptions);
                            //card.tok                   
                            //  newToken = card.;
                            customerId = card.CustomerId;
                            cardid = card.Id;
                            var servicetoGetCustomer = new CustomerService();
                            var customer = servicetoGetCustomer.Get(isCustomerExistinStrip.Value);
                            var optionsUpdate = new CustomerUpdateOptions
                            {
                                DefaultSource = card.Id
                            };
                            servicetoGetCustomer.Update(isCustomerExistinStrip.Value, optionsUpdate);
                        }

                    }

                    var options = new ChargeCreateOptions
                    {
                        Amount = total * 100,
                        Currency = Helper.GetCurrentCurrencyCode(order.StoreId, dbcontext),
                        Description = "Platform Charges",
                        Customer = customerId,
                        StatementDescriptor = "Custom description",
                        Source = cardid,
                        Metadata = new Dictionary<String, String>() { { "OrderId", "" + checkoutPaymentModel.OrderId } },
                    };
                    var service = new ChargeService();
                    charge = service.Create(options);
                }
                var orderNoteToDelete = dbcontext.OrderNote.Where(x => x.OrderId == checkoutPaymentModel.OrderId).ToList();
                foreach (var item in orderNoteToDelete)
                {
                    dbcontext.Entry(item).State = EntityState.Deleted;
                    dbcontext.SaveChanges();
                }

                var orderToUpdate = dbcontext.Order.Where(x => x.Id == checkoutPaymentModel.OrderId).FirstOrDefault();
                paymentCheckout.OrderNumber = orderToUpdate.CustomOrderNumber;
                paymentCheckout.OrderGuid = orderToUpdate.OrderGuid.ToString();
                var ProductsIds = dbcontext.OrderItem.Where(x => x.OrderId == checkoutPaymentModel.OrderId).ToList().Select(x => x.ProductId).ToList();
                int PrepareTime = 0;
                int ProductPrepareTime = 0;
                string VendorLatLong = string.Empty;

                foreach (var productId in ProductsIds)
                {
                    var productPrepareTime = (from p in dbcontext.Product
                                              where p.Id == productId
                                              select new
                                              {
                                                  p.PrepareTime
                                              }) != null ? (from p in dbcontext.Product
                                                            where p.Id == productId
                                                            select new
                                                            {
                                                                p.PrepareTime
                                                            }).FirstOrDefault().PrepareTime : 0;
                    if (productPrepareTime.HasValue)
                        ProductPrepareTime = productPrepareTime.Value + ProductPrepareTime;
                    var vendorId = dbcontext.Product.Where(x => x.Id == productId).Select(x => x.VendorId).FirstOrDefault();
                    VendorLatLong = dbcontext.Vendor.Where(x => x.Id == vendorId).FirstOrDefault().Geolocation;
                }
                PrepareTime = ProductPrepareTime / ProductsIds.Count();
                
                if (!string.IsNullOrEmpty(VendorLatLong) && VendorLatLong.Contains("MARKER"))
                {
                    string[] venLtLngStrArr = VendorLatLong.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
                    if (venLtLngStrArr.Length > 0)
                    {
                        var AddressId = order.PickupInStore ? order.BillingAddressId : order.ShippingAddressId;
                        var customerAddress = dbcontext.Address.Where(x => x.Id == AddressId).FirstOrDefault();
                        double latRest = Convert.ToDouble(venLtLngStrArr[0]);
                        double longRest = Convert.ToDouble(venLtLngStrArr[1]);
                        double latCust = Convert.ToDouble(customerAddress.Latitude);
                        double longCust = Convert.ToDouble(customerAddress.Longitude);
                        string URl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latRest + "," + longRest + "&destination=" + latCust + "," + longCust + "&mode=transit&" + googleMapKey;
                        var request = (HttpWebRequest)WebRequest.Create(URl);
                        WebResponse response = request.GetResponse();
                        using (Stream dataStream = response.GetResponseStream())
                        {
                            // Open the stream using a StreamReader for easy access.  
                            StreamReader reader = new StreamReader(dataStream);
                            // Read the content.  
                            string responseFromServer = reader.ReadToEnd();
                            // Display the content.
                            var result = JsonConvert.DeserializeObject<GoogleTimeAPIModel>(responseFromServer);
                            //new JavaScriptSerializer().Deserialize<Friends>(result);
                            if (result.routes.Any())
                            {
                                var TravelTime = (result.routes.FirstOrDefault().legs.FirstOrDefault().duration.value) / 60;
                                PrepareTime = PrepareTime + TravelTime;
                            }

                            Console.WriteLine(responseFromServer);
                        }
                    }
                }
                order.CreatedOnUtc = Helper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc, context: dbcontext);
                var OrderFromTime = order.CreatedOnUtc.AddMinutes(PrepareTime).ToString("hh:mm tt");
                var OrderToTime = order.CreatedOnUtc.AddMinutes(PrepareTime + 10).ToString("hh:mm tt");
                paymentCheckout.OrderArrivalTime = OrderFromTime + "-" + OrderToTime;
                var ProductsToCheckout = dbcontext.ShoppingCartItem.Where(x => x.CustomerId == order.CustomerId && x.StoreId == order.StoreId).ToList();
                foreach (var item in ProductsToCheckout)
                {
                    dbcontext.Entry(item).State = EntityState.Deleted;
                    dbcontext.SaveChanges();
                }

                if ((checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.COD ||
                    (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal) ||
                    (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Square && checkoutPaymentModel.IsSuccess)) ||
                    (charge != null && charge.Status == "succeeded"))
                {
                    if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.COD)
                    {

                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Pending;
                        orderToUpdate.AuthorizationTransactionCode = Guid.NewGuid().ToString();
                        orderToUpdate.PaymentMethodSystemName = "Payments.CashOnDelivery";
                    }
                    else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal)
                    {

                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Pending;
                        orderToUpdate.AuthorizationTransactionCode = checkoutPaymentModel.PaymentId;
                        orderToUpdate.PaymentMethodSystemName = "Payments.Paypal";
                    }
                    else if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Square)
                    {
                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Paid;
                        orderToUpdate.AuthorizationTransactionCode = checkoutPaymentModel.PaymentId;
                        orderToUpdate.PaymentMethodSystemName = "Payments.Square";
                    }
                    else
                    {
                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Paid;
                        orderToUpdate.AuthorizationTransactionCode = charge?.Id;
                    }

                    if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal)
                        orderToUpdate.OrderStatusId = (int)OrderHistoryEnum.Cancelled;
                    else
                        orderToUpdate.OrderStatusId = (int)OrderHistoryEnum.Received;

                    dbcontext.Entry(orderToUpdate).State = EntityState.Modified;
                    dbcontext.SaveChanges();

                    //Note and Email send only of other payment methods 
                    if (checkoutPaymentModel.PaymentMethodId != (int)PaymentMethodEnum.PayPal)
                    {
                        OrderNote orderNote = new OrderNote();
                        orderNote.CreatedOnUtc = DateTime.UtcNow;
                        orderNote.DisplayToCustomer = true;
                        orderNote.DownloadId = 0;
                        orderNote.Note = "Order Received & Payment Made Successfully";
                        orderNote.OrderStatus = (int)OrderHistoryEnum.Received;
                        orderNote.OrderId = checkoutPaymentModel.OrderId;
                        dbcontext.OrderNote.Add(orderNote);
                        dbcontext.SaveChanges();

                        OrderReviewModel orderReviewModel = new OrderReviewModel();
                        orderReviewModel.CustomerId = checkoutPaymentModel.CustomerId;
                        orderReviewModel.OrderId = checkoutPaymentModel.OrderId;
                        orderReviewModel.StoreId = orderToUpdate.StoreId;
                        orderReviewModel.UniqueSeoCode = checkoutPaymentModel.UniqueSeoCode;
                        Helper.sendNotificationToCustomer(orderReviewModel, languageId, dbcontext);
                    }

                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.PaymentSuccessfully", languageId, dbcontext);
                    customerAPIResponses.Status = true;
                    customerAPIResponses.StatusCode = 200;
                    customerAPIResponses.ResponseObj = paymentCheckout;
                    return customerAPIResponses;
                }
                else
                {
                    if (checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.PayPal || checkoutPaymentModel.PaymentMethodId == (int)PaymentMethodEnum.Square)
                    {
                        orderToUpdate.PaymentStatusId = (int)PaymentStatus.Failed;
                        orderToUpdate.OrderStatusId = (int)OrderHistoryEnum.Cancelled;
                        orderToUpdate.AuthorizationTransactionCode = checkoutPaymentModel.PaymentId;
                        dbcontext.Entry(orderToUpdate).State = EntityState.Modified;
                        dbcontext.SaveChanges();
                    }
                    OrderNote orderNote = new OrderNote();
                    orderNote.CreatedOnUtc = DateTime.UtcNow;
                    orderNote.DisplayToCustomer = true;
                    orderNote.DownloadId = 0;
                    orderNote.Note = "Order Cancelled due to payment failed.";
                    orderNote.OrderStatus = (int)OrderHistoryEnum.Cancelled;
                    orderNote.OrderId = checkoutPaymentModel.OrderId;
                    dbcontext.OrderNote.Add(orderNote);
                    dbcontext.SaveChanges();
                    customerAPIResponses.ErrorMessage = charge != null ? charge.FailureMessage : LanguageHelper.GetResourseValueByName(checkoutPaymentModel.StoreId, "API.ErrorMesaage.PaymentFailed", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = 400;
                    customerAPIResponses.ResponseObj = paymentCheckout;
                    return customerAPIResponses;
                }
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = 400;
                customerAPIResponses.ResponseObj = null;
                return customerAPIResponses;
            }
        }


        [Route("~/api/PDTHandler")]
        [HttpGet]
        public IActionResult PDTHandler()
        {
            var tx = HttpContext.Request.Query["tx"];
            var customOrderNumber = (string) HttpContext.Request.Query["cm"];

            if (Helper.GetPdtDetails(tx, customOrderNumber, dbcontext, out var values, out var response))
            {
                values.TryGetValue("custom", out var orderNumber);
                var orderNumberGuid = Guid.Empty;
                try
                {
                    orderNumberGuid = new Guid(orderNumber);
                }
                catch
                {
                    // ignored
                }

                var order = dbcontext.Order.Where(x => x.OrderGuid == orderNumberGuid).FirstOrDefault();

                if (order == null)
                    return Content(string.Empty);

                var defaultLangCode = LanguageHelper.GetDefaultLangCode(order.StoreId, order.CustomerId, dbcontext);
                int languageId = LanguageHelper.GetIdByLangCode(order.StoreId, defaultLangCode, dbcontext);

                var mcGross = decimal.Zero;

                try
                {
                    mcGross = decimal.Parse(values["mc_gross"], new CultureInfo("en-US"));
                }
                catch (Exception exc)
                {
                    Helper.SentryLogs(exc);
                }

                values.TryGetValue("payer_status", out var payerStatus);
                values.TryGetValue("payment_status", out var paymentStatus);
                values.TryGetValue("pending_reason", out var pendingReason);
                values.TryGetValue("mc_currency", out var mcCurrency);
                values.TryGetValue("txn_id", out var txnId);
                values.TryGetValue("payment_type", out var paymentType);
                values.TryGetValue("payer_id", out var payerId);
                values.TryGetValue("receiver_id", out var receiverId);
                values.TryGetValue("invoice", out var invoice);
                values.TryGetValue("payment_fee", out var paymentFee);

                var sb = new StringBuilder();
                sb.AppendLine("PayPal PDT:");
                sb.AppendLine("mc_gross: " + mcGross);
                sb.AppendLine("Payer status: " + payerStatus);
                sb.AppendLine("Payment status: " + paymentStatus);
                sb.AppendLine("Pending reason: " + pendingReason);
                sb.AppendLine("mc_currency: " + mcCurrency);
                sb.AppendLine("txn_id: " + txnId);
                sb.AppendLine("payment_type: " + paymentType);
                sb.AppendLine("payer_id: " + payerId);
                sb.AppendLine("receiver_id: " + receiverId);
                sb.AppendLine("invoice: " + invoice);
                sb.AppendLine("payment_fee: " + paymentFee);

                var newPaymentStatus = PayPalHelper.GetPaymentStatus(paymentStatus, string.Empty);
                sb.AppendLine("New payment status: " + newPaymentStatus);

                //order note
                OrderNote orderNote = new OrderNote
                {
                    CreatedOnUtc = DateTime.UtcNow,
                    DisplayToCustomer = false,
                    Note = sb.ToString(),
                    OrderId = order.Id,
                    OrderStatus = (int)OrderHistoryEnum.Received
                };
                dbcontext.OrderNote.Add(orderNote);
                dbcontext.SaveChanges();

                //validate order total
                var orderTotalSentToPayPalString = dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == order.Id && x.Key == PayPalHelper.OrderTotalSentToPayPal).FirstOrDefault().Value;

                decimal? orderTotalSentToPayPal = string.IsNullOrEmpty(orderTotalSentToPayPalString) ? (decimal?)null : Convert.ToDecimal(orderTotalSentToPayPalString);

                if (orderTotalSentToPayPal.HasValue && mcGross != orderTotalSentToPayPal.Value)
                {
                    var errorStr = $"PayPal PDT. Returned order total {mcGross} doesn't equal order total {order.OrderTotal}. Order# {order.Id}.";
                    //log
                    //Helper.SentryLogs(errorStr);

                    order.PaymentStatusId = (int)PaymentStatus.Failed;
                    order.OrderStatusId = (int)OrderHistoryEnum.Cancelled;
                    dbcontext.Entry(order).State = EntityState.Modified;
                    dbcontext.SaveChanges();

                    //order note
                    OrderNote orderNote2 = new OrderNote
                    {
                        CreatedOnUtc = DateTime.UtcNow,
                        DisplayToCustomer = false,
                        Note = errorStr,
                        OrderId = order.Id
                    };
                    dbcontext.OrderNote.Add(orderNote2);
                    dbcontext.SaveChanges();

                    OrderNote orderNote3 = new OrderNote
                    {
                        CreatedOnUtc = DateTime.UtcNow,
                        DisplayToCustomer = true,
                        DownloadId = 0,
                        Note = "Order Cancelled due to payment failed.",
                        OrderStatus = (int)OrderHistoryEnum.Cancelled,
                        OrderId = order.Id
                    };
                    dbcontext.OrderNote.Add(orderNote3);
                    dbcontext.SaveChanges();

                    return Content(string.Empty);
                }

                //clear attribute
                if (orderTotalSentToPayPal.HasValue)
                {
                    var OrderTotalSentToPayPalObj = dbcontext.GenericAttribute.AsEnumerable().Where(x => x.EntityId == order.Id && x.Key == PayPalHelper.OrderTotalSentToPayPal).FirstOrDefault();
                    dbcontext.Entry(OrderTotalSentToPayPalObj).State = EntityState.Deleted;
                    dbcontext.SaveChanges();
                }

                if (newPaymentStatus != PaymentStatus.Paid)
                {
                    order.PaymentStatusId = (int)PaymentStatus.Failed;
                    order.OrderStatusId = (int)OrderHistoryEnum.Cancelled;
                    dbcontext.Entry(order).State = EntityState.Modified;
                    dbcontext.SaveChanges();

                    OrderNote orderNote3 = new OrderNote
                    {
                        CreatedOnUtc = DateTime.UtcNow,
                        DisplayToCustomer = true,
                        DownloadId = 0,
                        Note = "Order Cancelled due to payment failed.",
                        OrderStatus = (int)OrderHistoryEnum.Cancelled,
                        OrderId = order.Id
                    };
                    dbcontext.OrderNote.Add(orderNote3);
                    dbcontext.SaveChanges();

                    return Content(string.Empty);
                }

                if (!Helper.CanMarkOrderAsPaid(order))
                    return Content(string.Empty);

                //mark order as paid
                order.AuthorizationTransactionId = txnId;
                order.PaymentStatusId = (int)PaymentStatus.Paid;
                order.OrderStatusId = (int)OrderHistoryEnum.Received;
                order.PaidDateUtc = DateTime.UtcNow;
                dbcontext.Entry(order).State = EntityState.Modified;
                dbcontext.SaveChanges();

                OrderNote orderNotePaid = new OrderNote
                {
                    CreatedOnUtc = DateTime.UtcNow,
                    DisplayToCustomer = true,
                    DownloadId = 0,
                    Note = "Order Received & Payment Made Successfully",
                    OrderStatus = (int)OrderHistoryEnum.Received,
                    OrderId = order.Id
                };
                dbcontext.OrderNote.Add(orderNotePaid);
                dbcontext.SaveChanges();

                OrderReviewModel orderReviewModel = new OrderReviewModel
                {
                    CustomerId = order.CustomerId,
                    OrderId = order.Id,
                    StoreId = order.StoreId,
                    UniqueSeoCode = defaultLangCode
                };

                // email after order place
                Helper.SendEmailNotification(order, orderReviewModel, languageId, dbcontext);
                Helper.sendNotificationToCustomer(orderReviewModel, languageId, dbcontext);
            }
            else
            {
                if (!values.TryGetValue("custom", out var orderNumber))
                    orderNumber = HttpContext.Request.Query["cm"];

                var orderNumberGuid = Guid.Empty;

                try
                {
                    orderNumberGuid = new Guid(orderNumber);
                }
                catch
                {
                    // ignored
                }

                var order = dbcontext.Order.Where(x => x.OrderGuid == orderNumberGuid).FirstOrDefault();
                if (order == null)
                    return Content(string.Empty);

                order.PaymentStatusId = (int)PaymentStatus.Failed;
                order.OrderStatusId = (int)OrderHistoryEnum.Cancelled;
                dbcontext.Entry(order).State = EntityState.Modified;
                dbcontext.SaveChanges();

                //order note
                OrderNote orderNote2 = new OrderNote
                {
                    CreatedOnUtc = DateTime.UtcNow,
                    DisplayToCustomer = false,
                    Note = "PayPal PDT failed. " + response,
                    OrderId = order.Id
                };
                dbcontext.OrderNote.Add(orderNote2);
                dbcontext.SaveChanges();

                OrderNote orderNote3 = new OrderNote
                {
                    CreatedOnUtc = DateTime.UtcNow,
                    DisplayToCustomer = true,
                    DownloadId = 0,
                    Note = "Order Cancelled due to payment failed.",
                    OrderStatus = (int)OrderHistoryEnum.Cancelled,
                    OrderId = order.Id
                };
                dbcontext.OrderNote.Add(orderNote3);
                dbcontext.SaveChanges();
            }

            //nothing should be rendered to visitor
            return Content(string.Empty);
        }

        [Route("~/api/CancelOrder")]
        [HttpGet]
        public IActionResult CancelOrder()
        {
            //nothing should be rendered to visitor
            return Content(string.Empty);
        }
    }
}
