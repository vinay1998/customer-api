﻿using Microsoft.AspNetCore.Mvc;
using NHKCustomerApplication.Services;
using NHKCustomerApplication.ViewModels;

namespace NHKCustomerApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        #region "fields"

        private readonly IUserService _userService;

        #endregion

        #region Constructor

        public UserController(IUserService userService)
        {
            this._userService = userService;
        }

        #endregion

        /// <summary>
        /// RegisterCustomer is used to register a custome.
        /// </summary>
        /// <param name="customer">customer.</param>
        /// <returns>registered customer details.</returns>
        [Route("~/api/v2/Signup")]
        [HttpPost]
        public CustomerAPIResponses RegisterCustomer([FromBody]CustomerModel customer)
        {
            return _userService.RegisterCustomer(customer);
        }

        /// <summary>
        /// RegistaionverificationCustomer is used to verify otp.
        /// <param name="customer">RegistationverificationCustomer.</param>
        /// <returns>Success message if otp matched.</returns>
        /// </summary>
        [Route("~/api/v2/RegistrationVerification")]
        [HttpPost]
        public CustomerAPIResponses RegistaionverificationCustomer([FromBody]RegistationverificationCustomer RegistationverificationCustomer)
        {
            return _userService.RegistaionVerificationCustomer(RegistationverificationCustomer);
        }

        ///<summary>
        /// Customer Login
        /// <param name="customer"></param>
        /// <returns></returns>
        /// </summary>
        [Route("~/api/v2/Login")]
        [HttpPost]
        public CustomerAPIResponses Login(CustomerLogin customer)
        {
            return _userService.CustomerLogin(customer);
        }

        ///<summary>
        /// Customer Login
        /// <param name="customer"></param>
        /// <returns></returns>
        /// </summary>
        [Route("~/api/v2.1/Login")]
        [HttpPost]
        public CustomerAPIResponses LoginV2_1(CustomerLoginV2_1 customer)
        {
            return _userService.CustomerLoginV2_1(customer);
        }

        [Route("~/api/v2/GetCooknRestProductsByRestnCookId")]
        [HttpPost]
        public CustomerAPIResponses GetCooknRestItemsByRestnCookIdv4([FromBody]CooknRestProductByID customer)
        {
            return _userService.GetCooknRestItemsByRestnCookId(customer);
        }
    }
}