﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.Utilities;
using NHKCustomerApplication.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace NHKCustomerApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VersionController : ControllerBase
    {
        private ecuadordevContext _context;

        public VersionController(ecuadordevContext context)
        {
            _context = context;
        }
        [HttpPost]
        [Route("~/api/Version")]
        public APIVersionV2 PostVersionV1([FromBody] PublicKey pubkey)
        {
            int languageId = LanguageHelper.GetIdByLangCode(pubkey.StoreId, pubkey.UniqueSeoCode, _context);

            APIVersionV2 customerAPIResponses = new APIVersionV2();
            if (pubkey == null && pubkey.PublicKeyAuth == null && pubkey.PublicKeyAuth == "")
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.GetAllStoreLanguages.AuthenticationKey", languageId, _context);
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NotAcceptable;
                customerAPIResponses.Status = false;
                return customerAPIResponses;
            }
            if (string.IsNullOrEmpty(pubkey.AppName))
            {
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.ErrorMessage.AppNameMissing", languageId, _context);
                customerAPIResponses.StatusCode = (int)HttpStatusCode.NotAcceptable;
                customerAPIResponses.Status = false;
                return customerAPIResponses;
            }
            try
            {
                if (pubkey.PublicKeyAuth != "4ED0BC8A-238F-4A90-BCEF-C76BEB9735D8")
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.GetAllStoreLanguages.InvalidAuthentication", languageId, _context);
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.Unauthorized;
                    customerAPIResponses.Status = false;
                    return customerAPIResponses;
                }
                if (!string.IsNullOrEmpty(pubkey.AppName))
                {
                    var appname = pubkey.AppName.ToLower().ToString();
                    var storeNameExist = _context.VersionInfo.AsEnumerable().Where(x => x.AppName.ToLower().ToString().Equals(appname)).Any();
                    if (!storeNameExist)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.ErrorMessage.StoreNotFound", languageId, _context);
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                        customerAPIResponses.Status = false;
                        return customerAPIResponses;
                    }

                }
                int count = _context.VersionInfo.AsEnumerable().Count();
                if (count == 0)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.ErrorMesaage.NoVersionRecordFound", languageId, _context);
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                    customerAPIResponses.Status = false;
                    return customerAPIResponses;
                }

                var entity = _context.VersionInfo.AsEnumerable().Where(x => x.DeviceOstype == pubkey.DeviceOStype && x.AppName.ToLower().ToString() == pubkey.AppName.ToLower().ToString()).FirstOrDefault();
                if (entity != null)
                {
                    var version = _context.Setting.FirstOrDefault(s => s.StoreId == pubkey.StoreId && ((pubkey.DeviceOStype == "A") ? s.Name == "apisettings.customerapi.version.android" : s.Name == "apisettings.customerapi.version.ios"))?.Value ?? "1.0.0";
                    if (Convert.ToInt32(pubkey.APKVersion.Replace(".", "")) < Convert.ToInt32(version.Replace(".", "")))
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.ErrorMesaage.VersionRecordNotFound", languageId, _context);
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                        return customerAPIResponses;
                    }
                    customerAPIResponses.ApiVersion = version;
                    customerAPIResponses.ApiKey = entity.Apikey;
                    customerAPIResponses.TokenDispStatus = entity.TokenDispStatus;
                    customerAPIResponses.IsBaseSplashNeeded = entity.IsBaseSplashNeeded ?? false;
                    customerAPIResponses.IsMultiVendorSupported = entity.IsMultiVendorSupported;
                    customerAPIResponses.SplashURL = entity.SplashUrl;
                    customerAPIResponses.IsTutorialNeeded = entity.IsTutorialNeeded;
                    customerAPIResponses.TutorialVersion = entity.TutorialVersion;
                    customerAPIResponses.IsMultiLanguage = entity.IsMultiLanguage;
                    customerAPIResponses.DataBaseVersion = entity.DataBaseVersion;
                    customerAPIResponses.IsDispGoogMap = entity.IsDispGoogMap ?? false;
                    customerAPIResponses.DispGoogMapURL = entity.DispGoogMapUrl;
                    customerAPIResponses.IsSocketAvailable = entity.IsSocketAvailable ?? false;
                    customerAPIResponses.SocketPort = entity.SocketPort;
                    customerAPIResponses.SocketURL = entity.SocketUrl;
                    customerAPIResponses.StoreId = entity.StoreId.GetValueOrDefault();
                    var store = _context.Store.FirstOrDefault(x => x.Id == customerAPIResponses.StoreId);
                    if (store != null)
                    {
                        customerAPIResponses.IsDelTakeAwayOnDashboard = store.IsTakeaway ?? false;
                        customerAPIResponses.IsDelDeliveryOnDashboard = store.IsDelivery ?? false;
                        customerAPIResponses.RestnCookId = _context.Vendor.FirstOrDefault(x => x.StoreId == store.Id && x.Active && !x.Deleted)?.Id ?? 0;
                    }
                    var storeCurrency = Helper.GetStoreCurrency(customerAPIResponses.StoreId, context: _context);
                    customerAPIResponses.StoreCurrency = !string.IsNullOrEmpty(storeCurrency) ? (storeCurrency + " ") : "";
                    customerAPIResponses.GetHelp = entity.GetHelp;
                    customerAPIResponses.TermsCondition = entity.TermsCondition;
                    customerAPIResponses.Privacy = entity.Privacy;
                    customerAPIResponses.SupportChatUrl = entity.SupportChatUrl;
                    //var gethelpUrl = entity.GetHelp.Split('/');
                    //if (gethelpUrl != null && gethelpUrl.Count() > 1)
                    //{
                    //    int idForGetHelp = 0;
                    //    var count1 = gethelpUrl.Count() - 1;
                    //    var urltoFetch = gethelpUrl[count1];
                    //    var idToFetch = _context.UrlRecord.AsEnumerable().Where(x => x.EntityName == "Topic" && x.Slug.Equals(urltoFetch)).FirstOrDefault();
                    //    if (idToFetch != null)
                    //    {
                    //        idForGetHelp = idToFetch.EntityId;
                    //    }
                    //    if (idForGetHelp != 0)
                    //    {
                    //        var topic = _context.Topic.Where(x => x.Id == idForGetHelp).FirstOrDefault();
                    //        if (topic != null)
                    //        {
                    //            StringBuilder sb = new StringBuilder();
                    //            sb.Append("<br>");
                    //            sb.Append(topic.Body);
                    //            customerAPIResponses.GetHelp = sb.ToString();
                    //        }
                    //    }
                    //    //supportUrl.GetHelp = _contextcontext.Topics.Where(x => x.Id == idForGetHelp).FirstOrDefault().Body;
                    //    //var templateToFetch=  _contextcontext.TopicTemplates.Where(x=>x.)
                    //}
                    //var getTermsConditionUrl = entity.TermsCondition.Split('/');
                    //if (getTermsConditionUrl != null && getTermsConditionUrl.Count() > 1)
                    //{
                    //    int idForTermsCondition = 0;
                    //    var count1 = getTermsConditionUrl.Count() - 1;
                    //    var urltoFetch = getTermsConditionUrl[count1];
                    //    var idToFetch = _context.UrlRecord.AsEnumerable().Where(x => x.EntityName == "Topic" && x.Slug.Equals(urltoFetch)).FirstOrDefault();
                    //    if (idToFetch != null)
                    //    {
                    //        idForTermsCondition = idToFetch.EntityId;
                    //    }
                    //    if (idForTermsCondition != 0)
                    //    {
                    //        var topic = _context.Topic.AsEnumerable().Where(x => x.Id == idForTermsCondition).FirstOrDefault();
                    //        if (topic != null)
                    //        {
                    //            StringBuilder sb = new StringBuilder();
                    //            sb.Append("<br>");
                    //            sb.Append(topic.Body);
                    //            customerAPIResponses.TermsCondition = sb.ToString();
                    //        }
                    //    }
                    //    //supportUrl.GetHelp = _contextcontext.Topics.Where(x => x.Id == idForGetHelp).FirstOrDefault().Body;
                    //    //var templateToFetch=  _contextcontext.TopicTemplates.Where(x=>x.)
                    //}
                    //var getPrivacyUrl = entity.Privacy.Split('/');
                    //if (getPrivacyUrl != null && getPrivacyUrl.Count() > 1)
                    //{
                    //    int idForPrivacy = 0;
                    //    var count1 = getPrivacyUrl.Count() - 1;
                    //    var urltoFetch = getPrivacyUrl[count1];
                    //    var idToFetch = _context.UrlRecord.AsEnumerable().Where(x => x.EntityName == "Topic" && x.Slug.Equals(urltoFetch)).FirstOrDefault();
                    //    if (idToFetch != null)
                    //    {
                    //        idForPrivacy = idToFetch.EntityId;
                    //    }
                    //    if (idForPrivacy != 0)
                    //    {
                    //        var topic = _context.Topic.AsEnumerable().Where(x => x.Id == idForPrivacy).FirstOrDefault();
                    //        if (topic != null)
                    //        {
                    //            StringBuilder sb = new StringBuilder();
                    //            sb.Append("<br>");
                    //            sb.Append(topic.Body);
                    //            customerAPIResponses.Privacy = sb.ToString();
                    //        }
                    //    }
                    //    //supportUrl.GetHelp = _contextcontext.Topics.Where(x => x.Id == idForGetHelp).FirstOrDefault().Body;
                    //    //var templateToFetch=  _contextcontext.TopicTemplates.Where(x=>x.)
                    //}
                    //var getSupportChatUrl = entity.SupportChatUrl.Split('/');
                    //if (getSupportChatUrl != null && getSupportChatUrl.Count() > 1)
                    //{
                    //    int idForSupportChatUrl = 0;
                    //    var count1 = getSupportChatUrl.Count() - 1;
                    //    var urltoFetch = getSupportChatUrl[count1];
                    //    var idToFetch = _context.UrlRecord.AsEnumerable().Where(x => x.EntityName == "Topic" && x.Slug.Equals(urltoFetch)).FirstOrDefault();
                    //    if (idToFetch != null)
                    //    {
                    //        idForSupportChatUrl = idToFetch.EntityId;
                    //    }
                    //    if (idForSupportChatUrl != 0)
                    //    {
                    //        var topic = _context.Topic.AsEnumerable().Where(x => x.Id == idForSupportChatUrl).FirstOrDefault();
                    //        if (topic != null)
                    //        {
                    //            StringBuilder sb = new StringBuilder();
                    //            sb.Append("<br>");
                    //            sb.Append(topic.Body);
                    //            customerAPIResponses.SupportChatUrl = sb.ToString();
                    //        }
                    //    }
                    //    //supportUrl.GetHelp = _contextcontext.Topics.Where(x => x.Id == idForGetHelp).FirstOrDefault().Body;
                    //    //var templateToFetch=  _contextcontext.TopicTemplates.Where(x=>x.)
                    //}
                    //supportUrl.GetHelp = SupportModel.GetHelp;
                    //supportUrl.GetHelp =  
                    //supportUrl.TermsCondition = SupportModel.TermsCondition;
                    //supportUrl.Privacy = SupportModel.Privacy;
                    //  supportUrl.SupportChatUrl = SupportModel.SupportChatUrl;
                    customerAPIResponses.SupportPhone = entity.SupportPhone;
                    customerAPIResponses.SupportEmail = entity.SupportEmail;
                    //if (ConfigurationManager.AppSettings["TokenDisp"].ToString().Equals("false"))
                    //    customerAPIResponses.TokenDispStatus = false;
                    //else
                    //    customerAPIResponses.TokenDispStatus = true;
                    // Fetch Languages
                    var entityLang = _context.VersionLangs.AsEnumerable().Where(x => x.DeviceOstype == pubkey.DeviceOStype && x.Apkversion == pubkey.APKVersion).ToList();
                    customerAPIResponses.LanguageCount = entityLang.Count();
                    List<APIVersionLanguage> verLangs = new List<APIVersionLanguage>();
                    if (entityLang.Any())
                    {
                        foreach (var langs in entityLang)
                        {
                            APIVersionLanguage aPIVersionLanguage = new APIVersionLanguage();
                            aPIVersionLanguage.APIVerLangId = langs.Id;
                            aPIVersionLanguage.APIVerLang = langs.VerLang;
                            verLangs.Add(aPIVersionLanguage);
                        }
                        customerAPIResponses.APIVerLangs = verLangs;
                    }
                    // Fetch Tutorials
                    var tutorialLang = _context.TutorialUrl.AsEnumerable().ToList();
                    customerAPIResponses.TutorialCount = tutorialLang.Count();
                    List<TutorialURLS> tutorialLangVerLangs = new List<TutorialURLS>();
                    if (tutorialLang.Any())
                    {
                        foreach (var langs in tutorialLang)
                        {
                            TutorialURLS tutorialVersionLanguage = new TutorialURLS();
                            tutorialVersionLanguage.TutorialID = langs.Id;
                            tutorialVersionLanguage.TutorialURL = langs.Url;
                            tutorialLangVerLangs.Add(tutorialVersionLanguage);
                        }
                        customerAPIResponses.TutorialURLs = tutorialLangVerLangs;
                    }
                    if (customerAPIResponses.TutorialURLs == null)
                    {
                        customerAPIResponses.TutorialURLs = new List<TutorialURLS>();
                    }
                    customerAPIResponses.ErrorMessageTitle = "Success";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.ErrorMesaage.VersionFoundSuccessfully", languageId, _context);
                    customerAPIResponses.Status = true;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                    return customerAPIResponses;
                }
                else
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(pubkey.StoreId, "API.ErrorMesaage.VersionRecordNotFound", languageId, _context);
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NotFound;
                    return customerAPIResponses;
                }

            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                customerAPIResponses.ErrorMessageTitle = "Error!!";
                customerAPIResponses.ErrorMessage = ex.Message;
                customerAPIResponses.Status = false;
                customerAPIResponses.StatusCode = (int)HttpStatusCode.InternalServerError;
                return customerAPIResponses;

            }




        }

    }
}