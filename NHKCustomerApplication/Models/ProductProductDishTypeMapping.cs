﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class ProductProductDishTypeMapping
    {
        public int ProductId { get; set; }
        public int ProductDishTypeId { get; set; }

        public virtual Product Product { get; set; }
        public virtual ProductDishType ProductDishType { get; set; }
    }
}
