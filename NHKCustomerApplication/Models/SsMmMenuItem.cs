﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsMmMenuItem
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public bool OpenInNewWindow { get; set; }
        public int DisplayOrder { get; set; }
        public string CssClass { get; set; }
        public int MaximumNumberOfEntities { get; set; }
        public int NumberOfBoxesPerRow { get; set; }
        public int CatalogTemplate { get; set; }
        public int ImageSize { get; set; }
        public int EntityId { get; set; }
        public string WidgetZone { get; set; }
        public decimal Width { get; set; }
        public int ParentMenuItemId { get; set; }
        public int? MenuId { get; set; }
        public bool SubjectToAcl { get; set; }

        public virtual SsMmMenu Menu { get; set; }
    }
}
