﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class ShippingByTotal
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public int WarehouseId { get; set; }
        public int CountryId { get; set; }
        public int StateProvinceId { get; set; }
        public string ZipPostalCode { get; set; }
        public int DisplayOrder { get; set; }
        public int ShippingMethodId { get; set; }
        public decimal From { get; set; }
        public decimal To { get; set; }
        public bool UsePercentage { get; set; }
        public decimal ShippingChargePercentage { get; set; }
        public decimal ShippingChargeAmount { get; set; }
    }
}
