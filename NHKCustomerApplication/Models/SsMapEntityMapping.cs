﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsMapEntityMapping
    {
        public int Id { get; set; }
        public int EntityType { get; set; }
        public int EntityId { get; set; }
        public int MappedEntityId { get; set; }
        public int DisplayOrder { get; set; }
        public int MappingType { get; set; }
    }
}
