﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsSpcProductsGroupItem
    {
        public int Id { get; set; }
        public bool Active { get; set; }
        public string Title { get; set; }
        public int SourceType { get; set; }
        public int EntityId { get; set; }
        public int SortMethod { get; set; }
        public int DisplayOrder { get; set; }
        public int GroupId { get; set; }

        public virtual SsSpcProductsGroup Group { get; set; }
    }
}
