﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class AgentOrderGeoLocation
    {
        public int Id { get; set; }
        public int? AgentId { get; set; }
        public int? OderId { get; set; }
        public string AgentLat { get; set; }
        public string AgentLong { get; set; }
        public string DeviceUpDt { get; set; }
        public DateTime? SysUpDt { get; set; }
    }
}
