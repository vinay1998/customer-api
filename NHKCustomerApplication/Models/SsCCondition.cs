﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsCCondition
    {
        public SsCCondition()
        {
            SsCConditionGroup = new HashSet<SsCConditionGroup>();
            SsCCustomerOverride = new HashSet<SsCCustomerOverride>();
            SsCEntityCondition = new HashSet<SsCEntityCondition>();
            SsCProductOverride = new HashSet<SsCProductOverride>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<SsCConditionGroup> SsCConditionGroup { get; set; }
        public virtual ICollection<SsCCustomerOverride> SsCCustomerOverride { get; set; }
        public virtual ICollection<SsCEntityCondition> SsCEntityCondition { get; set; }
        public virtual ICollection<SsCProductOverride> SsCProductOverride { get; set; }
    }
}
