﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class ProductProductCuisineMapping
    {
        public int Id { get; set; }
        public int CuisineId { get; set; }
        public int ProductId { get; set; }

        public virtual ProductCuisine Cuisine { get; set; }
        public virtual Product Product { get; set; }
    }
}
