﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsSSchedule
    {
        public int Id { get; set; }
        public int EntityType { get; set; }
        public int EntityId { get; set; }
        public DateTime? EntityFromDate { get; set; }
        public DateTime? EntityToDate { get; set; }
        public int SchedulePatternType { get; set; }
        public TimeSpan? SchedulePatternFromTime { get; set; }
        public TimeSpan? SchedulePatternToTime { get; set; }
        public int? ExactDayValue { get; set; }
        public int? EveryMonthFromDayValue { get; set; }
        public int? EveryMonthToDayValue { get; set; }
    }
}
