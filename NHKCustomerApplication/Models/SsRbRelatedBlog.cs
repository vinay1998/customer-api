﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsRbRelatedBlog
    {
        public int Id { get; set; }
        public int BlogPostId { get; set; }
        public int RelatedBlogPostId { get; set; }
        public int DisplayOrder { get; set; }
    }
}
