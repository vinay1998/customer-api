﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class VendorGallery
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public int VendorId { get; set; }
        public int GalleryType { get; set; }
        public int? PictureId { get; set; }
        public string GalleryUrl { get; set; }
        public string GalleryCaption { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedDt { get; set; }
        public DateTime? UpdatedDt { get; set; }
        public int? DisplayOrder { get; set; }
    }
}
