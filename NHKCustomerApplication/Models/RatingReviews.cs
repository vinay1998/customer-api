﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class RatingReviews
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int EntityId { get; set; }
        public int OrderId { get; set; }
        public int ReviewType { get; set; }
        public int StoreId { get; set; }
        public bool IsApproved { get; set; }
        public string Title { get; set; }
        public string ReviewText { get; set; }
        public string ReplyText { get; set; }
        public int Rating { get; set; }
        public string ReviewOptions { get; set; }
        public bool IsLiked { get; set; }
        public bool IsNotInterested { get; set; }
        public bool? IsSkipped { get; set; }
        public DateTime CreatedOnUtc { get; set; }
    }
}
