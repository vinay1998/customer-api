﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class StoreNotyek
    {
        public int Id { get; set; }
        public string StoreName { get; set; }
        public string Kw1 { get; set; }
        public string Kw2 { get; set; }
        public string Kw3 { get; set; }
        public string Aw1 { get; set; }
        public string Aw2 { get; set; }
        public string Aw3 { get; set; }
        public string Ka1 { get; set; }
        public string Ka2 { get; set; }
        public string Ka3 { get; set; }
        public DateTime? Dt { get; set; }
    }
}
