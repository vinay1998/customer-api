﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class ProductCuisine
    {
        public ProductCuisine()
        {
            ProductProductCuisineMapping = new HashSet<ProductProductCuisineMapping>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? StoreId { get; set; }

        public virtual ICollection<ProductProductCuisineMapping> ProductProductCuisineMapping { get; set; }
    }
}
