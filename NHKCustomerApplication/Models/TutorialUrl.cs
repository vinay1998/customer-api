﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class TutorialUrl
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int StoreId { get; set; }
    }
}
