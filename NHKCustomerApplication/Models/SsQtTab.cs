﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsQtTab
    {
        public int Id { get; set; }
        public string SystemName { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public bool LimitedToStores { get; set; }
        public int TabMode { get; set; }
        public int DisplayOrder { get; set; }
    }
}
