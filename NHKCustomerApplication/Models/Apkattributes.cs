﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class Apkattributes
    {
        public int Id { get; set; }
        public string Lang { get; set; }
        public string AttributeName { get; set; }
        public string AttributeValue { get; set; }
        public DateTime CreatedDt { get; set; }
        public string CreatedBy { get; set; }
    }
}
