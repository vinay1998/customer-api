﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class ProductScheduleMapping
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int ScheduleId { get; set; }
        public bool IsFeaturedProduct { get; set; }
        public int DisplayOrder { get; set; }
    }
}
