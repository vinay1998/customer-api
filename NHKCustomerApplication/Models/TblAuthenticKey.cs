﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class TblAuthenticKey
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public Guid? AuthenticKey { get; set; }
        public DateTime? KeyDt { get; set; }
    }
}
