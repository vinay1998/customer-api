﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class BookingTableMaster
    {
        public int Id { get; set; }
        public int? StoreId { get; set; }
        public int? VendorId { get; set; }
        public int? TableNo { get; set; }
        public int? Seats { get; set; }
        public bool? Status { get; set; }
        public decimal? Availability { get; set; }
        public DateTime? CreatedAt { get; set; }
        public TimeSpan? TableAvailableFrom { get; set; }
        public TimeSpan? TableAvailableTo { get; set; }
        public int? BookingDetailsId { get; set; }
    }
}
