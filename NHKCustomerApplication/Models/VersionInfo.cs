﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class VersionInfo
    {
        public int Id { get; set; }
        public string Apiversion { get; set; }
        public string Apiurl { get; set; }
        public string Apikey { get; set; }
        public string DeviceOstype { get; set; }
        public string Apkversion { get; set; }
        public bool? IsStoreIdAvlb { get; set; }
        public int? StoreId { get; set; }
        public bool? IsBaseSplashNeeded { get; set; }
        public bool? IsStoreSplashRequired { get; set; }
        public string SplashUrl { get; set; }
        public DateTime CreatedDt { get; set; }
        public string CreatedBy { get; set; }
        public bool? IsDispAccRejOption { get; set; }
        public bool? IsDispGoogMap { get; set; }
        public string DispGoogMapUrl { get; set; }
        public bool? IsSocketAvailable { get; set; }
        public string SocketUrl { get; set; }
        public string SocketPort { get; set; }
        public string SupportUrl { get; set; }
        public string AppName { get; set; }
        public bool TokenDispStatus { get; set; }
        public bool IsMultiVendorSupported { get; set; }
        public bool IsTutorialNeeded { get; set; }
        public string TutorialVersion { get; set; }
        public bool IsMultiLanguage { get; set; }
        public string DataBaseVersion { get; set; }
        public bool IsDelTakeAwayOnDashboard { get; set; }
        public string GetHelpUrl { get; set; }
        public string GetHelp { get; set; }
        public string TermsCondition { get; set; }
        public string Privacy { get; set; }
        public string SupportChatUrl { get; set; }
        public string SupportPhone { get; set; }
        public string SupportEmail { get; set; }
    }
}
