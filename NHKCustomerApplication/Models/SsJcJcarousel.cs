﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsJcJcarousel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string DataSourceType { get; set; }
        public int DataSourceEntityId { get; set; }
        public int CarouselType { get; set; }
        public bool LimitedToStores { get; set; }
    }
}
