﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsCEntityCondition
    {
        public int Id { get; set; }
        public int ConditionId { get; set; }
        public int EntityType { get; set; }
        public int EntityId { get; set; }
        public bool LimitedToStores { get; set; }

        public virtual SsCCondition Condition { get; set; }
    }
}
