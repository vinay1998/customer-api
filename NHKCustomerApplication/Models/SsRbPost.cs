﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsRbPost
    {
        public int Id { get; set; }
        public int HomePagePictureId { get; set; }
        public int PictureId { get; set; }
        public int BlogPostId { get; set; }
    }
}
