﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class AgentAvailableStatus
    {
        public int Id { get; set; }
        public int? AgentId { get; set; }
        public bool? Status { get; set; }
        public string UpDt { get; set; }
        public DateTime? SysDt { get; set; }
    }
}
