﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class TaxCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DisplayOrder { get; set; }
    }
}
