﻿using System;

namespace NHKCustomerApplication.Models
{
    /// <summary>
    /// Represents a MerchantCategory
    /// </summary>
    public partial class MerchantCategory 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public int PictureId { get; set; }
        public int DisplayOrder { get; set; }
        public int StoreId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the entity is published
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets the date and time of product creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of product update
        /// </summary>
        public DateTime? UpdatedOnUtc { get; set; }
    }
}
