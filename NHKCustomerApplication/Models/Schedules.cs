﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class Schedules
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public int VendorId { get; set; }
        public string Name { get; set; }
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool? IsAllDay { get; set; }
        public bool? IsMonday { get; set; }
        public bool? IsTuesday { get; set; }
        public bool? IsWednesday { get; set; }
        public bool? IsThursday { get; set; }
        public bool? IsFriday { get; set; }
        public bool? IsSaturday { get; set; }
        public bool? IsSunday { get; set; }
    }
}
