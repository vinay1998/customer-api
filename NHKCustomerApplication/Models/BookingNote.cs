﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class BookingNote
    {
        public int Id { get; set; }
        public int BookingId { get; set; }
        public string Note { get; set; }
        public int DownloadId { get; set; }
        public bool DisplayToCustomer { get; set; }
        public DateTime CreatedOnUtc { get; set; }

        public virtual BookingTableTransaction Booking { get; set; }
    }
}
