﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsPrCategoryPageRibbon
    {
        public int Id { get; set; }
        public int ProductRibbonId { get; set; }
        public int? PictureId { get; set; }
        public bool Enabled { get; set; }
        public string Text { get; set; }
        public string Position { get; set; }
        public string TextStyle { get; set; }
        public string ImageStyle { get; set; }
        public string ContainerStyle { get; set; }

        public virtual SsPrRibbonPicture Picture { get; set; }
        public virtual SsPrProductRibbon ProductRibbon { get; set; }
    }
}
