﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class OrderSummaryHistory
    {
        public int Id { get; set; }
        public int? AgentId { get; set; }
        public int? OrderId { get; set; }
        public string CustomerName { get; set; }
        public string PickupLocation { get; set; }
        public string PickupLat { get; set; }
        public string PickupLong { get; set; }
        public string PickupMobileNo { get; set; }
        public string DeliveryLocation { get; set; }
        public string DeliveryLat { get; set; }
        public string DeliveryLong { get; set; }
        public string DeliveryMobileNo { get; set; }
        public string ItemDetails { get; set; }
        public DateTime? OrderDt { get; set; }
    }
}
