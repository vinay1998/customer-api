﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsAsAnywhereSlider
    {
        public SsAsAnywhereSlider()
        {
            SsAsSliderImage = new HashSet<SsAsSliderImage>();
        }

        public int Id { get; set; }
        public string SystemName { get; set; }
        public int SliderType { get; set; }
        public int LanguageId { get; set; }
        public bool LimitedToStores { get; set; }

        public virtual ICollection<SsAsSliderImage> SsAsSliderImage { get; set; }
    }
}
