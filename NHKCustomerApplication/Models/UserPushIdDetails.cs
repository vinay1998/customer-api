﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class UserPushIdDetails
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string Token { get; set; }
        public DateTime? CrDt { get; set; }
    }
}
