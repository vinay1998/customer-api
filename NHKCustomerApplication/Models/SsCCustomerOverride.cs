﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsCCustomerOverride
    {
        public int Id { get; set; }
        public int ConditionId { get; set; }
        public int CustomerId { get; set; }
        public int OverrideState { get; set; }

        public virtual SsCCondition Condition { get; set; }
    }
}
