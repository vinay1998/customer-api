﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class Type
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
