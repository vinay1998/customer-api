﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class ProductTagImageMapping
    {
        public int Id { get; set; }
        public int TagId { get; set; }
        public int? PictureId { get; set; }
    }
}
