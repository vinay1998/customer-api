﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.Models
{
    public partial class SsMapEntityWidgetMapping
    {
        public int Id { get; set; }
        public int EntityType { get; set; }
        public int EntityId { get; set; }
        public string WidgetZone { get; set; }
        public int DisplayOrder { get; set; }
    }
}
