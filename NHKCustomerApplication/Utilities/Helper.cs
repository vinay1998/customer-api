﻿//using APICustomerNestorhawk.DBContext;
//using APICustomerNestorhawk.Models;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using NHKCustomerApplication.Models;
using NHKCustomerApplication.ViewModels;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.WebUtilities;
using System.Net.Http;
//using Twilio;
//using Twilio.Rest.Api.V2010.Account;
//using Twilio.Types;

namespace NHKCustomerApplication.Utilities
{

    public static class Helper
    {
        private const string bucketName = "*** bucket name ***";
        // For simplicity the example creates two objects from the same file.
        // You specify key names for these objects.
        private const string keyName1 = "*** key name for first object created ***";
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.APSouth1;
        private static IAmazonS3 client=new AmazonS3Client(bucketRegion);
        //private readonly static string googleMapKey = System.Configuration.ConfigurationManager.AppSettings["googleMapKey"];
        //private readonly static string sentryURL = System.Configuration.ConfigurationManager.AppSettings["sentryURL"];

        public static string CreateSaltKey(int size)
        {
            //generate a cryptographic random number
            using (var provider = new RNGCryptoServiceProvider())
            {
                var buff = new byte[size];
                provider.GetBytes(buff);

                // Return a Base64 string representation of the random number
                return Convert.ToBase64String(buff);
            }
        }
        public static CustomerAPIResponses GetCustomerAPIErrorResponses(string errorMessage)
        {
            return new CustomerAPIResponses
            {
                ErrorMessageTitle = "Error!!",
                ErrorMessage = errorMessage,
                Status = false,
                StatusCode = (int)HttpStatusCode.NoContent,
                ResponseObj = null
            };
        }

        public static async Task<UploadPhotoModel> UploadObject(byte[] fileBytes, string ContentType, string fileName, ecuadordevContext db)
        {
            string accessKey = db.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("s3.accesskey")).FirstOrDefault() != null ?
                    db.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("s3.accesskey")).FirstOrDefault().Value : "AC039a08adfdbdc51566e5ebe18654dccd";
            string accessSecret = db.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("s3.accesssecret")).FirstOrDefault() != null ?
                db.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("s3.accesssecret")).FirstOrDefault().Value : "b07f1451c3b921e08f924fe81a86a0e3d";

            // connecting to the client
            var client = new AmazonS3Client(accessKey, accessSecret, Amazon.RegionEndpoint.APSouth1);

            // get the file and convert it to the byte[]
            //byte[] fileBytes = new Byte[file.Length];
            //file.OpenReadStream().Read(fileBytes, 0, Int32.Parse(file.Length.ToString()));

            // create unique file name for prevent the mess
            // var fileName = Guid.NewGuid() + "ABC";

            PutObjectResponse response = null;

            using (var stream = new MemoryStream(fileBytes))
            {
                var request = new PutObjectRequest
                {
                    BucketName = "nhkprofileimages",
                    Key = fileName,

                    InputStream = stream,
                    ContentType = ContentType,
                    CannedACL = S3CannedACL.BucketOwnerFullControl
                };

                response = await client.PutObjectAsync(request);
            };

            if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                // this model is up to you, in my case I have to use it following;
                return new UploadPhotoModel
                {
                    Success = true,
                    FileName = fileName
                };
            }
            else
            {
                // this model is up to you, in my case I have to use it following;
                return new UploadPhotoModel
                {
                    Success = false,
                    FileName = fileName
                };
            }
        }

        public static IList<string> GetAllAvailableTiming()
        {
            List<string> timeIntervals = new List<string>();
            TimeSpan startTime = new TimeSpan(0, 0, 0);
            DateTime startDate = new DateTime(DateTime.MinValue.Ticks); // Date to be used to get shortTime format.
            for (int i = 0; i < 48; i++)
            {
                int minutesToBeAdded = 30 * i;      // Increasing minutes by 30 minutes interval
                TimeSpan timeToBeAdded = new TimeSpan(0, minutesToBeAdded, 0);
                TimeSpan t = startTime.Add(timeToBeAdded);
                DateTime result = startDate + t;
                timeIntervals.Add(result.ToShortTimeString());      // Use Date.ToShortTimeString() method to get the desired format                
            }
            return timeIntervals;
        }
        /// <summary>
        /// Create a password hash
        /// </summary>
        /// <param name="password">Password</param>
        /// <param name="saltkey">Salk key</param>
        /// <param name="passwordFormat">Password format (hash algorithm)</param>
        /// <returns>Password hash</returns>
        public static string CreatePasswordHash(string password, string saltkey, string passwordFormat)
        {
            return CreateHash(Encoding.UTF8.GetBytes(string.Concat(password, saltkey)), passwordFormat);
        }

        /// <summary>
        /// Create a data hash
        /// </summary>
        /// <param name="data">The data for calculating the hash</param>
        /// <param name="hashAlgorithm">Hash algorithm</param>
        /// <returns>Data hash</returns>
        public static string CreateHash(byte[] data, string hashAlgorithm)
        {
            if (string.IsNullOrEmpty(hashAlgorithm))
                throw new ArgumentNullException(nameof(hashAlgorithm));

            var algorithm = HashAlgorithm.Create(hashAlgorithm);
            if (algorithm == null)
                throw new ArgumentException("Unrecognized hash name");

            var hashByteArray = algorithm.ComputeHash(data);
            return BitConverter.ToString(hashByteArray).Replace("-", "");
        }
        //public static Image LoadImage(string imagetoCOnvert)
        //{
        //    //data:image/gif;base64,
        //    //this image is a single pixel (black)
        //    byte[] bytes = Convert.FromBase64String(imagetoCOnvert);

        //    Image image;
        //    using (MemoryStream ms = new MemoryStream(bytes))
        //    {
        //        image = Image.FromStream(ms);
        //    }

        //    return image;
        //}
        public static async Task<bool> SendMessage(string to, string bodymessage, ecuadordevContext db)
        {
            try
            {

                string accountSid = db.Setting.AsEnumerable().Where(x => x.Name.Equals("twillio.accountsid")).FirstOrDefault() != null ?
                    db.Setting.AsEnumerable().Where(x => x.Name.Equals("twillio.accountsid")).FirstOrDefault().Value : "AC039a08adfdbdc51566e5ebe18654dccd";
                string authToken = db.Setting.AsEnumerable().Where(x => x.Name.Equals("twillio.authtoken")).FirstOrDefault() != null ?
                    db.Setting.AsEnumerable().Where(x => x.Name.Equals("twillio.authtoken")).FirstOrDefault().Value : "b07f1451c3b921e08f924fe81a86a0e3d";
                string senderId = db.Setting.AsEnumerable().Where(x => x.Name.Equals("twillio.senderid")).FirstOrDefault() != null ?
                    db.Setting.AsEnumerable().Where(x => x.Name.Equals("twillio.senderid")).FirstOrDefault().Value : "5123578406";
                TwilioClient.Init(accountSid, authToken);
                var from = new PhoneNumber(senderId);
                var To = new PhoneNumber(to);
                var message = MessageResource.Create(
                to: To,
                from: from,
                body: bodymessage);
                return true;
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                return false;
            }
        }
        public static string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            return text;
        }
        public static string GetCurrencySymbolFromCode(string currencyCode)
        {
            var symbol = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => !c.IsNeutralCulture).Select(culture =>
            {
                try
                {
                    if (culture.LCID != 127)
                    {
                        RegionInfo region = new RegionInfo(culture.LCID);
                        //RegionInfo region = new RegionInfo(culture.LCID);
                        return new RegionInfo(culture.LCID);
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    Helper.SentryLogs(ex);
                    return null;
                }
            }).Where(ri => ri != null && ri.ISOCurrencySymbol == currencyCode).Select(ri => ri.CurrencySymbol).FirstOrDefault();
            return symbol;
        }
        public static string GetStoreCurrency(int StoreId = 0, ecuadordevContext context = null)
        {
            var currencyCode = (from currency in context.Currency
                                 join mapPrimary in context.Setting.Where(x => x.StoreId == StoreId && x.Name.Contains("currencysettings.primarystorecurrencyid"))
                                 on currency.Id.ToString() equals mapPrimary.Value
                                 select new
                                 {
                                     currency.CurrencyCode
                                 }).FirstOrDefault()?.CurrencyCode ?? "USD";
            var symbol = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => !c.IsNeutralCulture).Select(culture =>
            {
                try
                {
                    if (culture.LCID != 127)
                    {
                        RegionInfo region = new RegionInfo(culture.LCID);
                    //RegionInfo region = new RegionInfo(culture.LCID);
                    return new RegionInfo(culture.LCID);
                    }
                    else
                    {
                        return null;
                    }

                }
                catch (Exception ex)
                {
                    Helper.SentryLogs(ex);
                    return null;
                }
            }).Where(ri => ri != null && ri.ISOCurrencySymbol == currencyCode).Select(ri => ri.CurrencySymbol).FirstOrDefault();
            return symbol;


        }
        public static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return origin.AddSeconds(timestamp);
        }

        public static double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }
        public static double distance(double lat1, double lon1, double lat2, double lon2, string unit)
        {
            if ((lat1 == lat2) && (lon1 == lon2))
            {
                return 0;
            }
            else
            {
                double theta = lon1 - lon2;
                double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
                dist = Math.Acos(dist);
                dist = rad2deg(dist);
                dist = dist * 60 * 1.1515;
                if (unit == "K")
                {
                    dist = dist * 1.609344;
                }
                else if (unit == "N")
                {
                    dist = dist * 0.8684;
                }
                return (dist);
            }
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts decimal degrees to radians             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }
        public static DateTime ToTimeZoneTime(this DateTime time, string timeZoneId = "Pacific Standard Time")
        {
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            return time.ToTimeZoneTime(tzi);
        }

        /// <summary>
        /// Returns TimeZone adjusted time for a given from a Utc or local time.
        /// Date is first converted to UTC then adjusted.
        /// </summary>
        /// <param name="time"></param>
        /// <param name="timeZoneId"></param>
        /// <returns></returns>
        public static DateTime ToTimeZoneTime(this DateTime time, TimeZoneInfo tzi)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(time, tzi);
        }
        public static DateTime ConvertToUserTime_Old(DateTime dt, DateTimeKind sourceDateTimeKind, ecuadordevContext context)
        {

            var timeZoneId = string.Empty;
                timeZoneId = context.Setting.Where(x => x.Name.Contains("datetimesettings.defaultstoretimezoneid")).FirstOrDefault().Value;

            //System.Collections.ObjectModel.ReadOnlyCollection<TimeZoneInfo> tz;
            //tz = TimeZoneInfo.GetSystemTimeZones();
            //timeZoneId = tz[87].Id;
            var CurrentTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            var timeZone = Helper.ToTimeZoneTime(dt, timeZoneId);
            return timeZone;
        }

        public static DateTime ConvertToUserTime(DateTime dt, DateTimeKind sourceDateTimeKind, ecuadordevContext context)
        {
            dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
           
            var timeZoneId = context.Setting.Where(x => x.Name.Contains("datetimesettings.defaultstoretimezoneid")).FirstOrDefault().Value;
            var timezones = TimeZoneInfo.GetSystemTimeZones();
            var currentTimeZone = timezones.FirstOrDefault(x=>x.Id==timeZoneId);
            if (currentTimeZone == null)
                currentTimeZone = timezones.FirstOrDefault(x => x.DisplayName.Contains("Kolkata"));
            var currentUserTimeZoneInfo = currentTimeZone;
            return TimeZoneInfo.ConvertTime(dt, currentUserTimeZoneInfo);
        }

        public static DateTime ConvertToUserTimeV2_1(DateTime dt, DateTimeKind sourceDateTimeKind, ecuadordevContext context, int userId, string userRole = "")
        {
            // string rolename = userRole;
            var cusomerInfo = context.Customer.Where(x => x.Id == userId).FirstOrDefault();
            if (cusomerInfo != null)
            {
                var timeZoneId = string.Empty;
                if (string.IsNullOrEmpty(userRole))
                {
                    // get user role
                    var role = (from rm in context.CustomerCustomerRoleMapping
                                join r in context.CustomerRole on rm.CustomerRoleId equals r.Id
                                where rm.CustomerId == userId
                                select r
                                );
                    if (role.Any(x => x.Name == "Administrators"))
                    {
                        //rolename = "admin";
                    }
                    else if (role.Any(x => x.Name == "Store Owner"))
                    {
                        timeZoneId = context.Setting.Where(x => x.Name == "datetimesettings.storetimezoneid" && x.StoreId == cusomerInfo.RegisteredInStoreId).FirstOrDefault().Value;
                        // rolename = "storeadmin";
                    }
                    else if (role.Any(x => x.Name == "Vendors"))
                    {
                        timeZoneId = context.GenericAttribute.Where(x => x.Key == "TimeZoneId" && x.KeyGroup == "Vendor" && x.EntityId == cusomerInfo.VendorId).FirstOrDefault().Value;
                        if (string.IsNullOrEmpty(timeZoneId))
                        {
                            timeZoneId = context.Setting.Where(x => x.Name == "datetimesettings.storetimezoneid" && x.StoreId == cusomerInfo.RegisteredInStoreId).FirstOrDefault().Value;
                        }
                        // rolename = "vendor";
                    }
                }
                if (string.IsNullOrEmpty(timeZoneId))
                {
                    timeZoneId = context.GenericAttribute.Where(x => x.Key == "TimeZoneId" && x.KeyGroup == "Customer" && x.EntityId == cusomerInfo.Id).FirstOrDefault().Value;
                }
                if (string.IsNullOrEmpty(timeZoneId))
                {
                    timeZoneId = context.Setting.Where(x => x.Name == "datetimesettings.storetimezoneid" && x.StoreId == cusomerInfo.RegisteredInStoreId).FirstOrDefault().Value;
                }
                if (string.IsNullOrEmpty(timeZoneId))
                {
                    return Helper.ToTimeZoneTime(dt, TimeZoneInfo.Local);
                }
                try
                {
                    // timeZoneId = context.Setting.Where(x => x.Name.Contains("datetimesettings.defaultstoretimezoneid")).FirstOrDefault().Value;

                    //System.Collections.ObjectModel.ReadOnlyCollection<TimeZoneInfo> tz;
                    //tz = TimeZoneInfo.GetSystemTimeZones();
                    //timeZoneId = tz[87].Id;
                    var CurrentTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                    var timeZone = Helper.ToTimeZoneTime(dt, timeZoneId);
                    return timeZone;
                }
                catch
                {
                    return Helper.ToTimeZoneTime(dt, TimeZoneInfo.Local);
                }
            }
            else
            {
                return Helper.ToTimeZoneTime(dt, TimeZoneInfo.Local);
            }

        }

        public static DateTime ConvertToUserTimeV2(DateTime dt, DateTimeKind sourceDateTimeKind, string timeZoneId)
        {
            //System.Collections.ObjectModel.ReadOnlyCollection<TimeZoneInfo> tz;
            //tz = TimeZoneInfo.GetSystemTimeZones();
            //timeZoneId = tz[87].Id;
            var CurrentTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            var timeZone = Helper.ToTimeZoneTime(dt, timeZoneId);
            return timeZone;
        }
        public static string GetStoreCurrencyCode(int storeId, ecuadordevContext db)
        {
            int _storeId = Convert.ToInt32(storeId);
            var symbol = (from setting in db.Setting.AsEnumerable()
                          join currency in db.Currency.AsEnumerable()
                          on setting.Value equals currency.Id.ToString()
                          where setting.Name.Contains("currencysettings.primaryexchangeratecurrencyid")
                          && setting.StoreId == _storeId
                          select new
                          {
                              currency.CurrencyCode
                          }).FirstOrDefault() != null ? (from setting in db.Setting.AsEnumerable()
                                                         join currency in db.Currency.AsEnumerable()
                                                         on setting.Value equals currency.Id.ToString()
                                                         where setting.Name.Contains("currencysettings.primaryexchangeratecurrencyid")
                                                         && setting.StoreId == _storeId
                                                         select new
                                                         {
                                                             currency.CurrencyCode
                                                         }).FirstOrDefault().CurrencyCode : "USD";
            return symbol;

        }

        public static string GetCurrentCurrencyCode(int storeId, ecuadordevContext db)
        {
            int _storeId = Convert.ToInt32(storeId);
            var symbol = (from setting in db.Setting.AsEnumerable()
                          join currency in db.Currency.AsEnumerable()
                          on setting.Value equals currency.Id.ToString()
                          where setting.Name.Contains("currencysettings.primarystorecurrencyid")
                          && setting.StoreId == _storeId
                          select new
                          {
                              currency.CurrencyCode
                          }).FirstOrDefault()?.CurrencyCode ?? "USD";
            return symbol;

        }
        public static string ConvertdecimaltoUptotwoPlaces(decimal number)
        {
            return string.Format("{0:N2}", number);
        }
        public static string GetFileExtensionFromMimeType(string mimeType)
        {
            if (mimeType == null)
                return null;

            //TODO use FileExtensionContentTypeProvider to get file extension

            var parts = mimeType.Split('/');
            var lastPart = parts[parts.Length - 1];
            switch (lastPart)
            {
                case "pjpeg":
                    lastPart = "jpg";
                    break;
                case "x-png":
                    lastPart = "png";
                    break;
                case "x-icon":
                    lastPart = "ico";
                    break;
            }
            return lastPart;
        }
        public static void SendEmail(EmailAccount emailAccount, string subject, string body,
           string fromAddress, string fromName, string toAddress, string toName,
            string replyTo = null, string replyToName = null,
           IEnumerable<string> bcc = null, IEnumerable<string> cc = null,
           string attachmentFilePath = null, string attachmentFileName = null,
           int attachedDownloadId = 0, IDictionary<string, string> headers = null)
        {
            var message = new MailMessage
            {
                //from, to, reply to
                From = new MailAddress(fromAddress, fromName)
            };
            message.To.Add(new MailAddress(toAddress, toName));
            if (!string.IsNullOrEmpty(replyTo))
            {
                message.ReplyToList.Add(new MailAddress(replyTo, replyToName));
            }

            //BCC
            if (bcc != null)
            {
                foreach (var address in bcc.Where(bccValue => !string.IsNullOrWhiteSpace(bccValue)))
                {
                    message.Bcc.Add(address.Trim());
                }
            }

            //CC
            if (cc != null)
            {
                foreach (var address in cc.Where(ccValue => !string.IsNullOrWhiteSpace(ccValue)))
                {
                    message.CC.Add(address.Trim());
                }
            }

            //content
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;

            //headers
            if (headers != null)
                foreach (var header in headers)
                {
                    message.Headers.Add(header.Key, header.Value);
                }

            //create the file attachment for this e-mail message
            if (!string.IsNullOrEmpty(attachmentFilePath) &&
                File.Exists(attachmentFilePath))
            {
                var attachment = new Attachment(attachmentFilePath);
                attachment.ContentDisposition.CreationDate = File.GetCreationTime(attachmentFilePath);
                attachment.ContentDisposition.ModificationDate = File.GetLastWriteTime(attachmentFilePath);
                attachment.ContentDisposition.ReadDate = File.GetLastAccessTime(attachmentFilePath);
                if (!string.IsNullOrEmpty(attachmentFileName))
                {
                    attachment.Name = attachmentFileName;
                }
                message.Attachments.Add(attachment);
            }


            //send email
            using (var smtpClient = new SmtpClient())
            {
                smtpClient.UseDefaultCredentials = emailAccount.UseDefaultCredentials;
                smtpClient.Host = emailAccount.Host;
                smtpClient.Port = emailAccount.Port;
                smtpClient.EnableSsl = emailAccount.EnableSsl;
                smtpClient.Credentials = emailAccount.UseDefaultCredentials ?
                    CredentialCache.DefaultNetworkCredentials :
                    new NetworkCredential(emailAccount.Username, emailAccount.Password);
                smtpClient.Send(message);
            }
        }
        //public static string BlogTimings(DateTime datetime)
        //{
        //    const int SECOND = 1;
        //    const int MINUTE = 60 * SECOND;
        //    const int HOUR = 60 * MINUTE;
        //    const int DAY = 24 * HOUR;
        //    const int MONTH = 30 * DAY;

        //    var ts = new TimeSpan(DateTime.Now.Ticks - datetime.Ticks);
        //    double delta = Math.Abs(ts.TotalSeconds);

        //    if (delta < 1 * MINUTE)
        //        return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";

        //    if (delta < 2 * MINUTE)
        //        return "a minute ago";

        //    if (delta < 45 * MINUTE)
        //        return ts.Minutes + " minutes ago";

        //    if (delta < 90 * MINUTE)
        //        return "an hour ago";

        //    if (delta < 24 * HOUR)
        //        return ts.Hours + " hours ago";

        //    if (delta < 48 * HOUR)
        //        return "yesterday";

        //    if (delta < 30 * DAY)
        //        return ts.Days + " days ago";

        //    if (delta < 12 * MONTH)
        //    {
        //        int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
        //        return months <= 1 ? "one month ago" : months + " months ago";
        //    }
        //    else
        //    {
        //        int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
        //        return years <= 1 ? "one year ago" : years + " years ago";
        //    }

        //}
        public static async Task<bool> sendMyFileToS3Async(string localFilePath, string keyName2, string filePath, string contentType)
        {
            // input explained :
            // localFilePath = the full local file path e.g. "c:\mydir\mysubdir\myfilename.zip"
            // bucketName : the name of the bucket in S3 ,the bucket should be alreadt created
            // subDirectoryInBucket : if this string is not empty the file will be uploaded to
            // a subdirectory with this name
            // fileNameInS3 = the file name in the S3

            // create an instance of IAmazonS3 class ,in my case i choose RegionEndpoint.EUWest1
            // you can change that to APNortheast1 , APSoutheast1 , APSoutheast2 , CNNorth1
            // SAEast1 , USEast1 , USGovCloudWest1 , USWest1 , USWest2 . this choice will not
            // store your file in a different cloud storage but (i think) it differ in performance
            // depending on your location
            var putRequest1 = new PutObjectRequest
            {
                BucketName = bucketName,
                Key = keyName1,
                ContentBody = "sample text"
            };

            PutObjectResponse response1 = await Helper.client.PutObjectAsync(putRequest1);

            // 2. Put the object-set ContentType and add metadata.
            var putRequest2 = new PutObjectRequest
            {
                BucketName = bucketName,
                Key = keyName2,
                FilePath = filePath,
                ContentType = contentType
            };
            putRequest2.Metadata.Add("x-amz-meta-title", "someTitle");
            PutObjectResponse response2 = await Helper.client.PutObjectAsync(putRequest2);
            return true;
        }
        public static string sendNotificationToCustomer(OrderReviewModel orderReviewModel,int languageId, ecuadordevContext dbcontext)
        {
            try
            {
                    FCSNotificationModel fCSNotificationModel = new FCSNotificationModel();
                    fCSNotificationModel.ApiKey = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.customerapinotificationkey") && x.StoreId == orderReviewModel.StoreId).Any() ?
                    dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.customerapinotificationkey") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault()?.Value : "";
                    fCSNotificationModel.OrderId = orderReviewModel.OrderId.ToString();
                    fCSNotificationModel.CustomerId = orderReviewModel.CustomerId.ToString();
                    fCSNotificationModel.StoreId = orderReviewModel.StoreId.ToString();
                    fCSNotificationModel.UniqueSeoCode = orderReviewModel.UniqueSeoCode;
                    var FSCToken = dbcontext.CustomerDetails.Where(x => x.CustomerId == orderReviewModel.CustomerId && !string.IsNullOrEmpty(x.DeviceToken)).OrderByDescending(x => x.Id).FirstOrDefault()?.DeviceToken;
                    fCSNotificationModel.To = FSCToken;
                    fCSNotificationModel.Heading = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "nb.customer.notification.orderongoing", languageId, dbcontext);
                fCSNotificationModel.Status = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.TrackOrder.OdrReceivedTxt", languageId, dbcontext);
                var order = dbcontext.Order.Where(x => x.Id == orderReviewModel.OrderId).FirstOrDefault();
                    var ProductsIds = dbcontext.OrderItem.Where(x => x.OrderId == orderReviewModel.OrderId).ToList().Select(x => x.ProductId).ToList();
                    int PrepareTime = 0;
                    int ProductPrepareTime = 0;
                    string VendorLatLong = string.Empty;

                foreach (var productId in ProductsIds)
                {
                    var productPrepareTime = (from p in dbcontext.Product
                                              where p.Id == productId
                                              select new
                                              {
                                                  p.PrepareTime
                                              }).FirstOrDefault() != null ? (from p in dbcontext.Product
                                                                             where p.Id == productId
                                                                             select new
                                                                             {
                                                                                 p.PrepareTime
                                                                             }).FirstOrDefault().PrepareTime : 0;
                    if (productPrepareTime == null)
                    {
                        productPrepareTime = 0;
                    }
                    ProductPrepareTime = productPrepareTime.Value + ProductPrepareTime;
                    //var vendorId = dbcontext.Products.Where(x => x.Id == productId).Select(x => x.VendorId).FirstOrDefault();
                    //VendorLatLong = dbcontext.Vendors.Where(x => x.Id == vendorId).FirstOrDefault().Geolocation;
                }
                PrepareTime = ProductPrepareTime;
                //int PId = ProductsIds.FirstOrDefault();
                //var VendorId = dbcontext.Products.Where(x => x.Id == PId).FirstOrDefault().VendorId;
                //var vendors = dbcontext.Vendors.Where(x => x.Id == VendorId).FirstOrDefault();

                //if (!string.IsNullOrEmpty(VendorLatLong) && VendorLatLong.Contains("MARKER"))
                //{
                //    string[] venLtLngStrArr = VendorLatLong.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
                //    if (venLtLngStrArr.Length > 0)
                //    {
                //        var AddressId = order.PickUpInStore ? order.BillingAddressId : order.ShippingAddressId;
                //        var customerAddress = dbcontext.Addresses.Where(x => x.Id == AddressId).FirstOrDefault();
                //        double latRest = Convert.ToDouble(venLtLngStrArr[0]);
                //        double longRest = Convert.ToDouble(venLtLngStrArr[1]);
                //        double latCust = Convert.ToDouble(customerAddress.Latitude);
                //        double longCust = Convert.ToDouble(customerAddress.Longitude);
                //        string URl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + latRest + "," + longRest + "&destination=" + latCust + "," + longCust + "&mode=transit&" + googleMapKey;
                //        var request = (HttpWebRequest)WebRequest.Create(URl);
                //        WebResponse response = request.GetResponse();
                //        using (Stream dataStream = response.GetResponseStream())
                //        {
                //            // Open the stream using a StreamReader for easy access.  
                //            StreamReader reader = new StreamReader(dataStream);
                //            // Read the content.  
                //            string responseFromServer = reader.ReadToEnd();
                //            // Display the content.
                //            var result = JsonConvert.DeserializeObject<GoogleTimeAPIModel>(responseFromServer);
                //            //new JavaScriptSerializer().Deserialize<Friends>(result);
                //            if (result.routes.Any())
                //            {
                //                var TravelTime = (result.routes.FirstOrDefault().legs.FirstOrDefault().duration.value) / 60;
                //                PrepareTime = PrepareTime + TravelTime;
                //            }

                //            Console.WriteLine(responseFromServer);
                //        }

                //        //if (ret <= DistanceCovered)
                //        //{
                //        //    CooknRestlist.Add(item.Id);
                //        //}
                //    }
                //}

                var ProductId = ProductsIds.FirstOrDefault();
                var VendorName = (from a in dbcontext.Product.Where(x => x.Id == ProductId)
                                  join b in dbcontext.Vendor
                                  on a.VendorId equals b.Id
                                  select new
                                  {
                                      b
                                  }).FirstOrDefault().b.Name;

                order.CreatedOnUtc = Helper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc, dbcontext);
                var OrderFromTime = order.CreatedOnUtc.AddMinutes(PrepareTime).ToString("hh:mm tt");
                var OrderToTime = order.CreatedOnUtc.AddMinutes(PrepareTime + 10).ToString("hh:mm tt");
                fCSNotificationModel.DeliveredTime = "ETA " + PrepareTime + " hrs";

                //String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                String strUrl = dbcontext.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("setting.customer.notificationurl") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value;
                var sendNotifcationURl = strUrl + "/api/v1/SendFCSNotifications";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(sendNotifcationURl);

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                string json = JsonConvert.SerializeObject(fCSNotificationModel);
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = JsonConvert.DeserializeObject<FCSResultModel>(streamReader.ReadToEnd());

                    return result.Result.ToString();
                }
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                throw;
            }
        }

        public static string sendNotificationToCustomerV2_1(OrderReviewModel orderReviewModel,int languageId, ecuadordevContext dbcontext, int userId)
        {
            try
            {
                FCSNotificationModel fCSNotificationModel = new FCSNotificationModel();
                fCSNotificationModel.ApiKey = dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.customerapinotificationkey") && x.StoreId == orderReviewModel.StoreId).Any() ?
                dbcontext.Setting.Where(x => x.Name.ToLower().Equals("setting.customer.customerapinotificationkey") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault()?.Value : "";
                fCSNotificationModel.OrderId = orderReviewModel.OrderId.ToString();
                fCSNotificationModel.CustomerId = orderReviewModel.CustomerId.ToString();
                fCSNotificationModel.StoreId = orderReviewModel.StoreId.ToString();
                fCSNotificationModel.UniqueSeoCode = orderReviewModel.UniqueSeoCode;
                var FSCToken = dbcontext.CustomerDetails.Where(x => x.CustomerId == orderReviewModel.CustomerId && !string.IsNullOrEmpty(x.DeviceToken)).OrderByDescending(x => x.Id).FirstOrDefault()?.DeviceToken;
                fCSNotificationModel.To = FSCToken;
                fCSNotificationModel.Heading = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "nb.customer.notification.orderongoing", languageId, dbcontext);
                fCSNotificationModel.Status = LanguageHelper.GetResourseValueByName(orderReviewModel.StoreId, "App.TrackOrder.OdrReceivedTxt", languageId, dbcontext);
                var order = dbcontext.Order.Where(x => x.Id == orderReviewModel.OrderId).FirstOrDefault();
                var ProductsIds = dbcontext.OrderItem.Where(x => x.OrderId == orderReviewModel.OrderId).ToList().Select(x => x.ProductId).ToList();
                int PrepareTime = 0;
                int ProductPrepareTime = 0;
                string VendorLatLong = string.Empty;

                foreach (var productId in ProductsIds)
                {
                    var productPrepareTime = (from p in dbcontext.Product
                                              where p.Id == productId
                                              select new
                                              {
                                                  p.PrepareTime
                                              }).FirstOrDefault() != null ? (from p in dbcontext.Product
                                                                             where p.Id == productId
                                                                             select new
                                                                             {
                                                                                 p.PrepareTime
                                                                             }).FirstOrDefault().PrepareTime : 0;
                    if (productPrepareTime == null)
                    {
                        productPrepareTime = 0;
                    }
                    ProductPrepareTime = productPrepareTime.Value + ProductPrepareTime;
                }
                PrepareTime = ProductPrepareTime;
                var ProductId = ProductsIds.FirstOrDefault();
                var VendorName = (from a in dbcontext.Product.Where(x => x.Id == ProductId)
                                  join b in dbcontext.Vendor
                                  on a.VendorId equals b.Id
                                  select new
                                  {
                                      b
                                  }).FirstOrDefault().b.Name;

                order.CreatedOnUtc = Helper.ConvertToUserTimeV2_1(order.CreatedOnUtc, DateTimeKind.Utc, dbcontext, userId);
                var OrderFromTime = order.CreatedOnUtc.AddMinutes(PrepareTime).ToString("hh:mm tt");
                var OrderToTime = order.CreatedOnUtc.AddMinutes(PrepareTime + 10).ToString("hh:mm tt");
                fCSNotificationModel.DeliveredTime = "ETA " + PrepareTime + " hrs";

                String strUrl = dbcontext.Setting.AsEnumerable().Where(x => x.Name.ToLower().Equals("setting.customer.notificationurl") && x.StoreId == orderReviewModel.StoreId).FirstOrDefault().Value;
                var sendNotifcationURl = strUrl + "/api/v2.1/SendFCSNotifications";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(sendNotifcationURl);

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                string json = JsonConvert.SerializeObject(fCSNotificationModel);
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = JsonConvert.DeserializeObject<FCSResultModel>(streamReader.ReadToEnd());

                    return result.Result.ToString();
                }
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                throw;
            }
        }

        public static decimal GetProductAttributeValuePriceAdjustment(ProductAttributeValue value, ecuadordevContext context)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var adjustment = decimal.Zero;
            switch (value.AttributeValueTypeId)
            {
                case (int)AttributeValueType.Simple:
                    {
                        //simple attribute
                        adjustment = value.PriceAdjustment;
                    }
                    break;
                case (int)AttributeValueType.AssociatedToProduct:
                    {
                        var associatedProduct = context.Product.AsEnumerable().Where(x => x.Id == value.AssociatedProductId).FirstOrDefault();
                        if (associatedProduct != null)
                        {
                            adjustment = associatedProduct.Price * value.Quantity;
                        }
                        //bundled product

                    }
                    break;
                default:
                    break;
            }

            return adjustment;
        }
        public static IList<int> ParseProductAttributeMappingIds(string attributesXml)
        {
            var ids = new List<int>();
            if (string.IsNullOrEmpty(attributesXml))
                return ids;

            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(attributesXml);

                var nodeList1 = xmlDoc.SelectNodes(@"//Attributes/ProductAttribute");
                foreach (XmlNode node1 in nodeList1)
                {
                    if (node1.Attributes != null && node1.Attributes["ID"] != null)
                    {
                        var str1 = node1.Attributes["ID"].InnerText.Trim();
                        if (int.TryParse(str1, out int id))
                        {
                            ids.Add(id);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                Helper.SentryLogs(exc);
                Debug.Write(exc.ToString());
            }
            return ids;
        }
        public static IList<ProductProductAttributeMapping> ParseProductAttributeMappings(string attributesXml, ecuadordevContext context)
        {
            var result = new List<ProductProductAttributeMapping>();
            if (string.IsNullOrEmpty(attributesXml))
                return result;

            var ids = ParseProductAttributeMappingIds(attributesXml);
            foreach (var id in ids)
            {
                var attribute = context.ProductProductAttributeMapping.AsEnumerable().Where(x => x.Id == id).FirstOrDefault();
                if (attribute != null)
                {
                    result.Add(attribute);
                }
            }
            return result;

        }
        public static string FormatAttributes(/*Product product1,*/ string attributesXml,
           string separator = "<br />", bool htmlEncode = true, bool renderPrices = true,
            bool renderProductAttributes = true, bool renderGiftCardAttributes = true,
            bool allowHyperlinks = true, ecuadordevContext context = null)
        {
            var result = new StringBuilder();

            //attributes
            if (renderProductAttributes)
            {
                foreach (var attribute in ParseProductAttributeMappings(attributesXml, context))
                {
                    foreach (var attributeValue in ParseProductAttributeValues(attributesXml, attribute.Id, context))
                    {
                        var formattedAttribute = string.Empty;
                        formattedAttribute = $"{attributeValue.Name}";

                        if (renderPrices)
                        {
                            var attributeValuePriceAdjustment = GetProductAttributeValuePriceAdjustment(attributeValue, context);
                            //var priceAdjustmentBase = _taxService.GetProductPrice(product, attributeValuePriceAdjustment, customer, out decimal _);
                            //var priceAdjustment = _currencyService.ConvertFromPrimaryStoreCurrency(priceAdjustmentBase, _workContext.WorkingCurrency);
                            //if (priceAdjustmentBase > 0)
                            //  formattedAttribute += $" [+{_priceFormatter.FormatPrice(priceAdjustment, false, false)}]";
                            //else if (priceAdjustmentBase < decimal.Zero)
                            //  formattedAttribute += $" [-{_priceFormatter.FormatPrice(-priceAdjustment, false, false)}]";
                        }



                        //encode (if required)
                        if (htmlEncode)
                            formattedAttribute = WebUtility.HtmlEncode(formattedAttribute);

                        if (!string.IsNullOrEmpty(formattedAttribute))
                        {
                            if (result.Length > 0)
                                result.Append(separator);
                            result.Append(formattedAttribute);
                        }
                    }

                }
            }
            var res = result.ToString();
            res = HttpUtility.HtmlDecode(Regex.Replace(res, "<.*?>", String.Empty)); ;

            return res;
        }
        public static IList<ProductAttributeValue> ParseProductAttributeValues(string attributesXml, int productAttributeMappingId = 0, ecuadordevContext context = null)
        {
            var values = new List<ProductAttributeValue>();
            if (string.IsNullOrEmpty(attributesXml))
                return values;

            var attributes = ParseProductAttributeMappings(attributesXml, context);

            //to load values only for the passed product attribute mapping
            if (productAttributeMappingId > 0)
                attributes = attributes.Where(attribute => attribute.Id == productAttributeMappingId).ToList();

            foreach (var attribute in attributes)
            {

                foreach (var attributeValue in ParseValuesWithQuantity(attributesXml, attribute.Id))
                {
                    if (!string.IsNullOrEmpty(attributeValue.Item1) && int.TryParse(attributeValue.Item1, out int attributeValueId))
                    {
                        var value = context.ProductAttributeValue.AsEnumerable().Where(x => x.Id == attributeValueId).FirstOrDefault();
                        if (value != null)
                        {
                            if (!string.IsNullOrEmpty(attributeValue.Item2) && int.TryParse(attributeValue.Item2, out int quantity) && quantity != value.Quantity)
                            {
                                //if customer enters quantity, use new entity with new quantity
                                ProductAttributeValue oldValue = new ProductAttributeValue();
                                oldValue.ProductAttributeMapping = attribute;
                                oldValue.Quantity = quantity;
                                values.Add(oldValue);
                            }
                            else
                                values.Add(value);
                        }
                    }
                }
            }
            return values;

        }
        public static IList<Tuple<string, string>> ParseValuesWithQuantity(string attributesXml, int productAttributeMappingId)
        {
            var selectedValues = new List<Tuple<string, string>>();
            if (string.IsNullOrEmpty(attributesXml))
                return selectedValues;

            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(attributesXml);

                foreach (XmlNode attributeNode in xmlDoc.SelectNodes(@"//Attributes/ProductAttribute"))
                {
                    if (attributeNode.Attributes != null && attributeNode.Attributes["ID"] != null)
                    {
                        if (int.TryParse(attributeNode.Attributes["ID"].InnerText.Trim(), out int attributeId) && attributeId == productAttributeMappingId)
                        {
                            foreach (XmlNode attributeValue in attributeNode.SelectNodes("ProductAttributeValue"))
                            {
                                var value = attributeValue.SelectSingleNode("Value").InnerText.Trim();
                                var quantityNode = attributeValue.SelectSingleNode("Quantity");
                                selectedValues.Add(new Tuple<string, string>(value, quantityNode != null ? quantityNode.InnerText.Trim() : string.Empty));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
            }

            return selectedValues;
        }
        public static CustomerAPIResponses ApplyCoupon(int CustomerId, string CouponCode, decimal subTotal, ecuadordevContext dbcontext, int storeId, int languageId)
        {
            CustomerAPIResponses customerAPIResponses = new CustomerAPIResponses();
           
            try
            {
                var isCustomerExist = dbcontext.Customer.Where(x => x.Id == CustomerId).Any();
                if (!isCustomerExist)
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.CustomerNotFound", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
                var isCouponCodeList = dbcontext.Discount.AsEnumerable().Where(x => x.DiscountTypeId == (int)DiscountType.AssignedToOrderTotal).ToList();
                CouponCode = CouponCode.ToUpper().ToString();
                var isCouponCodeExist = isCouponCodeList.Where(x => x.CouponCode != null && x.CouponCode.ToUpper().ToString().Equals(CouponCode)).FirstOrDefault();
                if (isCouponCodeExist != null)
                {
                    if (isCouponCodeExist.StartDateUtc <= DateTime.UtcNow && isCouponCodeExist.EndDateUtc >= DateTime.UtcNow)
                    {
                        if (isCouponCodeExist.UsePercentage)
                        {
                            bool nTimes = false;
                            bool nTimesPerCustomer = false;
                            bool unLimited = false;
                            switch (isCouponCodeExist.DiscountLimitationId)
                            {
                                case (int)DiscountLimitationType.NTimesOnly:
                                    {
                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimes = true;
                                        }
                                        else
                                        {
                                            nTimes = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.NTimesPerCustomer:
                                    {

                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimesPerCustomer = true;
                                        }
                                        else
                                        {
                                            nTimesPerCustomer = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.Unlimited:
                                    unLimited = true;
                                    break;
                                default:
                                    break;

                            }
                            if (nTimes || nTimesPerCustomer || unLimited)
                            {
                                var DiscountAmount = (isCouponCodeExist.DiscountPercentage / 100) * subTotal;
                                DiscountModel discountModel = new DiscountModel();
                                discountModel.DiscountAmount = DiscountAmount;
                                discountModel.DiscountId = isCouponCodeExist.Id;
                                customerAPIResponses.ErrorMessageTitle = "Success!!";
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
                                customerAPIResponses.Status = true;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                                customerAPIResponses.ResponseObj = discountModel;
                                return customerAPIResponses;
                            }
                            else
                            {
                                customerAPIResponses.ErrorMessageTitle = "Error!!";
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
                                customerAPIResponses.Status = false;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                customerAPIResponses.ResponseObj = null;
                                return customerAPIResponses;
                            }
                        }
                        else
                        {
                            bool nTimes = false;
                            bool nTimesPerCustomer = false;
                            bool unLimited = false;
                            switch (isCouponCodeExist.DiscountLimitationId)
                            {
                                case (int)DiscountLimitationType.NTimesOnly:
                                    {
                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimes = true;
                                        }
                                        else
                                        {
                                            nTimes = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.NTimesPerCustomer:
                                    {

                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimesPerCustomer = true;
                                        }
                                        else
                                        {
                                            nTimesPerCustomer = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.Unlimited:
                                    unLimited = true;
                                    break;
                                default:
                                    break;

                            }
                            if (nTimes || nTimesPerCustomer || unLimited)
                            {
                                if (subTotal >= isCouponCodeExist.DiscountAmount)
                                {
                                    var DiscountAmount = isCouponCodeExist.DiscountAmount;
                                    DiscountModel discountModel = new DiscountModel();
                                    discountModel.DiscountAmount = DiscountAmount;
                                    discountModel.DiscountId = isCouponCodeExist.Id;
                                    customerAPIResponses.ErrorMessageTitle = "Success!!";
                                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
                                    customerAPIResponses.Status = true;
                                    customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                                    customerAPIResponses.ResponseObj = discountModel;
                                    return customerAPIResponses;
                                }
                                else
                                {
                                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + isCouponCodeExist.DiscountAmount, languageId, dbcontext);
                                    customerAPIResponses.Status = false;
                                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                    customerAPIResponses.ResponseObj = null;
                                    return customerAPIResponses;
                                }

                            }
                            else
                            {
                                customerAPIResponses.ErrorMessageTitle = "Error!!";
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
                                customerAPIResponses.Status = false;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                customerAPIResponses.ResponseObj = null;
                                return customerAPIResponses;
                            }
                        }
                    }
                    else if (isCouponCodeExist.StartDateUtc == null && isCouponCodeExist.EndDateUtc >= DateTime.UtcNow)
                    {
                        if (isCouponCodeExist.UsePercentage)
                        {
                            bool nTimes = false;
                            bool nTimesPerCustomer = false;
                            bool unLimited = false;
                            switch (isCouponCodeExist.DiscountLimitationId)
                            {
                                case (int)DiscountLimitationType.NTimesOnly:
                                    {
                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, db: dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimes = true;
                                        }
                                        else
                                        {
                                            nTimes = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.NTimesPerCustomer:
                                    {

                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimesPerCustomer = true;
                                        }
                                        else
                                        {
                                            nTimesPerCustomer = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.Unlimited:
                                    unLimited = true;
                                    break;
                                default:
                                    break;

                            }
                            if (nTimes || nTimesPerCustomer || unLimited)
                            {
                                var DiscountAmount = (isCouponCodeExist.DiscountPercentage / 100) * subTotal;
                                DiscountModel discountModel = new DiscountModel();
                                discountModel.DiscountAmount = DiscountAmount;
                                discountModel.DiscountId = isCouponCodeExist.Id;
                                customerAPIResponses.ErrorMessageTitle = "Success!!";
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
                                customerAPIResponses.Status = true;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                                customerAPIResponses.ResponseObj = discountModel;
                                return customerAPIResponses;
                            }
                            else
                            {
                                customerAPIResponses.ErrorMessageTitle = "Error!!";
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
                                customerAPIResponses.Status = false;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                customerAPIResponses.ResponseObj = null;
                                return customerAPIResponses;
                            }
                        }
                        else
                        {
                            bool nTimes = false;
                            bool nTimesPerCustomer = false;
                            bool unLimited = false;
                            switch (isCouponCodeExist.DiscountLimitationId)
                            {
                                case (int)DiscountLimitationType.NTimesOnly:
                                    {
                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimes = true;
                                        }
                                        else
                                        {
                                            nTimes = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.NTimesPerCustomer:
                                    {

                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, db: dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimesPerCustomer = true;
                                        }
                                        else
                                        {
                                            nTimesPerCustomer = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.Unlimited:
                                    unLimited = true;
                                    break;
                                default:
                                    break;

                            }
                            if (nTimes || nTimesPerCustomer || unLimited)
                            {
                                if (subTotal >= isCouponCodeExist.DiscountAmount)
                                {
                                    var DiscountAmount = isCouponCodeExist.DiscountAmount;
                                    DiscountModel discountModel = new DiscountModel();
                                    discountModel.DiscountAmount = DiscountAmount;
                                    discountModel.DiscountId = isCouponCodeExist.Id;
                                    customerAPIResponses.ErrorMessageTitle = "Success!!";
                                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
                                    customerAPIResponses.Status = true;
                                    customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                                    customerAPIResponses.ResponseObj = discountModel;
                                    return customerAPIResponses;
                                }
                                else
                                {
                                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + isCouponCodeExist.DiscountAmount, languageId, dbcontext);
                                    customerAPIResponses.Status = false;
                                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                    customerAPIResponses.ResponseObj = null;
                                    return customerAPIResponses;
                                }

                            }
                            else
                            {
                                customerAPIResponses.ErrorMessageTitle = "Error!!";
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
                                customerAPIResponses.Status = false;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                customerAPIResponses.ResponseObj = null;
                                return customerAPIResponses;
                            }
                        }
                    }
                    else if (isCouponCodeExist.StartDateUtc <= DateTime.UtcNow && isCouponCodeExist.EndDateUtc == null)
                    {
                        if (isCouponCodeExist.UsePercentage)
                        {
                            bool nTimes = false;
                            bool nTimesPerCustomer = false;
                            bool unLimited = false;
                            switch (isCouponCodeExist.DiscountLimitationId)
                            {
                                case (int)DiscountLimitationType.NTimesOnly:
                                    {
                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimes = true;
                                        }
                                        else
                                        {
                                            nTimes = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.NTimesPerCustomer:
                                    {

                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimesPerCustomer = true;
                                        }
                                        else
                                        {
                                            nTimesPerCustomer = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.Unlimited:
                                    unLimited = true;
                                    break;
                                default:
                                    break;

                            }
                            if (nTimes || nTimesPerCustomer || unLimited)
                            {
                                var DiscountAmount = (isCouponCodeExist.DiscountPercentage / 100) * subTotal;
                                DiscountModel discountModel = new DiscountModel();
                                discountModel.DiscountAmount = DiscountAmount;
                                discountModel.DiscountId = isCouponCodeExist.Id;
                                customerAPIResponses.ErrorMessageTitle = "Success!!";
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
                                customerAPIResponses.Status = true;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                                customerAPIResponses.ResponseObj = discountModel;
                                return customerAPIResponses;
                            }
                            else
                            {
                                customerAPIResponses.ErrorMessageTitle = "Error!!";
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
                                customerAPIResponses.Status = false;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                customerAPIResponses.ResponseObj = null;
                                return customerAPIResponses;
                            }
                        }
                        else
                        {
                            bool nTimes = false;
                            bool nTimesPerCustomer = false;
                            bool unLimited = false;
                            switch (isCouponCodeExist.DiscountLimitationId)
                            {
                                case (int)DiscountLimitationType.NTimesOnly:
                                    {
                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimes = true;
                                        }
                                        else
                                        {
                                            nTimes = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.NTimesPerCustomer:
                                    {

                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimesPerCustomer = true;
                                        }
                                        else
                                        {
                                            nTimesPerCustomer = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.Unlimited:
                                    unLimited = true;
                                    break;
                                default:
                                    break;

                            }
                            if (nTimes || nTimesPerCustomer || unLimited)
                            {
                                if (subTotal >= isCouponCodeExist.DiscountAmount)
                                {
                                    var DiscountAmount = isCouponCodeExist.DiscountAmount;
                                    DiscountModel discountModel = new DiscountModel();
                                    discountModel.DiscountAmount = DiscountAmount;
                                    discountModel.DiscountId = isCouponCodeExist.Id;
                                    customerAPIResponses.ErrorMessageTitle = "Success!!";
                                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
                                    customerAPIResponses.Status = true;
                                    customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                                    customerAPIResponses.ResponseObj = discountModel;
                                    return customerAPIResponses;
                                }
                                else
                                {
                                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + isCouponCodeExist.DiscountAmount, languageId, dbcontext);
                                    customerAPIResponses.Status = false;
                                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                    customerAPIResponses.ResponseObj = null;
                                    return customerAPIResponses;
                                }

                            }
                            else
                            {
                                customerAPIResponses.ErrorMessageTitle = "Error!!";
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
                                customerAPIResponses.Status = false;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                customerAPIResponses.ResponseObj = null;
                                return customerAPIResponses;
                            }
                        }
                    }
                    else if (isCouponCodeExist.StartDateUtc != null)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodenotapplicable", languageId, dbcontext);

                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                    else if (isCouponCodeExist.EndDateUtc != null)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodenotapplicable", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                    else if (isCouponCodeExist.EndDateUtc != null && isCouponCodeExist.EndDateUtc != null)
                    {
                        customerAPIResponses.ErrorMessageTitle = "Error!!";
                        customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodenotapplicable", languageId, dbcontext);
                        customerAPIResponses.Status = false;
                        customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                        customerAPIResponses.ResponseObj = null;
                        return customerAPIResponses;
                    }
                    else
                    {
                        if (isCouponCodeExist.UsePercentage)
                        {
                            bool nTimes = false;
                            bool nTimesPerCustomer = false;
                            bool unLimited = false;
                            switch (isCouponCodeExist.DiscountLimitationId)
                            {
                                case (int)DiscountLimitationType.NTimesOnly:
                                    {
                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimes = true;
                                        }
                                        else
                                        {
                                            nTimes = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.NTimesPerCustomer:
                                    {

                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimesPerCustomer = true;
                                        }
                                        else
                                        {
                                            nTimesPerCustomer = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.Unlimited:
                                    unLimited = true;
                                    break;
                                default:
                                    break;

                            }
                            if (nTimes || nTimesPerCustomer || unLimited)
                            {
                                var DiscountAmount = (isCouponCodeExist.DiscountPercentage / 100) * subTotal;
                                DiscountModel discountModel = new DiscountModel();
                                discountModel.DiscountAmount = DiscountAmount;
                                discountModel.DiscountId = isCouponCodeExist.Id;
                                customerAPIResponses.ErrorMessageTitle = "Success!!";
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
                                customerAPIResponses.Status = true;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                                customerAPIResponses.ResponseObj = discountModel;
                                return customerAPIResponses;
                            }
                            else
                            {
                                customerAPIResponses.ErrorMessageTitle = "Error!!";
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
                                customerAPIResponses.Status = false;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                customerAPIResponses.ResponseObj = null;
                                return customerAPIResponses;
                            }
                        }
                        else
                        {
                            bool nTimes = false;
                            bool nTimesPerCustomer = false;
                            bool unLimited = false;
                            switch (isCouponCodeExist.DiscountLimitationId)
                            {
                                case (int)DiscountLimitationType.NTimesOnly:
                                    {
                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, null, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimes = true;
                                        }
                                        else
                                        {
                                            nTimes = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.NTimesPerCustomer:
                                    {

                                        var usedTimes = Helper.GetAllDiscountUsageHistory(isCouponCodeExist.Id, CustomerId, null, 0, 1, dbcontext).ToList().Count();
                                        if (usedTimes < isCouponCodeExist.LimitationTimes)
                                        {
                                            nTimesPerCustomer = true;
                                        }
                                        else
                                        {
                                            nTimesPerCustomer = false;
                                        }
                                    }
                                    break;
                                case (int)DiscountLimitationType.Unlimited:
                                    unLimited = true;
                                    break;
                                default:
                                    break;

                            }
                            if (nTimes || nTimesPerCustomer || unLimited)
                            {
                                if (subTotal >= isCouponCodeExist.DiscountAmount)
                                {
                                    var DiscountAmount = isCouponCodeExist.DiscountAmount;
                                    DiscountModel discountModel = new DiscountModel();
                                    discountModel.DiscountAmount = DiscountAmount;
                                    discountModel.DiscountId = isCouponCodeExist.Id;
                                    customerAPIResponses.ErrorMessageTitle = "Success!!";
                                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.DiscountApplySuccessfully", languageId, dbcontext);
                                    customerAPIResponses.Status = true;
                                    customerAPIResponses.StatusCode = (int)HttpStatusCode.OK;
                                    customerAPIResponses.ResponseObj = discountModel;
                                    return customerAPIResponses;
                                }
                                else
                                {
                                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Amountshouldbemorethan" + isCouponCodeExist.DiscountAmount, languageId, dbcontext);
                                    customerAPIResponses.Status = false;
                                    customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                    customerAPIResponses.ResponseObj = null;
                                    return customerAPIResponses;
                                }

                            }
                            else
                            {
                                customerAPIResponses.ErrorMessageTitle = "Error!!";
                                customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodeexpired", languageId, dbcontext);
                                customerAPIResponses.Status = false;
                                customerAPIResponses.StatusCode = (int)HttpStatusCode.NoContent;
                                customerAPIResponses.ResponseObj = null;
                                return customerAPIResponses;
                            }
                        }
                    }
                }
                else
                {
                    customerAPIResponses.ErrorMessageTitle = "Error!!";
                    customerAPIResponses.ErrorMessage = LanguageHelper.GetResourseValueByName(storeId, "API.ErrorMesaage.Couponcodenotapplicable", languageId, dbcontext);
                    customerAPIResponses.Status = false;
                    customerAPIResponses.StatusCode = (int)HttpStatusCode.BadRequest;
                    customerAPIResponses.ResponseObj = null;
                    return customerAPIResponses;
                }
            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                throw;
            }
        }
        public static List<DiscountUsageHistory> GetAllDiscountUsageHistory(int? discountId = null,
            int? customerId = null, int? orderId = null, int pageIndex = 0, int pageSize = int.MaxValue, ecuadordevContext db = null)
        {
            var discountUsageHistory = db.DiscountUsageHistory.AsEnumerable().ToList();

            //filter by discount
            if (discountId.HasValue && discountId.Value > 0)
                discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.DiscountId == discountId.Value).ToList();

            //filter by customer
            if (customerId.HasValue && customerId.Value > 0)
                discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.Order != null && historyRecord.Order.CustomerId == customerId.Value).ToList();

            //filter by order
            if (orderId.HasValue && orderId.Value > 0)
                discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.OrderId == orderId.Value).ToList();

            //ignore deleted orders
            discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.Order != null && !historyRecord.Order.Deleted).ToList();

            //order
            discountUsageHistory = discountUsageHistory.OrderByDescending(historyRecord => historyRecord.CreatedOnUtc).ThenBy(historyRecord => historyRecord.Id).ToList();
            return discountUsageHistory;
        }

        public static string GetStripeKey(int storeId, ecuadordevContext db)
        {
            var stripeKey = db.Setting.FirstOrDefault(setting => setting.Name.Contains("stripepaymentsettings.secretkey")
                          && setting.StoreId == storeId)?.Value ?? "";
            return stripeKey;

        }

        public static string GetConnectionString()
        {
            try
            {
                return (new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("ConnectionStrings").GetSection("ecuadordatabase").Value).ToString();
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
        public static void SentryLogs(Exception exception)
        {
            //var ravenClient = new RavenClient(sentryURL);
            //ravenClient.Capture(new SentryEvent(exception));
        }
        //public static List<DiscountUsageHistory> GetAllDiscountUsageHistory(int? discountId = null,
        //    int? customerId = null, int? orderId = null, int pageIndex = 0, int pageSize = int.MaxValue)
        //{
        //    using (var db = new CustomerNestorhawkContext())
        //    {
        //        var discountUsageHistory = db.DiscountUsageHistories.ToList();

        //        //filter by discount
        //        if (discountId.HasValue && discountId.Value > 0)
        //            discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.DiscountId == discountId.Value).ToList();

        //        //filter by customer
        //        if (customerId.HasValue && customerId.Value > 0)
        //            discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.Order != null && historyRecord.Order.CustomerId == customerId.Value).ToList();

        //        //filter by order
        //        if (orderId.HasValue && orderId.Value > 0)
        //            discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.OrderId == orderId.Value).ToList();

        //        //ignore deleted orders
        //        discountUsageHistory = discountUsageHistory.Where(historyRecord => historyRecord.Order != null && !historyRecord.Order.Deleted).ToList();

        //        //order
        //        discountUsageHistory = discountUsageHistory.OrderByDescending(historyRecord => historyRecord.CreatedOnUtc).ThenBy(historyRecord => historyRecord.Id).ToList();
        //        return discountUsageHistory;
        //    }
        //}

        public static bool IsNumeric(string strValue)
        {
            int intValue;
            return int.TryParse(strValue, out intValue);
        }

        public static decimal GetTaxRate(Product product, int storeId, ecuadordevContext db)
        {
            decimal result = 0;
            try
            {
                if (product == null || product.VendorId == 0 || string.IsNullOrEmpty(product.TaxCategoryIds))
                {
                    return result;
                }
                var address = (from a in db.Address
                               join v in db.Vendor
                               on a.Id equals v.AddressId
                               where v.Id == product.VendorId
                               select a).FirstOrDefault();
                if (address == null)
                {
                    return result;
                }

                var allTaxRates = db.TaxRate.Select(taxRate => new
                {
                    Id = taxRate.Id,
                    StoreId = taxRate.StoreId,
                    TaxCategoryId = taxRate.TaxCategoryId,
                    CountryId = taxRate.CountryId,
                    StateProvinceId = taxRate.StateProvinceId,
                    Zip = taxRate.Zip,
                    Percentage = taxRate.Percentage
                }).ToList();

                var countryId = address.CountryId;
                var stateProvinceId = address.StateProvinceId;
                var zip = address.ZipPostalCode?.Trim() ?? string.Empty;

                //get tax by tax categories
                var taxCategoryIds = new List<int>();
                if (!string.IsNullOrEmpty(product?.TaxCategoryIds))
                    taxCategoryIds = product.TaxCategoryIds.Split(',').Select(x => int.Parse(x)).ToList();
                var existingRates = allTaxRates.Where(taxRate => taxRate.CountryId == countryId && taxCategoryIds.Contains(taxRate.TaxCategoryId));

                //filter by store
                var matchedByStore = existingRates.Where(taxRate => storeId == taxRate.StoreId || taxRate.StoreId == 0);

                //filter by state/province
                var matchedByStateProvince = matchedByStore.Where(taxRate => stateProvinceId == taxRate.StateProvinceId || taxRate.StateProvinceId == 0);

                //filter by zip
                var matchedByZip = matchedByStateProvince.Where(taxRate => string.IsNullOrWhiteSpace(taxRate.Zip) || taxRate.Zip.Equals(zip, StringComparison.InvariantCultureIgnoreCase));

                //sort from particular to general, more particular cases will be the first
                var foundRecords = matchedByZip.OrderBy(r => r.StoreId == 0).ThenBy(r => r.StateProvinceId == 0).ThenBy(r => string.IsNullOrEmpty(r.Zip));

                var foundRecord = foundRecords.ToList();

                if (foundRecord.Count > 0)
                    result = foundRecord.Sum(x => x.Percentage);
            }
            catch (Exception ex)
            {
                //no need to show ex
            }
            return result;
        }

        public static string GetProductPictureUrl(Product product,int pictureSize, string baseUrl,ecuadordevContext dbcontext)
        {
            string productImageURL = string.Format("{0}default-prod-image_{1}.png?{2}", baseUrl, pictureSize, DateTime.Now.ToFileTime());
            var picture = (from ppm in dbcontext.ProductPictureMapping.Where(x => x.ProductId == product.Id)
                           join pic in dbcontext.Picture
                           on ppm.PictureId equals pic.Id
                           select pic).FirstOrDefault();
            if (picture!=null)
            {
                if (picture != null)
                {

                    string lastPart = GetFileExtensionFromMimeType(picture.MimeType);
                    string thumbFileName = !string.IsNullOrEmpty(picture.SeoFilename)
                    ? $"{picture.Id:0000000}_{picture.SeoFilename}.{lastPart}" + "?" + DateTime.Now.ToFileTime()
                    : $"{picture.Id:0000000}_{pictureSize}.{lastPart}" + "?" + DateTime.Now.ToFileTime();

                    productImageURL = baseUrl + thumbFileName + "?" + DateTime.Now.ToFileTime();
                }
            }
            return productImageURL;
        }

        public static List<CooknRestProductProductAttributesResponse> GetProductAttributes(Product product,string currencySymbol,ecuadordevContext dbcontext)
        {
            List<CooknRestProductProductAttributesResponse> productAttributes = new List<CooknRestProductProductAttributesResponse>();
                productAttributes = (from _e in dbcontext.ProductProductAttributeMapping
                                     join _f in dbcontext.ProductAttribute
                                     on _e.ProductAttributeId equals _f.Id
                                     where _e.ProductId==product.Id
                                     select new CooknRestProductProductAttributesResponse
                                     {
                                         AttributeName = _f.Name,
                                         IsRequired = _e.IsRequired,
                                         AttributeTypeId = _e.AttributeControlTypeId,
                                         ProductId = _e.ProductId,
                                         ProductAttributeId = _f.Id,
                                         ProductAttributeMappingId = _e.Id,
                                         ProductAttribute = (from __d in dbcontext.ProductAttributeValue
                                                             where __d.ProductAttributeMappingId == _e.Id
                                                             select new ProductAttributes
                                                             {
                                                                 Name = __d.Name,
                                                                 Price = __d.PriceAdjustment.ToString(),
                                                                 ProductAttributeValueId = __d.Id,
                                                                 IsPreSelected = __d.IsPreSelected,
                                                                 Currency = currencySymbol
                                                             }).ToList()
                                     }).Distinct().ToList();
            return productAttributes;
        }

        public static ItemsAPIResponses GetAPIErrorResponses(string errorMessage,int StatusCode= (int)HttpStatusCode.NoContent)
        {
            return new ItemsAPIResponses
            {
                ErrorMessageTitle = "Error!!",
                ErrorMessage = errorMessage,
                Status = false,
                StatusCode = StatusCode,
                ResponseObj = null
            };
        }

        /// <summary>
        /// Functionality to calculate delivery fee
        /// </summary>
        /// <returns>returns Delivery Fees in decimal </returns>
        public static decimal CalculateDeliveryFee(int RestnCookId, int BillingAddressId, int storeId, ecuadordevContext dbcontext)
        {
            decimal distance = 0;
            decimal deliveryFee = 0;

            var merchant = dbcontext.Vendor.FirstOrDefault(x=>x.Id == RestnCookId);
            if (merchant != null)
            {
                if (merchant.FixedOrDynamic == "f")
                {
                    deliveryFee = merchant.DeliveryCharge;
                }
                else if (merchant.FixedOrDynamic == "d")
                {
                    deliveryFee = merchant.PriceForDynamic;
                    try
                    {
                        var venLtLngStr = merchant.Geolocation;
                        var BillingAddress = dbcontext.Address.FirstOrDefault(x => x.Id == BillingAddressId);
                        if (venLtLngStr.Any() && BillingAddress != null && !string.IsNullOrEmpty(BillingAddress.Address2))
                        {
                            var lat = string.Empty;
                            var lon = string.Empty;
                            string[] venLtLngStrArr = venLtLngStr.Replace("{\"type\":\"MARKER\",\"coordinates\":{\"lat\":", "").Replace("\"lng\":", "").Replace("}}", "").Split(',');
                            if (venLtLngStrArr.Length > 0)
                            {
                                lat = venLtLngStrArr[0].ToString();
                                lon = venLtLngStrArr[1].ToString();
                            }
                            if (!string.IsNullOrEmpty(lat) && !string.IsNullOrEmpty(lon))
                            {
                                decimal number = 0;
                                var distanceString = GetDistance(string.Format("{0},{1}", lat, lon), BillingAddress.Address2, storeId, dbcontext);
                                if (Decimal.TryParse(distanceString, out number))
                                {
                                    if (merchant.KmOrMilesForDynamic == "k")
                                    {
                                        distance = Math.Round(number / 1000, 1);
                                    }
                                    else if (merchant.KmOrMilesForDynamic == "m")
                                    {
                                        distance = Math.Round(number / Convert.ToDecimal(1609.34), 1);
                                    }
                                }
                            }
                        }
                    }
                    catch { }

                    if (distance > merchant.DistanceForDynamic)
                    {
                        deliveryFee += (distance - merchant.DistanceForDynamic) * merchant.PricePerUnitDistanceForDynamic;
                    }
                }
            }

            return deliveryFee;
        }

        /// <summary>
        /// Functionality to calculate delivery fee
        /// </summary>
        /// <returns>returns Delivery Fees in decimal </returns>
        public static string GetDistance(string origin, string destination, int storeId, ecuadordevContext dbcontext)
        {
            string result = string.Empty;
            var keyObject = dbcontext.Setting.Where(x => x.Name == "NB.Google.Account.Key" && x.StoreId == storeId).FirstOrDefault();
            string key = keyObject != null ? keyObject.Value : string.Empty;
            string url = @"https://maps.googleapis.com/maps/api/distancematrix/xml?origins=" + origin + "&destinations=" + destination + "&sensor=false&key=" + key;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader sreader = new StreamReader(dataStream);
            string responsereader = sreader.ReadToEnd();
            response.Close();

            DataSet ds = new DataSet();
            ds.ReadXml(new XmlTextReader(new StringReader(responsereader)));
            if (ds.Tables.Count > 0)
            {
                if(ds.Tables["element"] != null && ds.Tables["element"].Rows[0]["status"] != null)
                {
                    if (ds.Tables["element"].Rows[0]["status"].ToString() == "OK")
                    {
                        result = ds.Tables["distance"].Rows[0]["value"].ToString();
                    }
                }
           
            }
            return result;
        }

        public static decimal CalculateServiceChargeAmount(decimal subTotal, int storeId, ecuadordevContext dbcontext)
        {
            decimal serviceChargeAmount = 0;

            if (storeId == 0)
                return serviceChargeAmount;
            var usePercentageObject = dbcontext.Setting.Where(x => x.Name == "setting.storesetup.UsePercentage" && x.StoreId == storeId).FirstOrDefault();
            bool usePercentage = usePercentageObject != null ? Convert.ToBoolean(usePercentageObject.Value) : false;

            var flatAmountObject = dbcontext.Setting.Where(x => x.Name == "setting.storesetup.FlatAmount" && x.StoreId == storeId).FirstOrDefault();
            decimal flatAmount = flatAmountObject != null ? Convert.ToDecimal(flatAmountObject.Value) : 0;

            var serviceChargePercentageObject = dbcontext.Setting.Where(x => x.Name == "setting.storesetup.ServiceChargePercentage" && x.StoreId == storeId).FirstOrDefault();
            decimal serviceChargePercentage = serviceChargePercentageObject != null ? Convert.ToDecimal(serviceChargePercentageObject.Value) : 0;

            var maximumServiceChargeAmountObject = dbcontext.Setting.Where(x => x.Name == "setting.storesetup.maximumservicechargeamount" && x.StoreId == storeId).FirstOrDefault();
            decimal maximumServiceChargeAmount = maximumServiceChargeAmountObject != null ? Convert.ToDecimal(maximumServiceChargeAmountObject.Value) : 0;


            if (usePercentage)
            {
                if (subTotal > 0 && serviceChargePercentage > 0)
                {
                    var serviceAmount = (subTotal * serviceChargePercentage) / 100;

                    if (serviceAmount > maximumServiceChargeAmount)
                    {
                        serviceChargeAmount = maximumServiceChargeAmount;
                    }
                    else
                    {
                        serviceChargeAmount = serviceAmount;
                    }
                }
            }
            else
            {
                serviceChargeAmount = flatAmount;
            }

            return serviceChargeAmount;
        }

        #region "PayPal section"
        /// <summary>
        /// Get PayPal Payment Url
        /// </summary>
        /// <param name="order"></param>
        /// <param name="dbcontext"></param>
        /// <param name="httpRequest"></param>
        /// <returns>return paypal url</returns>
        public static string GetPayPalPaymentUrl(Order order, ecuadordevContext dbcontext, HttpContext httpRequest)
        {
            string sendBox = dbcontext.Setting.Where(x => x.Name.ToLower().Trim().Equals("paypalstandardpaymentsettings.usesandbox") && x.StoreId == order.StoreId).FirstOrDefault()?.Value??"";

            var baseUrl = !string.IsNullOrEmpty(sendBox) && Convert.ToBoolean(sendBox) ?
                "https://www.sandbox.paypal.com/us/cgi-bin/webscr" :
                "https://www.paypal.com/us/cgi-bin/webscr";

            //create common query parameters for the request
            var queryParameters = CreateQueryParameters(order, dbcontext, httpRequest);

            string passProductNamesAndTotals = dbcontext.Setting.Where(x => x.Name.ToLower().Trim().Equals("paypalstandardpaymentsettings.passproductnamesandtotals") && x.StoreId == order.StoreId).FirstOrDefault()?.Value ?? "";

            //whether to include order items in a transaction
            if (!string.IsNullOrEmpty(passProductNamesAndTotals) && Convert.ToBoolean(passProductNamesAndTotals))
            {
                //add order items query parameters to the request
                var parameters = new Dictionary<string, string>(queryParameters);
                AddItemsParameters(parameters, order, dbcontext);

                //remove null values from parameters
                parameters = parameters.Where(parameter => !string.IsNullOrEmpty(parameter.Value))
                    .ToDictionary(parameter => parameter.Key, parameter => parameter.Value);

                //ensure redirect URL doesn't exceed 2K chars to avoid "too long URL" exception
                var redirectUrl = QueryHelpers.AddQueryString(baseUrl, parameters);
                if (redirectUrl.Length <= 2048)
                {
                    return redirectUrl;
                }
            }

            //or add only an order total query parameters to the request
            AddOrderTotalParameters(queryParameters, order, dbcontext);

            //remove null values from parameters
            queryParameters = queryParameters.Where(parameter => !string.IsNullOrEmpty(parameter.Value))
                .ToDictionary(parameter => parameter.Key, parameter => parameter.Value);

            var url = QueryHelpers.AddQueryString(baseUrl, queryParameters);

            return url;
        }

        /// <summary>
        /// Create common query parameters for the request
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Created query parameters</returns>
        private static IDictionary<string, string> CreateQueryParameters(Order order, ecuadordevContext dbcontext, HttpContext httpRequest)
        {
            //get store location
            var storeLocation = httpRequest.Request;

            //choosing correct order address
            var orderAddress = order.PickupInStore
                    ? order.PickupAddress
                    : order.ShippingAddress;

            string businessEmail = dbcontext.Setting.Where(x => x.Name.ToLower().Trim().Equals("paypalstandardpaymentsettings.businessemail") && x.StoreId == order.StoreId).FirstOrDefault()?.Value ?? "";

            var storeUrl = dbcontext.Store.Where(x => x.Id == order.StoreId).FirstOrDefault().Url;

            //create query parameters
            return new Dictionary<string, string>
            {
                //PayPal ID or an email address associated with your PayPal account
                ["business"] = businessEmail,

                //the character set and character encoding
                ["charset"] = "utf-8",

                //set return method to "2" (the customer redirected to the return URL by using the POST method, and all payment variables are included)
                ["rm"] = "2",

                ["bn"] = PayPalHelper.NopCommercePartnerCode,
                ["currency_code"] = order.CustomerCurrencyCode,

                //order identifier
                ["invoice"] = order.CustomOrderNumber,
                ["custom"] = order.OrderGuid.ToString(),

                //PDT, IPN and cancel URL
                ["return"] = $"{storeLocation.Scheme}://{storeLocation.Host.Value}/api/PDTHandler",
                ["notify_url"] = $"{storeUrl}Plugins/PaymentPayPalStandard/IPNHandler",
                ["cancel_return"] = $"{storeLocation.Scheme}://{storeLocation.Host.Value}/api/CancelOrder",

                //shipping address, if exists
                ["no_shipping"] = order.ShippingStatusId == (int) ShippingStatus.ShippingNotRequired ? "1" : "2",
                ["address_override"] = order.ShippingStatusId == (int) ShippingStatus.ShippingNotRequired ? "0" : "1",
                ["first_name"] = orderAddress?.FirstName,
                ["last_name"] = orderAddress?.LastName,
                ["address1"] = orderAddress?.Address1,
                ["address2"] = orderAddress?.Address2,
                ["city"] = orderAddress?.City,
                ["state"] = orderAddress?.StateProvince?.Abbreviation,
                ["country"] = orderAddress?.Country?.TwoLetterIsoCode,
                ["zip"] = orderAddress?.ZipPostalCode,
                ["email"] = orderAddress?.Email
            };
        }

        /// <summary>
        /// Add order items to the request query parameters
        /// </summary>
        /// <param name="parameters">Query parameters</param>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        private static void AddItemsParameters(IDictionary<string, string> parameters, Order order, ecuadordevContext dbcontext)
        {
            //upload order items
            parameters.Add("cmd", "_cart");
            parameters.Add("upload", "1");

            var cartTotal = decimal.Zero;
            var roundedCartTotal = decimal.Zero;
            var itemCount = 1;

            //add shopping cart items
            foreach (var item in order.OrderItem)
            {
                var roundedItemPrice = Math.Round(item.UnitPriceExclTax, 2);

                //add query parameters
                parameters.Add($"item_name_{itemCount}", item.Product.Name);
                parameters.Add($"amount_{itemCount}", roundedItemPrice.ToString("0.00", CultureInfo.InvariantCulture));
                parameters.Add($"quantity_{itemCount}", item.Quantity.ToString());

                cartTotal += item.PriceExclTax;
                roundedCartTotal += roundedItemPrice * item.Quantity;
                itemCount++;
            }

            //add checkout attributes as order items
            //var checkoutAttributeValues = _checkoutAttributeParser.ParseCheckoutAttributeValues(order.CheckoutAttributesXml);
            //foreach (var attributeValue in checkoutAttributeValues)
            //{
            //    var attributePrice = _taxService.GetCheckoutAttributePrice(attributeValue, false, order.Customer);
            //    var roundedAttributePrice = Math.Round(attributePrice, 2);

            //    //add query parameters
            //    if (attributeValue.CheckoutAttribute == null)
            //        continue;

            //    parameters.Add($"item_name_{itemCount}", attributeValue.CheckoutAttribute.Name);
            //    parameters.Add($"amount_{itemCount}", roundedAttributePrice.ToString("0.00", CultureInfo.InvariantCulture));
            //    parameters.Add($"quantity_{itemCount}", "1");

            //    cartTotal += attributePrice;
            //    roundedCartTotal += roundedAttributePrice;
            //    itemCount++;
            //}

            //add shipping fee as a separate order item, if it has price
            var roundedShippingPrice = Math.Round(order.OrderShippingExclTax, 2);
            if (roundedShippingPrice > decimal.Zero)
            {
                parameters.Add($"item_name_{itemCount}", "Shipping fee");
                parameters.Add($"amount_{itemCount}", roundedShippingPrice.ToString("0.00", CultureInfo.InvariantCulture));
                parameters.Add($"quantity_{itemCount}", "1");

                cartTotal += order.OrderShippingExclTax;
                roundedCartTotal += roundedShippingPrice;
                itemCount++;
            }

            //add payment method additional fee as a separate order item, if it has price
            var roundedPaymentMethodPrice = Math.Round(order.PaymentMethodAdditionalFeeExclTax, 2);
            if (roundedPaymentMethodPrice > decimal.Zero)
            {
                parameters.Add($"item_name_{itemCount}", "Payment method fee");
                parameters.Add($"amount_{itemCount}", roundedPaymentMethodPrice.ToString("0.00", CultureInfo.InvariantCulture));
                parameters.Add($"quantity_{itemCount}", "1");

                cartTotal += order.PaymentMethodAdditionalFeeExclTax;
                roundedCartTotal += roundedPaymentMethodPrice;
                itemCount++;
            }

            //add tax as a separate order item, if it has positive amount
            var roundedTaxAmount = Math.Round(order.OrderTax, 2);
            if (roundedTaxAmount > decimal.Zero)
            {
                parameters.Add($"item_name_{itemCount}", "Tax amount");
                parameters.Add($"amount_{itemCount}", roundedTaxAmount.ToString("0.00", CultureInfo.InvariantCulture));
                parameters.Add($"quantity_{itemCount}", "1");

                cartTotal += order.OrderTax;
                roundedCartTotal += roundedTaxAmount;
            }

            if (cartTotal > order.OrderTotal)
            {
                //get the difference between what the order total is and what it should be and use that as the "discount"
                var discountTotal = Math.Round(cartTotal - order.OrderTotal, 2);
                roundedCartTotal -= discountTotal;

                //gift card or rewarded point amount applied to cart in nopCommerce - shows in PayPal as "discount"
                parameters.Add("discount_amount_cart", discountTotal.ToString("0.00", CultureInfo.InvariantCulture));
            }

            //save order total that actually sent to PayPal (used for PDT order total validation)
            GenericAttribute genericAttributeGender = new GenericAttribute
            {
                Key = PayPalHelper.OrderTotalSentToPayPal,
                EntityId = order.Id,
                Value = roundedCartTotal.ToString(),
                KeyGroup = "Order",
                StoreId = order.StoreId
            };
            dbcontext.GenericAttribute.Add(genericAttributeGender);
            dbcontext.SaveChanges();
        }

        /// <summary>
        /// Add order total to the request query parameters
        /// </summary>
        /// <param name="parameters">Query parameters</param>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        private static void AddOrderTotalParameters(IDictionary<string, string> parameters, Order order, ecuadordevContext dbcontext)
        {
            //round order total
            var roundedOrderTotal = Math.Round(order.OrderTotal, 2);

            parameters.Add("cmd", "_xclick");
            parameters.Add("item_name", $"Order Number {order.CustomOrderNumber}");
            parameters.Add("amount", roundedOrderTotal.ToString("0.00", CultureInfo.InvariantCulture));

            //save order total that actually sent to PayPal (used for PDT order total validation)
            GenericAttribute genericAttributeGender = new GenericAttribute
            {
                Key = PayPalHelper.OrderTotalSentToPayPal,
                EntityId = order.Id,
                Value = roundedOrderTotal.ToString(),
                KeyGroup = "Order",
                StoreId = order.StoreId
            };
            dbcontext.GenericAttribute.Add(genericAttributeGender);
            dbcontext.SaveChanges();
        }

        /// <summary>
        /// Gets a value indicating whether order can be marked as paid
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>A value indicating whether order can be marked as paid</returns>
        public static bool CanMarkOrderAsPaid(Models.Order order)
        {
            if (order.PaymentStatusId == (int)PaymentStatus.Paid ||
                order.PaymentStatusId == (int)PaymentStatus.Refunded ||
                order.PaymentStatusId == (int)PaymentStatus.Voided)
                return false;

            return true;
        }

        /// <summary>
        /// Gets PDT details
        /// </summary>
        /// <param name="tx">TX</param>
        /// <param name="values">Values</param>
        /// <param name="response">Response</param>
        /// <returns>Result</returns>
        public static bool GetPdtDetails(string tx, string customOrderNumber, ecuadordevContext db, out Dictionary<string, string> values, out string response)
        {
            response = WebUtility.UrlDecode(GetPdtDetailsAsync(tx, customOrderNumber, db).Result);

            values = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            bool firstLine = true, success = false;
            foreach (var l in response.Split('\n'))
            {
                var line = l.Trim();
                if (firstLine)
                {
                    success = line.Equals("SUCCESS", StringComparison.OrdinalIgnoreCase);
                    firstLine = false;
                }
                else
                {
                    var equalPox = line.IndexOf('=');
                    if (equalPox >= 0)
                        values.Add(line.Substring(0, equalPox), line.Substring(equalPox + 1));
                }
            }

            return success;
        }


        /// <summary>
        /// Gets PDT details
        /// </summary>
        /// <param name="tx">TX</param>
        /// <returns>The asynchronous task whose result contains the PDT details</returns>
        public static async Task<string> GetPdtDetailsAsync(string tx, string customOrderNumber, ecuadordevContext dbcontext)
        {
            HttpClient client = new HttpClient();
            //configure client
            client.Timeout = TimeSpan.FromMilliseconds(5000);
            client.DefaultRequestHeaders.Add("User-Agent", $"nopCommerce-4.20");

            var orderNumberGuid = Guid.Empty;
            try
            {
                orderNumberGuid = new Guid(customOrderNumber);
            }
            catch
            {
                // ignored
            }

            var order = dbcontext.Order.Where(x => x.OrderGuid == orderNumberGuid).FirstOrDefault();

            //get response
            string sendBox = dbcontext.Setting.Where(x => x.Name.ToLower().Trim().Equals("paypalstandardpaymentsettings.usesandbox") && x.StoreId == order.StoreId).FirstOrDefault()?.Value ?? "";

            //get response
            string pdtToken = dbcontext.Setting.Where(x => x.Name.ToLower().Trim().Equals("paypalstandardpaymentsettings.pdttoken") && x.StoreId == order.StoreId).FirstOrDefault()?.Value ?? "";

            var url = !string.IsNullOrEmpty(sendBox) && Convert.ToBoolean(sendBox) ?
                "https://www.sandbox.paypal.com/us/cgi-bin/webscr" :
                "https://www.paypal.com/us/cgi-bin/webscr";
            var requestContent = new StringContent($"cmd=_notify-synch&at={pdtToken}&tx={tx}",
                Encoding.UTF8, "application/x-www-form-urlencoded");
            var response = await client.PostAsync(url, requestContent);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }

        #endregion

        #region Email notification

        public static void SendEmailNotification(Order order, OrderReviewModel orderReviewModel, int languageId, ecuadordevContext dbcontext)
        {
            try
            {
                var store = dbcontext.Store.Where(x=>x.Id == order.StoreId).FirstOrDefault();
                var CustomerAddressDetails = dbcontext.GenericAttribute.Where(x => x.EntityId == order.CustomerId).ToList();
                var customer = dbcontext.Customer.Where(x => x.Id == order.CustomerId).ToList().FirstOrDefault();

                var customerName = CustomerAddressDetails.Where(x => x.Key.Contains("FirstName"))?.FirstOrDefault()?.Value + " " + (CustomerAddressDetails.Where(x => x.Key.Contains("LastName")).Any() ? CustomerAddressDetails.Where(x => x.Key.Contains("LastName"))?.FirstOrDefault()?.Value : "");

                /* Customer Email notification*/
                var messageTemplate = dbcontext.MessageTemplate.Where(x => x.Name.Contains("CustomerApp.Notification.OrderPlaced")
                                            && (dbcontext.StoreMapping.Any(y => y.StoreId == orderReviewModel.StoreId && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
                                            ).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();
                if (messageTemplate != null)
                {
                    //Order confirm email for mobile app
                    string body = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Body", languageId, messageTemplate.Body, dbcontext);
                    string subject = LanguageHelper.GetLocalizedValueMessageTemplate(messageTemplate.Id, "Subject", languageId, messageTemplate.Subject, dbcontext);

                    body = body.Replace("%Store.URL%", store.Url);
                    body = body.Replace("%Store.Name%", LanguageHelper.GetLocalizedValue(store.Id, "Name", "Store", languageId, store.Name, dbcontext));
                    body = body.Replace("%Customer.OrderNumber%", order.CustomOrderNumber);
                    body = body.Replace("%Customer.FullName%", customerName);

                    var emailAccount = (from b in dbcontext.EmailAccount
                                        where b.StoreId == store.Id
                                        select new
                                        {
                                            b
                                        }).FirstOrDefault()?.b;
                    if (emailAccount != null)
                    {
                        try
                        {
                            Helper.InsertQueuedEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, customer.Email, customerName, messageTemplate, dbcontext);
                        }
                        catch (Exception ex) 
                        {
                            Helper.SentryLogs(ex);
                        }
                    }

                }
                /* Customer Email notification*/

                /* Vendor Email notification*/

                var vendors = (from p in dbcontext.Product
                               join oi in dbcontext.OrderItem on p.Id equals oi.ProductId
                               join v in dbcontext.Vendor on p.VendorId equals v.Id
                               where oi.OrderId == order.Id
                               select v).Distinct().ToList();

                foreach (var vendor in vendors)
                {
                    var messageVendorTemplate = dbcontext.MessageTemplate.Where(x => x.Name.Contains("OrderPlaced.VendorNotification")
                                            && (dbcontext.StoreMapping.Any(y => y.StoreId == orderReviewModel.StoreId && y.EntityId == x.Id && y.EntityName == "MessageTemplate") || !x.LimitedToStores)
                                            ).OrderByDescending(x => x.LimitedToStores).FirstOrDefault();
                    if (messageVendorTemplate != null)
                    {
                        //Order confirm email for mobile app
                        string body = LanguageHelper.GetLocalizedValueMessageTemplate(messageVendorTemplate.Id, "Body", languageId, messageVendorTemplate.Body, dbcontext);
                        string subject = LanguageHelper.GetLocalizedValueMessageTemplate(messageVendorTemplate.Id, "Subject", languageId, messageVendorTemplate.Subject, dbcontext);

                        subject = subject.Replace("%Store.Name%", LanguageHelper.GetLocalizedValue(store.Id, "Name", "Store", languageId, store.Name, dbcontext));

                        body = body.Replace("%Customer.Email%", customer.Email);
                        body = body.Replace("%Order.OrderNumber%", order.CustomOrderNumber);
                        body = body.Replace("%Order.CreatedOn%", order.CreatedOnUtc.ToString());
                        body = body.Replace("%Store.URL%", store.Url);
                        body = body.Replace("%Store.Name%", LanguageHelper.GetLocalizedValue(store.Id, "Name", "Store", languageId, store.Name, dbcontext));
                        body = body.Replace("%Customer.FullName%", customerName);

                        var emailAccount = (from b in dbcontext.EmailAccount
                                            where b.StoreId == store.Id
                                            select new
                                            {
                                                b
                                            }).FirstOrDefault()?.b;
                        if (emailAccount != null)
                        {
                            try
                            {
                                Helper.InsertQueuedEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, vendor.Email, vendor.Name, messageVendorTemplate, dbcontext);
                            }
                            catch (Exception ex)
                            {
                                Helper.SentryLogs(ex);
                            }
                        }

                    }
                }
                /* Vendor Email notification*/

            }
            catch (Exception ex)
            {
                Helper.SentryLogs(ex);
                throw;
            }
        }

        public static void InsertQueuedEmail(EmailAccount emailAccount, string subject, string body,
           string fromAddress, string fromName, string toAddress, string toName, MessageTemplate messageTemplate, ecuadordevContext dbcontext,
            string replyTo = null, string replyToName = null,
           IEnumerable<string> bcc = null, IEnumerable<string> cc = null,
           string attachmentFilePath = null, string attachmentFileName = null,
           int attachedDownloadId = 0, IDictionary<string, string> headers = null)
        {
            var email = new QueuedEmail
            {
                PriorityId = 5,
                From = !string.IsNullOrEmpty(fromAddress) ? fromAddress : emailAccount.Email,
                FromName = !string.IsNullOrEmpty(fromName) ? fromName : emailAccount.DisplayName,
                To = toAddress,
                ToName = toName,
                ReplyTo = string.Empty,
                ReplyToName = replyToName,
                Cc = string.Empty,
                Bcc = bcc != null && bcc.Count() > 0 ? string.Join(",", bcc.Select(x => x)) : string.Empty,
                Subject = subject,
                Body = body,
                AttachmentFilePath = attachmentFilePath,
                AttachmentFileName = attachmentFileName,
                AttachedDownloadId = messageTemplate.AttachedDownloadId,
                CreatedOnUtc = DateTime.UtcNow,
                EmailAccountId = emailAccount.Id,
                DontSendBeforeDateUtc = null
            };

            dbcontext.Add(email);
            dbcontext.SaveChanges();
        }
        #endregion
    }
}