﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NHKCustomerApplication.ViewModels
{
    public class CustomerLoginV2_1 : CustomerLogin
    {
        public string TimeZoneId { get; set; }
    }

    public class UploadPhotoModel
    {
        public bool Success { get; set; }
        public string FileName { get; set; }
    }
    public class CustomerModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Pswd { get; set; }
        public string ProfilePic { get; set; }
        //public string Address { get; set; }
        //public int City { get; set; }
        //public int Country { get; set; }
        //public int State { get; set; }
        //public int Pin { get; set; }
        //public int Gender { get; set; }
        public string Imei1 { get; set; }
        public string Imei2 { get; set; }
        public string DeviceNo { get; set; }
        public string LatPos { get; set; }
        public string LongPos { get; set; }
        public string ApiKey { get; set; }
        public int StoreId { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class CustomerDetails
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int MobileStd { get; set; }
        public string MobileNo { get; set; }
        public string ProfilePic { get; set; }
        public int StoreId { get; set; }
    }
    public class RegistationverificationCustomer
    {
        public int CustomerId { get; set; }
        public string OTP { get; set; }
        public string ApiKey { get; set; }
        public int StoreId { get; set; }
      
        public string UniqueSeoCode { get; set; }

    }
    public class ResendOTPModel
    {
        public int CustomerId { get; set; }
        public string ApiKey { get; set; }
        public string Imei1 { get; set; }
        public string Imei2 { get; set; }
        public string DeviceNo { get; set; }
        public string LatPos { get; set; }
        public string LongPos { get; set; }
        public int StoreId { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
        public string UniqueSeoCode { get; set; }

    }
    public class CustomerSignUpResponseModel
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNo { get; set; }
        public string Gender { get; set; }
        public string UserStatus { get; set; }
        public bool IsFCMToken { get; set; }
        public string DeviceToken { get; set; }
        public string Email { get; set; }
        public string OTP { get; set; }
    }
    public class CustomerDetailsModel
    {
        public int CustomerId { get; set; }
        public string ApiKey { get; set; }
        public int StoreId { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class CustomerLogin
    {
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Pswd { get; set; }
        public string Imei1 { get; set; }
        public string Imei2 { get; set; }
        public string DeviceNo { get; set; }
        public string LatPos { get; set; }
        public string LongPos { get; set; }
        public string ApiKey { get; set; }
        public string DeviceToken { get; set; }
        public int StoreId { get; set; }
        public string DeviceType { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class CustomerForgetPassword
    {

        public string Email { get; set; }
        public string Phone { get; set; }
        public string ApiKey { get; set; }
        public string Imei1 { get; set; }
        public string Imei2 { get; set; }
        public string DeviceNo { get; set; }
        public string LatPos { get; set; }
        public string LongPos { get; set; }
        public string DeviceToken { get; set; }
        public int StoreId { get; set; }
        public string DeviceType { get; set; }
        public string UniqueSeoCode { get; set; }

    }

    public class CustomerResetPassword
    {
        public string ApiKey { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public int CustomerId { get; set; }
        public int StoreId { get; set; }
        public string UniqueSeoCode { get; set; }
    }

    public class CustomerCreateNewPassword
    {
        public string ApiKey { get; set; }
        public string OTP { get; set; }
        public string NewPassword { get; set; }
        public int CustomerId { get; set; }
        public int StoreId { get; set; }
        public string UniqueSeoCode { get; set; }
    }

    public class ApplyCouponModel
    {
        public int CustomerId { get; set; }
        public string ApiKey { get; set; }
        public int StoreId { get; set; }
        public string CouponCode { get; set; }
        public int OrderId { get; set; }
        public decimal OrderTotal { get; set; }
    }
    public class CustomerAddressDetailsModel
    {
        public int CustId { get; set; }
        public string ApiKey { get; set; }
        public int AddressId { get; set; }
        public string UniqueSeoCode { get; set; }
        public int StoreId { get; set; }
    }
    public class CheckoutPaymentModel
    {
        public int CustomerId { get; set; }
        public string ApiKey { get; set; }
        public string PaymentToken { get; set; }
        public long Total { get; set; }
        public int OrderId { get; set; }
        public bool IsTip { get; set; }
        public long TipAmount { get; set; }
        public string Comments { get; set; }
        public string CouponCode { get; set; }
    }
    public class CheckoutPaymentModelV2
    {
        public int CustomerId { get; set; }
        public string ApiKey { get; set; }
        public int OrderId { get; set; }
        public string PaymentId { get; set; }
        public string PaymentMessage { get; set; }
        public bool IsSuccess { get; set; }
        public int PaymentMethodId { get; set; }
        public string PaymentToken { get; set; }
        public bool IsNew { get; set; }

        public string CardId { get; set; }
        public string UniqueSeoCode { get; set; }
        public int StoreId { get; set; }
    }
    public class PaymentMethodModel
    {
        public int PaymentMethodId { get; set; }
        public string PaymentMethodName { get; set; }
        public bool isShow { get; set; }
    }
    public class CustomerAddressModel
    {
        public int Id { get; set; }
        public int LabelId { get; set; }
        public string Label { get; set; }
        public string City { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipPostalCode { get; set; }
        public string APIKey { get; set; }
        public int CustId { get; set; }

    }
    public class CustomerAddressModelV2
    {
        public int Id { get; set; }
        public int LabelId { get; set; }
        public string Label { get; set; }
        public string City { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string phoneNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipPostalCode { get; set; }
        public string ApiKey { get; set; }
        public int CustId { get; set; }
        public string LatPos { get; set; }
        public string LongPos { get; set; }
        public string UniqueSeoCode { get; set; }
        public int StoreId { get; set; }

    }

    public class CustomerAddressViewModel
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public string City { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string ZipCode { get; set; }
        public decimal LatPos { get; set; }
        public decimal LongPos { get; set; }
    }

    public class PasswordModel
    {
        public int CustomerId { get; set; }
        public bool Active { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
    }

    public class CustomerResponseModel
    {
        public bool ValidData { get; set; }
        public string ResultData { get; set; }
    }
    public class CustomerLoginResponseModel : CustomerResponseModel
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNo { get; set; }
        public string UserStatus { get; set; }
        public string Email { get; set; }
    }
    public class CustomerRestProductResponseModel : CustomerResponseModel
    {
        public List<RestProductModel> Products { get; set; }
    }
    public class RestProductModelV2
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
    public class RestProductModel
    {
        public int ItemId { get; set; }
        public string itemName { get; set; }
        public string ShortDescription { get; set; }
        public int CategoryId { get; set; }
        public int ParentCategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ParentCategoryName { get; set; }
        public bool Available { get; set; }
        public int? Quantity { get; set; }
        public bool IsProductAttributesExist { get; set; }
        public decimal? ProductPrice { get; set; }
        public string ProductPreferences { get; set; }
        public string ProductPics { get; set; }
        public string CustomeURL { get; set; }
        public string Rating { get; set; }
        public string Cuisine { get; set; }
        public string ChefBaseUrl { get; set; }
        public int? PictureSize { get; set; }
        public string CurrencyCode { get; set; }
    }

}