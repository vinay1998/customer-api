﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace NHKCustomerApplication.ViewModels
{
    public class TopRatedRestnChefModel
    {
        public int CustomerId { get; set; }
        public double LatPos { get; set; }
        public double LongPos { get; set; }
        public int StoreId { get; set; }
        public int SplitSize { get; set; }
        public int EndItemCount { get; set; }
        public DateTime CurrentDate{ get; set; }
        public string ApiKey { get; set; }
        public int RestType { get; set; }

        [DefaultValue(false)]
        public bool SkipLocationSearch { get; set; }
        public string MerchantCategoryIds { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class SearchCooknChefDishesModel
    {
        public int CustomerId { get; set; }
        public string    LatPos { get; set; }
        public string LongPos { get; set; }
        public int StoreId { get; set; }
        public string ApiKey { get; set; }
        public string SearchValue { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public  class Filters
    {
        public string Title { get; set; }
        public string TitleValueName { get; set; }
        public int OrderIndex { get; set; }
        public string CategoryName { get; set; }
    }
    public class CooknRestDetailsByID
    {
        public int CustId { get; set; }
        public int RestnCookId { get; set; }
        public string APIKey { get; set; }
        public decimal CustLat { get; set; }
        public decimal CustLong { get; set; }
        public int StoreId { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class AvailableFromTimingModel
    {
        public int CustomerId { get; set; }
        public int RestnCookId { get; set; }
        public string APIKey { get; set; }
        public int StoreId { get; set; }
        public string Date { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class AvailableToTimingModel
    {
        public int CustomerId { get; set; }
        public int RestnCookId { get; set; }
        public string APIKey { get; set; }
        public int StoreId { get; set; }
        public string Fromtime { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class AvailableTablesModel
    {
        public int CustomerId { get; set; }
        public int RestnCookId { get; set; }
        public string APIKey { get; set; }
        public int StoreId { get; set; }
        public string Fromtime { get; set; }
        public string ToTime { get; set; }
        public string Date { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class CooknRestProductByID
    {
        public int CustId { get; set; }
        public int StoreId { get; set; }
        public int RestnCookId { get; set; }
        public string APIKey { get; set; }
        public string UniqueSeoCode { get; set; }
        public List<Filters> Filters { get; set; }

    }
    public class CooknRestProductDetailsByID
    {
        public int CustId { get; set; }
        public int RestnCookId { get; set; }
        public int ProductId { get; set; }
        public string APIKey { get; set; }
        public int StoreId { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class CooknRestCrossSellProducts
    {
        public int CustId { get; set; }
        public string APIKey { get; set; }
        public int StoreId { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class CooknRestProductByCategoryId
    {
        public int CustomerId { get; set; }
        public string APIKey { get; set; }
        public double LatPos { get; set; }
        public double LongPos { get; set; }
        public int StoreId { get; set; }
        public int SplitSize { get; set; }
        public int EndItemCount { get; set; }
    }
    public class CooknRestGalleryById
    {
        public int RestnCookId { get; set; }
        public string APIKey { get; set; }
    }
    public class CooknRestProductProductAttributeModel
    {
        public int ProductId { get; set; }
        public string APIKey { get; set; }
        public int RestnCookId { get; set; }
    }
    public class MarkaslikeModel
    {
        public int CustomerId { get; set; }
        public int RestnCookId { get; set; }
        public string APIKey { get; set; }
        public bool IsLike { get; set; }
        public int StoreId { get; set; }
       
        public string UniqueSeoCode { get; set; }
    }
    public class CooknChefProducts
    {
        public int RestnCookId { get; set; }
        public string RestnCook { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public bool Available { get; set; }
        public string Cuisine { get; set; }
        public List<Preferences> Preferences { get; set; }
        public List<CooknChefProductsImages> ImageUrl { get; set; }
        public string Price { get; set; }
        public decimal Rating { get; set; }
        public int RatingCount { get; set; }
    }
    public class CooknChefProductsImages
    {
        public byte[] ProductImage { get; set; }
        public string ProductImageURL { get; set; }
        public string ProductMime { get; set; }
        public int ProductPictureId { get; set; }
    }
    public class CooknChefProductsV2
    {
        public int RestnCookId { get; set; }
        public string RestnCook { get; set; }
        public int RestnCookPictureId { get; set; }
        public string RestnCookPictureURL { get; set; }
        public int ItemId { get; set; }
        public int Quantity { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public bool Available { get; set; }
        public string Cuisine { get; set; }
        public List<PreferencesV2> Preferences { get; set; }
        public List<CooknChefProductsImagesV2> ImageUrl { get; set; }
        public string Price { get; set; }
        public decimal Rating { get; set; }
        public int RatingCount { get; set; }
        public int ReviewCount { get; set; }
        public List<CustomerImage> CustomerImages { get; set; }
    }
    public class CustomerImage
    {
        public string Image { get; set; }
    }
    public class CooknChefCatProductsV2
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public bool Available { get; set; }
        public string Cuisine { get; set; }
        public List<PreferencesV2> Preferences { get; set; }
        public List<CooknChefProductsImagesV2> ImageUrl { get; set; }
        public string Price { get; set; }
        public double Rating { get; set; }
        public int RatingCount { get; set; }
        public int ReviewCount { get; set; }
        public List<CustomerImage> CustomerImages { get; set; }
        public string AvailableAfterhrs { get; set; }
        public bool IsAvailableAfterhrs { get; set; }
        public List<CooknRestProductProductAttributesResponse> ProductProductAttributes { get; set; }
        public bool IsProductAttributesExist { get; set; }
    }

    public class CooknRestProductProductAttributesResponse
    {
        //public CooknRestProductProductAttributesResponse()
        //{
        //    ProductAttribute = new List<ProductAttributes>();
        //}
        public int ProductAttributeMappingId { get; set; }
        public int ProductAttributeId { get; set; }
        public int ProductId { get; set; }
        public string AttributeName { get; set; }
        public int AttributeTypeId { get; set; }
        //public string AttributeType { get; set; }
        public bool IsRequired { get; set; }
        public List<ProductAttributes> ProductAttribute { get; set; }
        public override bool Equals(object obj)
        {
            //As the obj parameter type id object, so we need to
            //cast it to Student Type
            return this.ProductAttributeMappingId == ((CooknRestProductProductAttributesResponse)obj).ProductAttributeMappingId && this.AttributeName == ((CooknRestProductProductAttributesResponse)obj).AttributeName;
        }
        public override int GetHashCode()
        {
            //Get the ID hash code value
            int IDHashCode = this.ProductAttributeMappingId.GetHashCode();
            //Get the string HashCode Value
            //Check for null refernece exception
            int NameHashCode = this.AttributeName == null ? 0 : this.AttributeName.GetHashCode();
            return IDHashCode ^ NameHashCode;
        }
    }
    public class ProductAttributes
    {
        public int ProductAttributeValueId { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public bool IsPreSelected { get; set; }
        public string Currency { get; set; }
        public bool UsePercentage { get; set; }
    }

    public class ProductAttributesV2: ProductAttributes
    {
        public string AttributeName { get; set; }
        public bool IsRequired { get; set; }
        public int AttributeTypeId { get; set; }
        public int ProductId { get; set; }
        public int ProductAttributeId { get; set; }
        public int ProductAttributeMappingId { get; set; }
    }

    public class CooknChefProductsImagesV2
    {
        public string ProductImageURL { get; set; }
        public string MimeType { get; set; }
        public string FileSeo { get; set; }
    }
    public class CooknRestProductsV2
    {
        public CooknRestProductsV2()
        {
            Subcategories = new List<CooknRestProductsV3>();
            CategoryItems = new List<CooknChefCatProductsV2>();
        }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public List<CooknRestProductsV3> Subcategories { get; set; }
        public List<CooknChefCatProductsV2> CategoryItems { get; set; }
        public override bool Equals(object obj)
        {
            //As the obj parameter type id object, so we need to
            //cast it to Student Type
            return this.CategoryId == ((CooknRestProductsV2)obj).CategoryId && this.CategoryName == ((CooknRestProductsV2)obj).CategoryName;
        }
        public override int GetHashCode()
        {
            //Get the ID hash code value
            int IDHashCode = this.CategoryId.GetHashCode();
            //Get the string HashCode Value
            //Check for null refernece exception
            int NameHashCode = this.CategoryName == null ? 0 : this.CategoryName.GetHashCode();
            return IDHashCode ^ NameHashCode;
        }
    }
    public class CooknRestProductsV3
    {

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public List<CooknChefCatProductsV2> CategoryItems { get; set; }
    }
    public partial class BestsellersProduct
    {
        /// <summary>
        /// Gets or sets the product identifier
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the total amount
        /// </summary>
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// Gets or sets the total quantity
        /// </summary>
        public int TotalQuantity { get; set; }
    }
    public partial class BookingTableTransactionListModel
    {
        public string APIKey { get; set; }
        public int StoreId { get; set; }
        public int RestnCookId { get; set; }
        public string BookingDate { get; set; }
        public string BookingTimeFrom { get; set; }
        public string BookingTimeTo { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string MobileNumber { get; set; }
        public int TableNumber { get; set; }

        public string SpecialNotes { get; set; }
        public string UniqueSeoCode { get; set; }


        public int Seats { get; set; }


    }
    public class SearchFilters
    {
        public int TitleValueId { get; set; }
        public string TitleValueName { get; set; }
    }
    public class SearchFiltersMain
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string TitleType { get; set; }
        public List<SearchFilters> CategoryValueFilter { get; set; }
    }
    public class SortPrice
    {
        public decimal Price { get; set; }
        public int ItemId { get; set; }
    }
    public class RestaurantnCookGalleryModel
    {
        public string Address { get; set; }
        public int ImageId { get; set; }
        public string ImageCaption { get; set; }
        public string ImageURL { get; set; }

    }
    public class ProductDetailResponseModel
    {
        public ProductDetailResponseModel()
        {
            CustomerImages = new List<CustomerImage>();
            Comments = new List<ProductCommentsModels>();
            Preferences = new List<PreferencesV2>();
            ImageUrl = new List<CooknChefProductsImagesV2>();
            ProductProductAttributes = new List<CooknRestProductProductAttributesResponse>();
            Specifications = new List<SpecificationModel>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public string Price { get; set; }
        public double Rating { get; set; }
        public int RatingCount { get; set; }
        public int ReviewCount { get; set; }
        public List<CustomerImage> CustomerImages { get; set; }
        public List<ProductCommentsModels> Comments { get; set; }
        public List<PreferencesV2> Preferences { get; set; }
        public List<CooknChefProductsImagesV2> ImageUrl { get; set; }
        public List<CooknRestProductProductAttributesResponse> ProductProductAttributes { get; set; }
        public string DishPrepareTime { get; set; }
        public string Cuisine { get; set; }
        public bool IsProductAttributesExist { get; set; }
        public List<SpecificationModel> Specifications { get; set; }
    }
    public class ProductCommentsModels
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string Comment { get; set; }
        public string CustomerImage { get; set; }
    }

    public class SearchItemsModel
    {
        public int CustomerId { get; set; }
        public int StoreId { get; set; }
        public int RestnCookId { get; set; }
        public string APIKey { get; set; }
        public string UniqueSeoCode { get; set; }
        public string Keywords { get; set; }

        [DefaultValue(0)]
        public int PageIndex { get; set; }

        [DefaultValue(0)]
        public int PageSize { get; set; }
    }
}