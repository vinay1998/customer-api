﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NHKCustomerApplication.ViewModels
{
    public class CountryModel
    {
        public string ApiKey { get; set; }
        public int StoreId { get; set; }
        public string UniqueSeoCode { get; set; }

    }
    public class CountryModel2
    {
        public string ApiKey { get; set; }
        public int StoreId { get; set; }
        public int MId { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class StoreOptions
    {
        public bool IsDelivery { get; set; }
        public bool IsDinning { get; set; }
        public bool IsTakeaway { get; set; }

    }
    public class RestTypeModel
    {
        public int Id { get; set; }
        public string   Name { get; set; }
        public bool IsAvailable { get; set; }

    }
    public class StateModel
    {
        public string ApiKey { get; set; }
        public int CountryId { get; set; }

    }
    public class CityModel
    {
        public string APIKey { get; set; }
        public int StateId { get; set; }

    }
    public class Items
    {
        public string Name { get; set; }
    }
    public class Preferences
    {
        public string Name { get; set; }
        public byte[] Image { get; set; }

    }
    public class PreferencesV2
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string ImageType { get; set; }

    }
    public class RatingModel
    {
        public int Rating { get; set; }
    }
    public class CooknRest
    {
        public int RestnCookId { get; set; }
        public string CooknRestName { get; set; }
        public string CooknRestDescription { get; set; }
        public byte[] CooknRestImageURL { get; set; }
        public string CooknRestImage { get; set; }
        public int PictureId { get; set; }
        public int? Rating { get; set; }
        public int RatingCount { get; set; }
        public bool IsLike { get; set; }
        public string RestnCookAddress { get; set; }
        public bool IsStatusAvailable { get; set; }
        public List<Items> CousinItems { get; set; }
        public string Distance { get; set; }
        public string OpenTime { get; set; }
        public string CloseTime { get; set; }
    }
    public class CooknRestV2
    {
        public CooknRestV2()
        {
            OpenCloseTime = new List<string>();
            Cuisine = new List<Items>();
        }
        public int RestnCookId { get; set; }
        public string CooknRestName { get; set; }
        public List<Items> Cuisine { get; set; }
        public string CooknRestImageURL { get; set; }
        public int PictureId { get; set; }
        public decimal Rating { get; set; }
        public int RatingCount { get; set; }
        public string RestnCookAddress { get; set; }
        public bool IsOpen { get; set; }
        public string Distance { get; set; }
        public string OpenTime { get; set; }
        public string CloseTime { get; set; }
        public bool IsRestAvailable { get; set; }

        public string Geolocation { get; set; }

        public int AvailableType { get; set; }
        public List<string> OpenCloseTime { get; set; }
    }
}