﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.ViewModels
{
    public class OrderReviewModel
    {
        public string ApiKey { get; set; }
        public int CustomerId { get; set; }
        public int OrderId { get; set; }
        public int StoreId { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class NotificationModel
    {
        public string ApiKey { get; set; }
        public int CustomerId { get; set; }
        public int StoreId { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class OrderTrackingModel
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderNumber { get; set; }
        public string Amount { get; set; }
        public string DiscountAmount { get; set; }
        public string OrderStatus { get; set; }
        public string BillingAddress { get; set; }
        public string ShippingAddress { get; set; }
        public string BillingAddressType { get; set; }
        public string ShippingAddressType { get; set; }
        public string MerchantAddressType { get; set; }
        public string CooknChefAddress { get; set; }
        public List<OrderedItems> OrderedItems { get; set; }
        public List<OrderedStatusArray> OrderedStatusArray { get; set; }
        public int OrderType { get; set; }
        public string PackingCharges { get; set; }
        public string SubAmount { get; set; }
        public string Tax { get; set; }
        public string DeliveryCharges { get; set; }
        public string ServiceChargeAmount { get; set; }
        public string OrderNotes { get; set; }
        public bool IsOrderNotes { get; set; }
        public string Tip { get; set; }
        public string AdditionalInstructions { get; set; }
        public bool IsAdditionalInstructions { get; set; }

    }
    public class OrderedItems
    {
        public string ProductName { get; set; }
        public int ProductQuantity { get; set; }
        public string ProductTotalPrice { get; set; }
        public string ProductPrice { get; set; }
        public string ProductImage { get; set; }
    }
    public class OrderNotesNotifiactionModel
    {
        public int OrderId { get; set; }
        public string Title { get; set; }
        public string OrderNumber { get; set; }
        public int OrderStatus { get; set; }
        public string Description { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime OrderNotificationDateTime { get; set; }
        public string OrderNotificationDate { get; set; }
        public string OrderNotificationTime { get; set; }
    }
}