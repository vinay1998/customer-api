﻿using System;
using System.Collections.Generic;

namespace NHKCustomerApplication.ViewModels
{
    public class OrderReviewDetailsModel
    {
        public string ApiKey { get; set; }
        public int CustomerId { get; set; }
        public int OrderId { get; set; }
        public int StoreId { get; set; }
        public List<ReviewItems> ReviewItems { get; set; }
    }
    public class OrderReviewDetailsModelV2
    {
        public string ApiKey { get; set; }
        public int CustomerId { get; set; }
        public int OrderId { get; set; }
        public int StoreId { get; set; }
        public string UniqueSeoCode { get; set; }
        public VendorReviews vendorReviews { get; set; }
        public AgentReviews agentReviews { get; set; }
        public List<ReviewProduct> ReviewProducts { get; set; }
    }
    public class ReviewItemsV2
    {
        public VendorReviews vendorReviews { get; set; }
        public AgentReviews agentReviews { get; set; } 
        public List<ReviewProduct> ReviewProducts { get; set; }
    }
    public class ReviewProduct
    {
        public int ProductId { get; set; }
        public int Rating { get; set; }
        public string AdditionalComments { get; set; }
        public bool IsLiked { get; set; }
        public bool IsSkip { get; set; }
        public bool IsNotIntersted { get; set; }
    }
    public class VendorReviews
    {
        public string Reviews { get; set; }
        public int Rating { get; set; }
        public int MerchantId { get; set; }
        public string AdditionalComments { get; set; }
        public bool IsLiked { get; set; }
        public bool IsSkip { get; set; }
        public bool IsNotIntersted { get; set; }
    }
public class AgentReviews
{
        public int Rating { get; set; }
        public string Reviews { get; set; }
    public int AgentId { get; set; }
    public string AdditionalComments { get; set; }
    public bool IsLiked { get; set; }
    public bool IsSkip { get; set; }
    public bool IsNotIntersted { get; set; }
}
public class ReviewItemAttributes
    {
        public string  Name { get; set; }
        public bool IsLiked { get; set; }
       
    }
    public class ReviewItems
    {
        public int ProductId { get; set; }
        public int Rating { get; set; }
        public bool IsLiked { get; set; }
        public string Comments { get; set; }
    }
    public class OrderProducts
    {
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public List<ProductsDetails> Products { get; set; }
    }
    public class ReviewsItems
    {
        public ReviewsItems()
        {
            AgentReview = new AgentReview();
            MerchantReview = new MerchantReview();
            Products = new List<ProductsDetails>();
        }
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public bool IsAgentReview { get; set; }
        public bool IsMerchantReview { get; set; }
        public bool IsItemReview { get; set; }
        public AgentReview AgentReview { get; set; }
        public MerchantReview MerchantReview { get; set; }
        public List<ProductsDetails> Products { get; set; }
    }
    public class AgentReview
    {
        public AgentReview()
        {
            ReviewOptions = new List<string>();
        }
        public int AgentId { get; set; }
        public int PictureId { get; set; }
        public string Picture { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string ReviewOptionsS { get; set; }
        public List<string> ReviewOptions { get; set; }
    }
    public class MerchantReview
    {
        public MerchantReview()
        {
            ReviewOptions = new List<string>();
        }
        public int MerchantId { get; set; }
        public int PictureId { get; set; }
        public string Picture { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string ReviewOptionsS { get; set; }
        public List<string> ReviewOptions { get; set; }
    }
    public class ProductsDetails
    {
        public int ProductId { get; set; }
        public string Title { get; set; }
        public string   Subtitle { get; set; }
        public int PictureId { get; set; }
        public string ProductImage { get; set; }
    }
    public class OrderedStatusArray
    {
        public int StatusId { get; set; }
        public string StatusTitle { get; set; }
        public string  StatusTime { get; set; }
        public string StatusDescription { get; set; }
        public DateTime StatusDate { get; set; }
        public bool Isdone { get; set; }
    }
    public partial class RatingReview
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }

        public int EntityId { get; set; }

        public int OrderId { get; set; }

        public int ReviewType { get; set; }

        public int StoreId { get; set; }

        public bool IsApproved { get; set; }

        public string Title { get; set; }

        public string ReviewText { get; set; }

        public string ReplyText { get; set; }

        public int Rating { get; set; }

        public string ReviewOptions { get; set; }

        public bool IsLiked { get; set; }

        public bool IsNotInterested { get; set; }

        public bool? IsSkipped { get; set; }

        public DateTime CreatedOnUtc { get; set; }
    }
    public class PaymentCheckout
    {
        public string OrderNumber { get; set; }
        public string OrderGuid { get; set; }
        public string OrderArrivalTime { get; set; }
        public long OrderId { get; set; }

        public string PayPalurl { get; set; }
    }
    public class CardModel
    {
        public string ApiKey { get; set; }
        public string CardToken { get; set; }
        public int CustomerId { get; set; }
        public int StoreId { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class DeleteCardModel
    {
        public string ApiKey { get; set; }
        public string CardId { get; set; }
        public int CustomerId { get; set; }
        public string UniqueSeoCode { get; set; }
        public int StoreId { get; set; }
    }
}