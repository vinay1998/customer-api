﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NHKCustomerApplication.ViewModels
{
    public class FCSNotificationModel
    {
        public string ApiKey { get; set; }
        //public string WebAddr { get; set; }
        //public string WebKey { get; set; }
        public string To { get; set; }
        public string Heading { get; set; }
        public string OrderId { get; set; }
        public string CustomerId { get; set; }
        public string Status { get; set; }
        public string DeliveredTime { get; set; }
        public string OrderNumber { get; set; }
        public int RestType { get; set; }
        public string UniqueSeoCode { get; set; }

        public string StoreId { get; set; }
    }
    public class FcsNotificationRestTypeModel
    {
        public FcsNotificationRestTypeModel()
        {
            delivery = new List<FCSNotificationV2Model>();
            takeaway = new List<FCSNotificationV2Model>();
        }
        public List<FCSNotificationV2Model> delivery { get; set; }
        public List<FCSNotificationV2Model> takeaway { get; set; }
    }
    public class FCSNotificationV2Model
    {
        //public string WebAddr { get; set; }
        //public string WebKey { get; set; }
        public string OrderId { get; set; }
        public string Status { get; set; }
        public string DeliveredTime { get; set; }
        public string OrderNumber { get; set; }
        public int RestType { get; set; }
    }
    public class FCSNotificationModelGet
    {
        public string APIKey { get; set; }
        //public string WebAddr { get; set; }
        //public string WebKey { get; set; }
        public string To { get; set; }
        public int OrderId { get; set; }
        public int CustomerId { get; set; }
        public int StoreId { get; set; }
    }
    public class FCSNotificationModelGetV2
    {
        public string ApiKey { get; set; }
        //public string WebAddr { get; set; }
        //public string WebKey { get; set; }
        public int CustomerId { get; set; }
        public int StoreId { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class FCSResultModel
    {
        public bool Result { get; set; }
        public string ResultMsg { get; set; }
    }
    public class FCSResultObjModel
    {
        public long multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public List<Result> results { get; set; }

    }
    public class Result
    {
        public string error { get; set; }
        public string message_id { get; set; }
    }
}