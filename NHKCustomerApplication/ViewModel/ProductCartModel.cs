﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NHKCustomerApplication.ViewModels
{
    public class ProductCartModel
    {
        public int CustomerId { get; set; }
        public List<ProductDetailsModel> products { get; set; }
        public int StoreId { get; set; }
        public int RestnCookId { get; set; }
        public string ApiKey { get; set; }
    }
    public class DiscountModel
    {
        public decimal DiscountAmount { get; set; }
        public int DiscountId { get; set; }
    }
    public class ProductDetailsModel
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }

    }
    public class AddProductToCartv2
    {
        public string ApiKey { get; set; }
        public int ProductId { get; set; }
        public int quantity { get; set; }
        public int StoreId { get; set; }
        public bool IsRestnCookChanged { get; set; }
        public int CustomerId { get; set; }
        public decimal TipAmount { get; set; }
        public string CouponCode { get; set; }
        public bool IsTakeaway { get; set; }
        public bool IsDelivery { get; set; }
        public string UniqueSeoCode { get; set; }

        public int languageId { get; set; }
      

        //public int ProductAttributeMappingId { get; set; }
        //public int ProductAttributeValueId { get; set; }
        public List<ProductAttributesModel> ProductAttributes { get; set; }

    }
    public class ProductAttributesModel
    {
        public int ProductAttributeMappingId { get; set; }
        public int ProductAttributeValueId { get; set; }
    }
    public class ProductDetailModel
    {
        public int CartItemId { get; set; }
        public string ProductAttribute { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public string ProductName { get; set; }
        public string ProductPrice { get; set; }
        public bool ProductAttributeAvailable { get; set; }
        public string ProductAttributePrice { get; set; }
        public string ProductAttributeValue { get; set; }
        public string ProductImageURL { get; set; }

        public decimal ProductTaxPrice { get; set; }
        //public int RestnChefId { get; set; }
    }
    public class AddRemoveProductCart
    {
        public string ApiKey { get; set; }
        public int StoreId { get; set; }
        public int CustomerId { get; set; }
        public int CartItemId { get; set; }
        public decimal TipAmount { get; set; }
        public string CouponCode { get; set; }
        public bool IsDelivery { get; set; }
        public bool IsTakeAway { get; set; }
        public string UniqueSeoCode { get; set; }
        public int languageId { get; set; }
        public int deliveryAddressId { get; set; }
    }
    public class ProductDetailRootModel
    {
        public string   Subtotal { get; set; }
        public string CurrencySymbol { get; set; }
        public string   Tax { get; set; }
        public string MinDateOrder { get; set; }
        public string DeliveryCharges { get; set; }
        public string Tip { get; set; }
        public string DiscountAmount { get; set; }
        public string DiscountCode { get; set; }
        public int DiscountId { get; set; }
        public string Total { get; set; }
        public string Coupon { get; set; }
        public string EstTime { get; set; }
        public string ServiceChargeAmount { get; set; }
        public string AppliedTaxRates { get; set; }
        public List<ProductDetailModel> CartProducts { get; set; }
    }
    public class ChekoutCartProductsModel
    {
        public string ApiKey { get; set; }
        public int CustomerId { get; set; }
        public int StoreId { get; set; }
        
        public bool IsDelivery { get; set; }
        public bool IsTakeAway { get; set; }
        public int DeliveryAddressId { get; set; }
        public int BillingAddressId { get; set; }
        public bool IsDeliveryCharges { get; set; }

        public List<ChekoutRestnCookModel> chekoutRestnCookModels { get; set; }

    }
    public class ChekoutCartProductsModelV3
    {
        public string ApiKey { get; set; }
        public int CustomerId { get; set; }
        public int StoreId { get; set; }
        public string SpecialNotes { get; set; }
        public bool IsDelivery { get; set; }
        public bool IsTakeAway { get; set; }
        public bool IsDinning { get; set; }
        public int DeliveryAddressId { get; set; }
        public int BillingAddressId { get; set; }
        public string DeliveryCharges { get; set; }
        public string PackingCharges { get; set; }
        public string Tax { get; set; }
        public string Tip { get; set; }
        public string DiscountAmount { get; set; }
        public string Subtotal { get; set; }
        public string Total { get; set; }
        public string AdditionalComments { get; set; }
        public int RestnCookId { get; set; }
        public string OrderDate { get; set; }
        public string CouponCode { get; set; }
        public string UniqueSeoCode { get; set; }
        //public List<ChekoutRestnCookModel> chekoutRestnCookModels { get; set; }

    }
    public class ChekoutRestnCookModel
    {
        public int RestnCookId { get; set; }
        public List<CheckoutProductCartItemsModel> checkoutProductCartItemslist { get; set; }
    }
    public class CheckoutProductCartItemsModel
    {
        
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
    public class CheckoutProductCartResponseModel
    {
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public string OrderTotal { get; set; }
    }
    public class CartProductsDetailsModel
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal ProductPrice { get; set; }

    }
    public class OrderHistoryModel
    {
        public int CustomerId { get; set; }
        public int StoreId { get; set; }
        public string ApiKey { get; set; }
        public string  OrderId { get; set; }
        public string OrderDate { get; set; }
        public string OrderStatus { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class AddProductToCart
    {
        public string ApiKey { get; set; }
        public int ProductId { get; set; }
        public int quantity { get; set; }
        public int StoreId { get; set; }
        public bool IsRestnCookChanged { get; set; }
        public int CustomerId { get; set; }
        public decimal TipAmount { get; set; }
        public string CouponCode { get; set; }
        public bool IsDelivery { get; set; }
        public bool IsTakeaway { get; set; }
        public string UniqueSeoCode { get; set; }
        public int languageId { get; set; } 
    }
    public class ProductCartCount
    {
        public string ApiKey { get; set; }
        public int StoreId { get; set; }
        public int CustomerId { get; set; }
        public string UniqueSeoCode { get; set; }
    }
    public class SpecificationModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}